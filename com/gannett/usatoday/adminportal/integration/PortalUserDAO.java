package com.gannett.usatoday.adminportal.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.users.PortalUserRoleEnum;
import com.gannett.usatoday.adminportal.users.intf.PortalUserIntf;
import com.gannett.usatoday.adminportal.users.intf.PortalUserTOIntf;
import com.gannett.usatoday.adminportal.users.to.PortalUserTO;
import com.ibm.websphere.ce.cm.DuplicateKeyException;
import com.usatoday.UsatException;

public class PortalUserDAO extends USATodayDAO {

	// SELECTS
    private static final String SELECT_ALL_USAT_USERS = "select user_id, enabled, last_name, first_name, phone, email, password, insert_timestamp from usat_users";	
    private static final String SELECT_ENABLED_USAT_USER_BY_ID_PWD = SELECT_ALL_USAT_USERS + " where user_id = ? and password = ? and enabled = 1";
    private static final String SELECT_USERS_BY_EMAIL = SELECT_ALL_USAT_USERS + " where email = ? and enabled = 1";
    // Update
    private static final String UPDATE_USER = "update usat_users set enabled = ?, password = ?, phone = ?, last_name = ?, first_name = ?, email = ? where user_id = ?";

    // INSERT
    private static final String INSERT_USER = "INSERT INTO usat_users (user_id, insert_timestamp, enabled, password, phone, last_name, first_name, email) values" +
    "(?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String INSERT_ROLE_FOR_USER = "INSERT INTO usat_userrole_xref (user_id, role_ID) values (?, ?)";
    
    // role query
    private static final String SELECT_USER_ROLES = "SELECT role_id FROM usat_userrole_xref WHERE usat_userrole_xref.user_id = ?";
    
    // DELETE
    private static final String DELETE_ROLES_FOR_USER = "delete from usat_userrole_xref where user_id = ?";
    private static final String DELETE_USER = "delete from usat_users where user_id = ?";
    
    
	public void insert(PortalUserIntf uu) throws UsatException {
		Connection conn = null;
		boolean updateRoles = false;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			PreparedStatement ps = conn.prepareStatement(PortalUserDAO.INSERT_USER);

            ps.setString(1, uu.getUserID());
            
            Timestamp insertTime = new Timestamp(System.currentTimeMillis()); 
            ps.setTimestamp(2, insertTime);
            
            if (uu.getEnabled()) {
                byte b = 0x01;
                ps.setByte(3, b);
            }
            else {
                byte b = 0x00;
                ps.setByte(3, b);
            }
            
            ps.setBytes(4, uu.getPassword().getBytes());
            
            ps.setString(5, uu.getPhone());
            
            ps.setString(6, uu.getLastName());
            
            ps.setString(7, uu.getFirstName());
            
            ps.setString(8, uu.getEmailAddress());
                                    
            int numRows = ps.executeUpdate();
            
            if (numRows != 1){
                throw new UsatException("usat_user insert failed (numrows not 1) for participant: " + uu.getUserID());
            }
            
            updateRoles = true;
        }
		catch (Exception e) {
			if (e instanceof DuplicateKeyException) {
				throw new UsatException("Exception Inserting New User. User ID already exists. Either edit the existing user or change the user id.", e);
			}
			throw new UsatException("Exception Inserting New User. " + e.getMessage(), e);
		}
		finally {
			this.cleanupConnection(conn);
		}

		// update the user role assignements
		if (updateRoles) {
			this.updateUserRoles(uu.getUserID(), uu.getAssignedRoles());
		}
	}
	
	public void update(PortalUserIntf uu) throws UsatException {
		Connection conn = null;
		boolean updateRoles = false;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			PreparedStatement ps = conn.prepareStatement(PortalUserDAO.UPDATE_USER);

			//      "update usat_users set enabled = ?, password = ?, phone = ?, last_name = ?, 
			//        first_name = ?, email = ? where user_id = ?";
            if (uu.getEnabled()){
                byte b = 1;
                ps.setByte(1, b);
            }
            else {
                byte b = 0;
                ps.setByte(1, b);
            }
            
            ps.setBytes(2, uu.getPassword().getBytes());
            ps.setString(3, uu.getPhone());
            
            ps.setString(4, uu.getLastName());
            ps.setString(5, uu.getFirstName());
            ps.setString(6, uu.getEmailAddress());
                        
            ps.setString(7, uu.getUserID());
            
            int numRows = ps.executeUpdate();
            
            if (numRows != 1){
                throw new UsatException("usat_user update failed (numrows not 1) for participant: " + uu.getUserID());
            }
            updateRoles = true;
        }
		catch (Exception e) {
			throw new UsatException("Exception updating User.", e);
		}
		finally {
			this.cleanupConnection(conn);
		}

		// update the user role assignements
		if (updateRoles) {
			this.updateUserRoles(uu.getUserID(), uu.getAssignedRoles());
		}
		
	}
	
	private void updateUserRoles(String userID, Collection<PortalUserRoleEnum>roles) throws UsatException{

		if (roles == null || roles.size() == 0){
            return;
        }
        
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
         
			// delete existing roles
			PreparedStatement deleteStatement = conn.prepareStatement(PortalUserDAO.DELETE_ROLES_FOR_USER);
			
			deleteStatement.setString(1,userID);
			deleteStatement.executeUpdate();
			
			// add new roles.
			for (PortalUserRoleEnum ur : roles) {
	
	            try {
	                 PreparedStatement ps = conn.prepareStatement(PortalUserDAO.INSERT_ROLE_FOR_USER);
	                 // user_id, role_id
	                 ps.setString(1, userID);
	                 ps.setInt(2, ur.getDbRepresentation());
	                                                     
	                 int numRows = ps.executeUpdate();
	            
	                 if (numRows != 1){
	                     throw new UsatException("usat_userrole_xref insert failed (numrows not 1) for participant: " + userID + " role:" + ur);
	                 }            
	             }
	             catch (Exception e) {
	                 // Continue to next one
	                System.out.println("Failed to insert userrole xref: " + e.getMessage());
	             }            
	        }

		}
		catch (Exception e) {
			throw new UsatException("Exception updating User.", e);
		}
		finally {
			this.cleanupConnection(conn);
		}

	}
	
	public void delete(PortalUserIntf user) throws UsatException {
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			PreparedStatement ps = conn.prepareStatement(PortalUserDAO.DELETE_ROLES_FOR_USER);
			ps.setString(1, user.getUserID());
			
			int numRows = ps.executeUpdate();
			
			System.out.println("Delete Portal Admin User, deleted roles: " + numRows);
			
			ps = conn.prepareStatement(PortalUserDAO.DELETE_USER);
			ps.setString(1, user.getUserID());
			
			numRows = ps.executeUpdate();
			
			System.out.println("deleted user: " + user.getUserID());
			
			user.setUserID("");
		
		}
		catch (Exception e) {
			throw new UsatException("Exception updating User.", e);
		}
		finally {
			this.cleanupConnection(conn);
		}
		
	}
	
	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private Collection<PortalUserTOIntf> objectFactory(ResultSet rs) throws UsatException {
		ArrayList<PortalUserTOIntf> users = new ArrayList<PortalUserTOIntf>();

        try {
            while (rs.next()){
                PortalUserTO uu = new PortalUserTO();
            
                uu.setUserID(rs.getString("user_id").trim());
                uu.setEmail(rs.getString("email").trim());
                byte b = rs.getByte("enabled");
                if (b == 0){
                    uu.setEnabled(false);
                }
                else {
                    uu.setEnabled(true);
                }
                uu.setFirstName(rs.getString("first_name").trim());
                uu.setLastName(rs.getString("last_name").trim());
                Timestamp ts = rs.getTimestamp("insert_timestamp");
                if (ts != null) {
                	DateTime inserted = new DateTime(ts.getTime());
                	uu.setInsertTimestamp(inserted);
                }
                uu.setPassword(new String(rs.getBytes("password")));
                uu.setPhone(rs.getString("phone").trim());
                
                users.add(uu);
            }
        }
        catch (SQLException sqle){
            throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
        }
				
		return users;
	}
	
	/**
	 * 
	 * @param userID
	 * @param password
	 * @return The authenticated User or null if invalid id/pwd
	 * @throws UsatException
	 */
	public PortalUserTOIntf fetchUserByIDandPwd(String userID, String password) throws UsatException { 
		PortalUserTOIntf user = null;
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(PortalUserDAO.SELECT_ENABLED_USAT_USER_BY_ID_PWD);
            ps.setString(1, userID);
            ps.setBytes(2, password.getBytes());
            
            ResultSet rs = ps.executeQuery();
            
            Collection<PortalUserTOIntf> al = this.objectFactory(rs);
            
            if (al.size() > 1) {
                throw new UsatException("Unexpected Number of users found: " + al.size());
            }
            else if(al.size() == 1){
                user = al.iterator().next();
            }
			
		}
		catch (Exception e) {
			throw new UsatException("Exception logging in.", e);
		}
		finally {
			this.cleanupConnection(conn);
		}
		
		// Now pull the user role assigntments
		if (user != null) {
	        Collection<PortalUserRoleEnum> roles = this.fetchRolesAssignedToUser(userID);
	        PortalUserTO to = (PortalUserTO)user;
	        to.setAssignedRoles(roles);
		}

		return user;
	}
	
	public Collection<PortalUserTOIntf> fetchAllPortalUsers() throws UsatException {
		Collection<PortalUserTOIntf> users = null;
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(PortalUserDAO.SELECT_ALL_USAT_USERS);
            
            ResultSet rs = ps.executeQuery();
            
            users = this.objectFactory(rs);
			
		}
		catch (Exception e) {
			throw new UsatException("Exception logging in.", e);
		}
		finally {
			this.cleanupConnection(conn);
		}

		// Now pull the user role assigntments
		if (users != null && users.size() > 0) {
			for (PortalUserTOIntf u : users) {
				PortalUserTO userTO = (PortalUserTO)u;
		        Collection<PortalUserRoleEnum> roles = this.fetchRolesAssignedToUser(u.getUserID());
		        userTO.setAssignedRoles(roles);
			}
		}
		
		return users;
	}

	public Collection<PortalUserTOIntf> fetchPortalUsersByEmail(String emailAddress) throws UsatException {
		Collection<PortalUserTOIntf> users = null;
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(PortalUserDAO.SELECT_USERS_BY_EMAIL);
            ps.setString(1, emailAddress);
            ResultSet rs = ps.executeQuery();
            
            users = this.objectFactory(rs);
			
		}
		catch (Exception e) {
			throw new UsatException("Exception getting users by email ", e);
		}
		finally {
			this.cleanupConnection(conn);
		}

		// Now pull the user role assigntments
		if (users != null && users.size() > 0) {
			for (PortalUserTOIntf u : users) {
				PortalUserTO userTO = (PortalUserTO)u;
		        Collection<PortalUserRoleEnum> roles = this.fetchRolesAssignedToUser(u.getUserID());
		        userTO.setAssignedRoles(roles);
			}
		}
		
		return users;
	}
	
	private Collection<PortalUserRoleEnum> fetchRolesAssignedToUser(String userID) throws UsatException {
		Collection<PortalUserRoleEnum> roles = new ArrayList<PortalUserRoleEnum>();
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(PortalUserDAO.SELECT_USER_ROLES);
            ps.setString(1, userID);
            
            ResultSet rs = ps.executeQuery();
            
            while (rs.next()) {
            	PortalUserRoleEnum cRole = PortalUserRoleEnum.NORMAL;
            	int role = rs.getInt("role_id");
            	switch(role) {
            		case 1:
            			cRole = PortalUserRoleEnum.ADMINISTRATOR;
            			break;
            		case 3:
            			cRole = PortalUserRoleEnum.POWER;
            			break;
            		case 4:
            			cRole = PortalUserRoleEnum.LIMITED;
            			break;
            		default:
            			break;
            	}
            	roles.add(cRole);
            }
			
		}
		catch (Exception e) {
			throw new UsatException("Exception creating role during login.", e);
		}
		finally {
			this.cleanupConnection(conn);
		}
		
		return roles;
	}
}
