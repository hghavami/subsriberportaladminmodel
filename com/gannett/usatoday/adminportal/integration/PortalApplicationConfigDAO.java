package com.gannett.usatoday.adminportal.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.appConfig.to.PortalApplicationSettingTO;
import com.usatoday.UsatException;

public class PortalApplicationConfigDAO extends USATodayDAO {

	// query
	private static final String SELECT_ALL = "select id, update_count, updated, setting_group, setting_key, setting_value, label, setting_description from XTRNT_APP_CONFIG order by setting_group";
	//private static final String SELECT_SETTING_BY_KEY = "select id, update_count, updated, group, key, value, description from XTRNT_APP_CONFIG where key = ?";
	private static final String SELECT_UPDATE_COUNT_BY_ID = "select update_count from XTRNT_APP_CONFIG where id = ?";
	
	// update
	private static final String UPDATE_SETTING = "update XTRNT_APP_CONFIG set setting_group = ?, setting_key = ?, setting_value = ?, setting_description = ?, update_count = ?, updated = ?, label = ? where id = ?";
	
	private Collection<PortalApplicationSettingTO> objectFactory(ResultSet rs) throws UsatException {
        ArrayList<PortalApplicationSettingTO> al = new ArrayList<PortalApplicationSettingTO>();
        
        try {
            while (rs.next()){
                PortalApplicationSettingTO config = new PortalApplicationSettingTO();

                String tempStr = null;

                int tempInt = rs.getInt("id");
                config.setID(tempInt);
                
                tempInt = rs.getInt("update_count");
                config.setUpdateCount(tempInt);
                
                Timestamp ts = rs.getTimestamp("updated");
                if (ts != null) {
                	DateTime dt = new DateTime(ts.getTime());
                	config.setLastUpdateTime(dt);
                }
                
                tempStr = rs.getString("setting_group");
                if (tempStr == null) {
                	tempStr = "";
                }
                config.setGroup(tempStr.trim());
                
                tempStr = rs.getString("setting_key");
                if (tempStr == null) {
                	tempStr = "";
                }
                config.setKey(tempStr.trim());

                tempStr = rs.getString("setting_value");
                if (tempStr == null) {
                	tempStr = "";
                }
                config.setValue(tempStr.trim());

                tempStr = rs.getString("label");
                if (tempStr == null) {
                	tempStr = "";
                }
                config.setLabel(tempStr.trim());
                
                tempStr = rs.getString("setting_description");
                if (tempStr == null) {
                	tempStr = "";
                }
                config.setDescription(tempStr.trim());

                al.add(config);
            }
        }
        catch (SQLException sqle){
            throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
        }
        return al;
	}
	
	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
    public Collection<PortalApplicationSettingTO> getApplicationConfigurations() throws UsatException {
        Collection<PortalApplicationSettingTO> results = null;

        Connection conn = null;
        try {
        	conn = this.connectionManager.getExtranetAdminDBConnection();
        	
            PreparedStatement ps = conn.prepareStatement(PortalApplicationConfigDAO.SELECT_ALL);
            
            ResultSet rs = ps.executeQuery();
            
            results = this.objectFactory(rs);
        }
        catch (Exception e){
            throw new UsatException(e.getMessage());
        }
        finally {
            if (conn != null) {
                this.cleanupConnection(conn);
            }
        }
        return results;
    }
	
    /**
     * 
     * @param setting
     * @throws UsatException
     */
    public PortalApplicationSettingTO update(PortalApplicationSettingTO setting) throws UsatException {
    	Connection conn = null;
    	
    	if (setting == null) {
    		return setting;
    	}
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
			// SELECT_UPDATE_COUNT_BY_ID
			PreparedStatement checkUpdateStatment = conn.prepareStatement(PortalApplicationConfigDAO.SELECT_UPDATE_COUNT_BY_ID);
			checkUpdateStatment.setInt(1, setting.getID());
			
			ResultSet updateCountRS = checkUpdateStatment.executeQuery();
			
			int updateCount = -1; 
			if (updateCountRS.next()) {
				updateCount = updateCountRS.getInt(1);
			}
			
			updateCountRS.close();
			
			if (updateCount > setting.getUpdateCount()) {
				throw new UsatException("This setting is stale. It has been updated by another user. Please refresh and try again.");
			}
			
			PreparedStatement ps = conn.prepareStatement(PortalApplicationConfigDAO.UPDATE_SETTING);

			//  update XTRNT_APP_CONFIG set group = ?, key = ?, 
			//     value = ?, description = ? where id = ?
            if (setting.getGroup() != null){
            	ps.setString(1, setting.getGroup());
            }
            else {
            	ps.setNull(1, Types.CHAR);
            }
            
            if (setting.getKey() != null) {
            	ps.setString(2, setting.getKey());
            }
            else {
            	ps.setNull(2, Types.CHAR);
            }
            
            if (setting.getValue() != null) {
            	ps.setString(3, setting.getValue());
            }
            else {
            	ps.setNull(3, Types.CHAR);
            }

            if (setting.getDescription() != null) {
            	ps.setString(4, setting.getDescription());
            }
            else {
            	ps.setNull(4, Types.CHAR);
            }
            
            updateCount++;
            
            ps.setInt(5, updateCount);
	        Timestamp ts = new Timestamp(System.currentTimeMillis());
	        ps.setTimestamp(6, ts);
	        
	        if (setting.getLabel() != null){
	        	ps.setString(7, setting.getLabel());
	        }
	        else {
	        	ps.setNull(7, Types.VARCHAR);
	        }
            
            ps.setInt(8, setting.getID());
            
            int numRows = ps.executeUpdate();
            
            if (numRows != 1){
                throw new UsatException("usat_app_config_setting update failed (numrows not 1) for id: " + setting.getID());
            }
            else {
            	setting.setUpdateCount(updateCount);
            	setting.setLastUpdateTime(new DateTime(ts.getTime()));
            }
        }
		catch (Exception e) {
			throw new UsatException("Exception updating User.", e);
		}
		finally {
			this.cleanupConnection(conn);
		}
		return setting;
    }
}
