package com.gannett.usatoday.adminportal.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usatoday.adminportal.campaigns.intf.KeyCodeIntf;
import com.gannett.usatoday.adminportal.campaigns.to.KeyCodeTO;
import com.usatoday.UsatException;
import com.usatoday.businessObjects.util.KeyCodeParser;

public class XtrntkeyrtDAO extends USATodayDAO {

	private static final String SELECT_ALL = "select PubCode, ContestCode, SrcOrderCode, PromoCode, OfferDesc from XTRNTKEYRT where PubCode = ? AND Active = 'Y' order by SrcOrderCode, PromoCode, ContestCode";
	private static final String SELECT_PUB_KEYCODE = "select PubCode, ContestCode, SrcOrderCode, PromoCode, OfferDesc from XTRNTKEYRT where PubCode = ? AND SrcOrderCode = ? AND PromoCode = ? AND ContestCode = ? AND Active = 'Y'";

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private Collection<KeyCodeIntf> objectFactory(ResultSet rs) throws UsatException {
        ArrayList<KeyCodeIntf> al = new ArrayList<KeyCodeIntf>();
        
        try {
            while (rs.next()){
                KeyCodeTO p = new KeyCodeTO();

                String tempStr = null;

                tempStr = rs.getString("ContestCode");
                if (tempStr == null) {
                	tempStr = "";
                }
                p.setContestCode(tempStr.trim());
                
                tempStr = rs.getString("PubCode");
                if (tempStr == null) {
                	tempStr = "";
                }
                p.setPubCode(tempStr.trim());

                tempStr = rs.getString("SrcOrderCode");
                if (tempStr == null) {
                	tempStr = "";
                }
                p.setSourceCode(tempStr.trim());
                
                tempStr = rs.getString("PromoCode");
                if (tempStr == null) {
                	tempStr = "";
                }
                p.setPromoCode(tempStr.trim());

                tempStr = rs.getString("OfferDesc");
                if (tempStr == null) {
                	tempStr = "";
                }
                p.setOfferDescription(tempStr.trim());
                al.add(p);
            }
        }
        catch (SQLException sqle){
            throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
        }
        return al;
	}

	/**
	 * 
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
    public Collection<KeyCodeIntf> getKeycodesForPub(String pubCode) throws UsatException {
        Collection<KeyCodeIntf> results = null;

        Connection conn = null;
        try {
        	conn = this.connectionManager.getEsubProductionDBConnection(null);
        	
            PreparedStatement ps = conn.prepareStatement(XtrntkeyrtDAO.SELECT_ALL);
            ps.setString(1, pubCode);
            
            ResultSet rs = ps.executeQuery();
            
            results = this.objectFactory(rs);
        }
        catch (Exception e){
            throw new UsatException(e.getMessage());
        }
        finally {
            if (conn != null) {
                this.cleanupConnection(conn);
            }
        }
        return results;
    }
    
    //getKeycodeForPubAndKeycode(pub, keycode)
    public KeyCodeIntf getKeycodeForPubAndKeycode(String pubCode, String keycode) throws UsatException {
        Collection<KeyCodeIntf> results = null;
        KeyCodeIntf code = null;
        
        Connection conn = null;
        try {
        	KeyCodeParser parser = new KeyCodeParser(keycode);

        	conn = this.connectionManager.getEsubProductionDBConnection(null);
        	
            PreparedStatement ps = conn.prepareStatement(XtrntkeyrtDAO.SELECT_PUB_KEYCODE);
            ps.setString(1, pubCode);
            ps.setString(2, parser.getSourceCode());
            ps.setString(3, parser.getPromoCode());
            ps.setString(4, parser.getContestCode());
            
            ResultSet rs = ps.executeQuery();
            
            results = this.objectFactory(rs);
            
            if (results.size() == 1) {
            	code = results.iterator().next();
            }
            else {
            	if (results.size() > 1) {
            		throw new UsatException("Too many keycodes returned for pub/keycode: " + pubCode + "/" + keycode);
            	}
            }
        }
        catch (Exception e){
            throw new UsatException(e.getMessage());
        }
        finally {
            if (conn != null) {
                this.cleanupConnection(conn);
            }
        }
        return code;
    }
	
}
