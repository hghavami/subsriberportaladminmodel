package com.gannett.usatoday.adminportal.integration.campaigns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignTOIntf;
import com.gannett.usatoday.adminportal.campaigns.to.UsatCampaignTO;
import com.gannett.usatoday.adminportal.integration.USATodayDAO;
import com.usatoday.UsatException;

public class UsatCampaignDAO extends USATodayDAO {

	private static final String FIELDS = "campaign_id, createdBy, type, insert_timestamp, pubcode, keycode, update_timestamp, updatedBy, relativeURL, redirectURL, campaign_name, redirectToPage, campaign_state, group_name";

	// Insert 
    // built in insert method in order to retrieve primary key 
    
    // Update
    private static final String UPDATE_CAMPAIGN = "update usat_campaign set pubcode = ?, keycode = ?, update_timestamp = ?, updatedBy = ?, relativeURL = ?, redirectURL = ?, campaign_state = ?, redirectToPage = ?, group_name = ? where campaign_id = ?";
    
    // Delete
    private static final String DELETE_CAMPAIGN_BY_ID = "delete from usat_campaign where campaign_id = ?";
    private static final String DELETE_RELATED_PROMO_IMAGES = "delete from XTRNTPROMO where pubcode = ? and keycode = ?";
    
    
    // SELECTS
    private static final String SELECT_ALL_CAMPAIGNS = "select " + UsatCampaignDAO.FIELDS + " from usat_campaign";
    private static final String SELECT_GROUP_NAMES = "select distinct group_name from usat_campaign where group_name <> '' and group_name like ? order by group_name";
    private static final String SELECT_GROUP_NAMES_FOR_PUB = "select distinct group_name from usat_campaign where group_name <> '' and group_name like ? and pubcode = ? order by group_name";
    private static final String SELECT_CAMPAIGNS_IN_GROUP = "select " + UsatCampaignDAO.FIELDS + " from usat_campaign where group_name = ?";
    private static final String SELECT_CAMPAIGN_FOR_PUB_KEYCODE = "select " + UsatCampaignDAO.FIELDS + " from usat_campaign where pubcode = ? and keycode = ?";
    private static final String SELECT_CAMPAIGN_COUNT_BY_PUBCODE = "select count(*) from usat_campaign where pubcode = ?";
    private static final String SELECT_CAMPAIGN_COUNT_BY_VANITY = "select count(*) from usat_campaign where relativeURL = ?";
    private static final String SELECT_CAMPAIGN_BY_PRIMARY_KEY = "select " + UsatCampaignDAO.FIELDS + " from usat_campaign where campaign_id = ?";
    /**
     * 
     * @param rs
     * @param includeDefaultCampaigns
     * @return
     * @throws UsatException
     */
	private Collection<UsatCampaignTOIntf> objectFactory(ResultSet rs) throws UsatException {
        ArrayList<UsatCampaignTOIntf> al = new ArrayList<UsatCampaignTOIntf>();
        
        try {
            while (rs.next()){
                            
                UsatCampaignTO uc = new UsatCampaignTO();
                
                int type = rs.getInt("type");
                int state = rs.getInt("campaign_state");
                
                uc.setType(type);
                uc.setCampaignState(state);
                
                String keycode = rs.getString("keycode").trim();

                // move this logic to BO
                //if (!includeDefaultCampaigns) {
                //    if (EZPayPromo.defaultUTKeycode.equalsIgnoreCase(keycode) || EZPayPromo.defaultSWKeycode.equalsIgnoreCase(keycode)) {
                //        continue; // skip any default keycode campaigns as they are special and only accessible through power users
                //    }
                //    
                //}
                
                uc.setCreatedBy(rs.getString("createdBy").trim());
                
                Timestamp ts = rs.getTimestamp("insert_timestamp");
                if (ts != null) {
                	DateTime dt = new DateTime(ts.getTime());
                	uc.setCreatedTime(dt);
                }
                
                uc.setKeyCode(keycode);
                uc.setPubCode(rs.getString("pubcode").trim());
                
                String tempStr = rs.getString("redirectURL");
                if (tempStr != null) {
                	tempStr = tempStr.trim();
                }
                uc.setRedirectURL(tempStr);
                
                tempStr = rs.getString("redirectToPage");
                if (tempStr != null) {
                    uc.setRedirectToPage(tempStr.trim());                                	
                }
                
                tempStr = rs.getString("updatedBy");
                if (tempStr != null) {
                	tempStr = tempStr.trim();
                }
                uc.setUpdatedBy(tempStr);
                
                ts = rs.getTimestamp("update_timestamp");
                if (ts != null) {
                	DateTime dt = new DateTime(ts.getTime());
                	uc.setUpdatedTime(dt);
                }
                
                tempStr = rs.getString("relativeURL");
                if (tempStr != null) {
                    uc.setVanityURL(tempStr.trim());
                }
                
                uc.setCampaignName(rs.getString("campaign_name").trim());
                uc.setPrimaryKey(rs.getInt("campaign_id"));
                tempStr = rs.getString("group_name");
                if (tempStr != null) {
                	uc.setGroupName(tempStr.trim());
                }
                
                al.add(uc);
            }
        }
        catch (SQLException sqle){
            throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
        }
        return al;
	}

	
	/**
	 * 
	 * @return Collection of Campaign transfer objects
	 * @throws UsatException
	 */
    public Collection<UsatCampaignTOIntf> fetchAllCampaigns() throws UsatException {
        Collection<UsatCampaignTOIntf> campaigns = null;
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.SELECT_ALL_CAMPAIGNS);
            
            ResultSet rs = ps.executeQuery();
            
            campaigns = this.objectFactory(rs);
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return campaigns;
    }

    /**
     * 
     * @param pubCode
     * @return
     * @throws UsatException
     */
    public int fetchCountOfCampaignsForPub(String pubCode) throws UsatException {
    	int numCampaigns = 0;
    	Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.SELECT_CAMPAIGN_COUNT_BY_PUBCODE);
            ps.setString(1, pubCode);
            
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()){            
            	numCampaigns = rs.getInt(1);
            }
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return numCampaigns;
    }

    public int fetchCountOfCampaignsForVanity(String vanity) throws UsatException {
    	int numCampaigns = 0;
    	Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.SELECT_CAMPAIGN_COUNT_BY_VANITY);
            ps.setString(1, vanity);
            
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()){            
            	numCampaigns = rs.getInt(1);
            }
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return numCampaigns;
    }
    
    /**
     * 
     * @param nameLike
     * @param groupLike
     * @param keycodeLike
     * @param vanityLike
     * @param publication
     * @param publishingStatus
     * @param createdAfter
     * @param createdBefore
     * @return
     * @throws UsatException
     */
    public Collection<UsatCampaignTOIntf> fetchAllCampaignsMeetingSpecificCriteria(String nameLike, String groupLike, String keycodeLike, 
    		String vanityLike, String publication, String publishingStatus, DateTime createdAfter, DateTime createdBefore) throws UsatException {
        Collection<UsatCampaignTOIntf> campaigns = null;
		Connection conn = null;
		try {
			// generate query
			StringBuilder query = new StringBuilder();
			query.append("select ").append(UsatCampaignDAO.FIELDS);
			query.append(" from usat_campaign");
			
			StringBuilder whereClause = new StringBuilder();
			String scrubbedString = null;
			int numAdded = 0;
			// add name criteria
			if (nameLike != null && nameLike.trim().length() > 0) {
				scrubbedString = USATodayDAO.escapeApostrophes(nameLike.trim());
				if (numAdded > 0) {
					whereClause.append(" or");
				}
				whereClause.append(" campaign_name like '").append(scrubbedString).append("%'");
				numAdded++;
			}
			// group name
			if (groupLike != null && groupLike.trim().length() > 0) {
				scrubbedString = USATodayDAO.escapeApostrophes(groupLike.trim());
				if (numAdded > 0) {
					whereClause.append(" or");
				}
				whereClause.append(" group_name like '").append(scrubbedString).append("%'");
				numAdded++;				
			}
			// keycode
			if (keycodeLike != null && keycodeLike.trim().length() > 0) {
				scrubbedString = USATodayDAO.escapeApostrophes(keycodeLike.trim());
				if (numAdded > 0) {
					whereClause.append(" or");
				}
				whereClause.append(" keycode like '").append(scrubbedString).append("%'");
				numAdded++;				
			}
			// vanity (relativeurl)
			if (vanityLike != null && vanityLike.trim().length() > 0) {
				scrubbedString = USATodayDAO.escapeApostrophes(vanityLike.trim());
				if (numAdded > 0) {
					whereClause.append(" or");
				}
				whereClause.append(" relativeURL like '").append(scrubbedString).append("%'");
				numAdded++;				
			}
			
			// if two datest specified use and
			if (createdAfter != null && createdBefore != null) {
				if (numAdded > 0) {
					whereClause.append(" or");
				}
				whereClause.append(" (insert_timestamp > '").append(createdAfter.toString("MM/dd/yyyy")).append("'");
				whereClause.append(" and");
				whereClause.append(" insert_timestamp < '").append(createdBefore.toString("MM/dd/yyyy")).append("')");
				numAdded++;				
			}
			else {
				// created After
				if (createdAfter != null) {
					if (numAdded > 0) {
						whereClause.append(" or");
					}
					whereClause.append(" insert_timestamp > '").append(createdAfter.toString("MM/dd/yyyy")).append("'");
					numAdded++;
				}
				// created before
				if (createdBefore != null) {
					if (numAdded > 0) {
						whereClause.append(" or");
					}
					whereClause.append(" insert_timestamp < '").append(createdBefore.toString("MM/dd/yyyy")).append("'");
					numAdded++;
				}
			}
			
			// Now add the AND Clause
			
			// pub
			if (publication != null && publication.trim().length() > 0) {
				// ALL means don't filter by pub
				if (!publication.equalsIgnoreCase("ALL")) {
					scrubbedString = USATodayDAO.escapeApostrophes(publication.trim());
					if (numAdded > 0) {
						whereClause.append(" AND ");
					}
					whereClause.append(" pubcode = '").append(scrubbedString).append("' ");
					numAdded++;
				}
			}
			// publishig status
			if (publishingStatus != null && publishingStatus.trim().length() > 0) {
				// ALL means don't filter by pub status
				if (!publishingStatus.equalsIgnoreCase("ALL")) {
					scrubbedString = USATodayDAO.escapeApostrophes(publishingStatus.trim());
					if (numAdded > 0) {
						whereClause.append(" AND ");
					}
					whereClause.append(" campaign_state = ").append(scrubbedString).append(" ");
					numAdded++;
				}
			}
			
			// add the where clause if requested
			if (numAdded > 0) {
				query.append(" where").append(whereClause);
			}
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
			System.out.println("Running Advanced Search Query: " + query);
			
            PreparedStatement ps = conn.prepareStatement(query.toString());
            
            ResultSet rs = ps.executeQuery();
            
            campaigns = this.objectFactory(rs);
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return campaigns;
    }

    /**
     * 
     * @param criteria
     * @return
     * @throws UsatException
     */
    public Collection<UsatCampaignTOIntf> fetchAllCampaignsMeetingGeneralCriteria(String criteria) throws UsatException {
        Collection<UsatCampaignTOIntf> campaigns = null;
		Connection conn = null;
		try {
			if (criteria == null) {
				throw new UsatException("UsatCampaignDAO: No search criteria provided to general search.");
			}
			
			// generate query
			StringBuilder query = new StringBuilder();
			query.append("select ").append(UsatCampaignDAO.FIELDS);
			query.append(" from usat_campaign");
			
			StringBuilder whereClause = new StringBuilder();
			String scrubbedString = USATodayDAO.escapeApostrophes(criteria.trim());
			int numAdded = 0;

			// add name criteria
			if (numAdded > 0) {
				whereClause.append(" or");
			}
			whereClause.append(" campaign_name like '%").append(scrubbedString).append("%'");
			numAdded++;

			// group name
			if (numAdded > 0) {
				whereClause.append(" or");
			}
			whereClause.append(" group_name like '%").append(scrubbedString).append("%'");
			numAdded++;				
			// keycode
			if (numAdded > 0) {
				whereClause.append(" or");
			}
			whereClause.append(" keycode like '%").append(scrubbedString).append("%'");
			numAdded++;				

			// vanity (relativeurl)
			if (numAdded > 0) {
				whereClause.append(" or");
			}
			whereClause.append(" relativeURL like '%").append(scrubbedString).append("%'");
			numAdded++;				

			if (numAdded > 0) {
				whereClause.append(" or");
			}
			whereClause.append(" createdBy like '%").append(scrubbedString).append("%'");
			numAdded++;				

			if (numAdded > 0) {
				whereClause.append(" or");
			}
			whereClause.append(" updatedBy like '%").append(scrubbedString).append("%'");
			numAdded++;				
			
			// add the where clause if requested
			if (numAdded > 0) {
				query.append(" where").append(whereClause);
			}
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
			System.out.println("Running Basic Search Query: " + query);
			
            PreparedStatement ps = conn.prepareStatement(query.toString());
            
            ResultSet rs = ps.executeQuery();
            
            campaigns = this.objectFactory(rs);
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return campaigns;
    }
	
    /**
     * 
     * @param groupName
     * @return
     * @throws UsatException
     */
    public Collection<UsatCampaignTOIntf> fetchAllCampaignsInGroup(String groupName) throws UsatException {
        Collection<UsatCampaignTOIntf> campaigns = null;
		Connection conn = null;
		try {
			if (groupName == null || groupName.trim().length() == 0) {
				throw new UsatException("UsatCampaignDAO: No search group name provided to fetch all in group.");
			}
			
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.SELECT_CAMPAIGNS_IN_GROUP);
            ps.setString(1, groupName);
			
            ResultSet rs = ps.executeQuery();
            
            campaigns = this.objectFactory(rs);
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return campaigns;
    }

    /**
     * 
     * @param campaignPrimaryKey
     * @return
     * @throws UsatException
     */
    public UsatCampaignTOIntf fetchCampaign(int campaignPrimaryKey) throws UsatException {
        Collection<UsatCampaignTOIntf> campaigns = null;
        UsatCampaignTOIntf c = null;
		Connection conn = null;
		try {
			
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.SELECT_CAMPAIGN_BY_PRIMARY_KEY);
            ps.setInt(1, campaignPrimaryKey);
			
            ResultSet rs = ps.executeQuery();
            
            campaigns = this.objectFactory(rs);
            
            if (campaigns.size() == 1) {
            	c = campaigns.iterator().next();
            }
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return c;
    }

    /**
     * 
     * @param uc The campaign to delete
     * @return
     * @throws UsatException
     */
	public int delete(UsatCampaignTOIntf uc) throws UsatException {
      
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            conn.setAutoCommit(false);
            PreparedStatement psPromo = conn.prepareStatement(UsatCampaignDAO.DELETE_RELATED_PROMO_IMAGES);
            
            //pubcode = ? and keycode = ?
            psPromo.setString(1, uc.getPubCode());
            psPromo.setString(2, uc.getKeyCode());
            
            int numDeleted = psPromo.executeUpdate();
            
            PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.DELETE_CAMPAIGN_BY_ID);
            ps.setInt(1, uc.getPrimaryKey());
           
            numDeleted = ps.executeUpdate();
            
            conn.commit();
            
            return numDeleted;
            
        }
        catch (Exception e) {
            try {
                conn.rollback();
            }
            catch (Exception exp){
                System.out.println("UsatCampaignDAO::delete() - Failed to rollback trans: " + exp.getMessage());
            }
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
        	this.cleanupConnection(conn); 
        }         
	}

	/**
	 * 
	 * @param uc
	 * @throws UsatException
	 */
	public UsatCampaignTOIntf update(UsatCampaignTO uc) throws UsatException {
	
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
	        PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.UPDATE_CAMPAIGN);
	
	        //pubcode = ?, keycode = ?, update_timestamp = ?, 
	        //updatedBy = ?, relativeURL = ?, redirectURL = ?
	        // group_name = ?
	        ps.setString(1, uc.getPubCode());
	        ps.setString(2, uc.getKeyCode());
	        Timestamp ts = new Timestamp(System.currentTimeMillis());
	        ps.setTimestamp(3, ts);
	        ps.setString(4, uc.getUpdatedBy());  
	        ps.setString(5, uc.getVanityURL());
	        ps.setString(6, uc.getRedirectURL());
	        ps.setInt(7,  uc.getCampaignState());
	        ps.setString(8, uc.getRedirectToPage());
	        ps.setString(9, uc.getGroupName());
	        // where clause
	        ps.setInt(10, uc.getPrimaryKey());
	        
	        int numRows = ps.executeUpdate();
	        
	        
	        if (numRows != 1){
	            throw new UsatException("usat_campaign update failed (numrows not 1) for campaign: " + uc.getPrimaryKey());
	        }
	        
	        uc.setUpdatedTime(new DateTime(ts.getTime()));
	        
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	        throw new UsatException(e.getMessage());
	    }
        finally {
        	this.cleanupConnection(conn); 
        }
        return uc;
	}

	/**
	 * 
	 * @param uc
	 * @return The inserted record.
	 * @throws UsatException
	 */
	public UsatCampaignTOIntf insert(UsatCampaignTO uc) throws UsatException {
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
			StringBuilder sql = new StringBuilder("insert into usat_campaign (createdBy, type, insert_timestamp, pubcode, keycode, update_timestamp, updatedBy, relativeURL, redirectURL, campaign_name, campaign_state, redirectToPage, group_name) values (");
			
    		java.sql.Statement ps = conn.createStatement();

            // createdBy, type, insert_timestamp, pubcode, keycode, 
            // update_timestamp, updatedBy, relativeURL, redirectURL, campaign_name
            // group_name
            sql.append("'").append(UsatCampaignDAO.escapeApostrophes(uc.getCreatedBy())).append("', ");
            sql.append(uc.getType()).append(", ");
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            DateTime insertTS = new DateTime(ts.getTime());
    		sql.append("'").append(ts.toString());
    		sql.append("', ");

    		sql.append("'").append(UsatCampaignDAO.escapeApostrophes(uc.getPubCode())).append("', ");
    		sql.append("'").append(UsatCampaignDAO.escapeApostrophes(uc.getKeyCode())).append("', ");
    		// update timestamp
    		sql.append("'").append(ts.toString());
    		sql.append("', ");

    		sql.append("'").append(UsatCampaignDAO.escapeApostrophes(uc.getCreatedBy())).append("', ");
    		sql.append("'").append(UsatCampaignDAO.escapeApostrophes(uc.getVanityURL())).append("', ");
    		if (uc.getRedirectURL() == null) {
        		//sql.append("null, ");
    			// ape - for backward compatibility with old version of code
    			sql.append("'',");
    		}
    		else {
    			sql.append("'").append(UsatCampaignDAO.escapeApostrophes(uc.getRedirectURL())).append("', ");
    		}
    		sql.append("'").append(UsatCampaignDAO.escapeApostrophes(uc.getCampaignName())).append("', ");
    		sql.append(uc.getCampaignState()).append(", ");
    		sql.append("'").append(UsatCampaignDAO.escapeApostrophes(uc.getRedirectToPage())).append("', ");
    		sql.append("'").append(UsatCampaignDAO.escapeApostrophes(uc.getGroupName())).append("') ");
    		
    		int rowsAffected = ps.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS );

    		if (rowsAffected != 1) {
    		    throw new Exception("Campaign Failed to Insert Or other error and yet no exception condition exists: Number rows affected: " + rowsAffected);
    		}

    		ResultSet rs = ps.getGeneratedKeys();
		
    		rs.next();
    		int primaryKey = rs.getInt(1);
		
    		uc.setPrimaryKey(primaryKey);
    		uc.setCreatedTime(insertTS);
    		uc.setUpdatedTime(insertTS);
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
        	this.cleanupConnection(conn); 
        }         
        return uc;
	}

	/**
	 * 
	 * @param pubCode
	 * @param keyCode
	 * @return
	 * @throws UsatException
	 */
	public UsatCampaignTOIntf fetchForPubAndKeyCode(String pubCode, String keyCode) throws UsatException {
        Collection<UsatCampaignTOIntf> campaigns = null;
        UsatCampaignTOIntf c = null;
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.SELECT_CAMPAIGN_FOR_PUB_KEYCODE);
            ps.setString(1, pubCode);
            ps.setString(2, keyCode);
            
            ResultSet rs = ps.executeQuery();
            
            campaigns = this.objectFactory(rs);
            
            if (campaigns.size() == 1) {
            	c = campaigns.iterator().next();
            }
            else {
            	if (campaigns.size() > 1) {
            		throw new UsatException("More than one campaign returned for the publication and keycode combination.");
            	}
            }
            
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return c;		
	}
	
    public Collection<String> fetchAllCampaignGroupNamesInUse(String filter) throws UsatException {
        Collection<String> names = new ArrayList<String>();
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.SELECT_GROUP_NAMES);
            
            String filterString = null;
            if (filter != null) {
            	filterString = filter + "%";
            }
            else {
            	filterString = "%";
            }
            ps.setString(1, filterString);
            
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
            	String groupName = rs.getString(1);
            	names.add(groupName.trim());
            }
            
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return names;
    }

    /**
     * 
     * @param filter
     * @param pubCode
     * @return
     * @throws UsatException
     */
    public Collection<String> fetchAllCampaignGroupNamesInUseForPub(String filter, String pubCode) throws UsatException {
        Collection<String> names = new ArrayList<String>();
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(UsatCampaignDAO.SELECT_GROUP_NAMES_FOR_PUB);
            
            String filterString = null;
            if (filter != null) {
            	filterString = filter + "%";
            }
            else {
            	filterString = "%";
            }
            ps.setString(1, filterString);
            if (pubCode == null) {
            	ps.setNull(2, Types.CHAR);
            }
            else {
            	ps.setString(2,pubCode);
            }
            
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
            	String groupName = rs.getString(1);
            	names.add(groupName.trim());
            }
            
        }
        catch (UsatException e){
            throw e; 
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
                    
        return names;
    }
    
}
