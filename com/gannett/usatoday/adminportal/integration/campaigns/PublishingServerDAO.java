package com.gannett.usatoday.adminportal.integration.campaigns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerTOIntf;
import com.gannett.usatoday.adminportal.campaigns.to.PublishingServerTO;
import com.gannett.usatoday.adminportal.integration.USATodayDAO;
import com.usatoday.UsatException;

public class PublishingServerDAO extends USATodayDAO {

	// SQL
    private static final String FETCH_ALL_TEST = "select active, host, publicDNSName, uid, pwd, serverType, jndiLookup, jndiLookupV2, publishMethod, publishRootDir, redirectURL, redirectURL1 from usat_pub_server where serverType = 'TEST' and active = 'Y'";
    private static final String FETCH_ALL_PROD = "select active, host, publicDNSName, uid, pwd, serverType, jndiLookup, jndiLookupV2, publishMethod, publishRootDir, redirectURL, redirectURL1 from usat_pub_server where serverType = 'PROD' and active = 'Y'";
    private static final String FETCH_ALL_ACTIVE = "select active, host, publicDNSName, uid, pwd, serverType, jndiLookup, jndiLookupV2, publishMethod, publishRootDir, redirectURL, redirectURL1 from usat_pub_server where active = 'Y'";

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	protected Collection<PublishingServerTOIntf> objectFactory(ResultSet rs) throws UsatException {
        ArrayList<PublishingServerTOIntf> al = new ArrayList<PublishingServerTOIntf>();
        
        try {
            while (rs.next()){
                PublishingServerTO ps = new PublishingServerTO();
            
                // active, host, uid, pwd, serverType, 
                // publishMethod, publishRootDir, redirectURL
                String temp = rs.getString("active");
                if ("Y".equalsIgnoreCase(temp)) {
                    ps.setActive(true);
                }
                else {
                    ps.setActive(false);
                }
                
                temp = rs.getString("host");
                if (temp != null) {
                    ps.setHost(temp.trim());
                }
                
                temp = rs.getString("publicDNSName");
                if (temp != null) {
                    ps.setPublicHostURL(temp.trim());
                }
                
                temp = rs.getString("uid");
                if (temp != null) {
                    ps.setFTPUid(temp.trim());
                }
                
                temp = rs.getString("pwd");
                if (temp != null) {
                    ps.setFTPPwd(temp.trim());
                }
                
                temp = rs.getString("serverType");
                if (temp != null) {
                    ps.setServerType(temp.trim());
                }
                
                temp = rs.getString("publishMethod");
                if (temp != null) {
                    ps.setPublishingMethod(temp.trim());
                }

                temp = rs.getString("publishRootDir");  
                if (temp != null) {
                    ps.setPublishRootDir(temp.trim());
                }
                
                temp = rs.getString("redirectURL");
                if (temp != null) {
                    ps.setRedirectURL(temp.trim());
                }
                
                temp = rs.getString("redirectURL1");
                if (temp != null) {
                    ps.setRedirectURL1(temp.trim());
                }
                
                temp = rs.getString("jndiLookup");
                if (temp != null) {
                    ps.setJNDILookup(temp.trim());
                }
                
                // jndiLookupV2
                temp = rs.getString("jndiLookupV2");
                if (temp != null) {
                    ps.setJNDILookupV2(temp.trim());
                }
                
                al.add(ps);
            }
        }
        catch (SQLException sqle){
            throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
        }
        return al;
	}
	
	public Collection<PublishingServerTOIntf> fetchTestServers() throws UsatException {
        Collection<PublishingServerTOIntf> servers = null;
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(PublishingServerDAO.FETCH_ALL_TEST);
            
            ResultSet rs = ps.executeQuery();
            
            servers = this.objectFactory(rs);
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
        return servers;
	}
	
	public Collection<PublishingServerTOIntf> fetchProductionServers() throws UsatException {
        Collection<PublishingServerTOIntf> servers = null;
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(PublishingServerDAO.FETCH_ALL_PROD);
            
            ResultSet rs = ps.executeQuery();
            
            servers = this.objectFactory(rs);
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
        return servers;
	}
	public Collection<PublishingServerTOIntf> fetchAllActiveServers() throws UsatException {
        Collection<PublishingServerTOIntf> servers = null;
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(PublishingServerDAO.FETCH_ALL_ACTIVE);
            
            ResultSet rs = ps.executeQuery();
            
            servers = this.objectFactory(rs);
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
        return servers;
	}
}
