package com.gannett.usatoday.adminportal.integration.campaigns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usatoday.adminportal.campaigns.intf.PersistentImageIntf;
import com.gannett.usatoday.adminportal.campaigns.to.PersistentImageTO;
import com.gannett.usatoday.adminportal.integration.USATodayDAO;
import com.usatoday.UsatException;

public class ImageDAO extends USATodayDAO {

	private static final String FIELDS = "imageName, fileType, protected, contents ";
	private static final String SELECT_IMAGE = "select " + ImageDAO.FIELDS + "from usat_image_temp where imageName = ?";
	private static final String SELECT_IMAGE_COUNT = "select count(*) from usat_image_temp where imageName = ?";
	
	private static final String INSERT_IMAGE = "insert into usat_image_temp (imageName, fileType, inserted, contents) values (?, ?, ?, ?)";
	private static final String UPDATE_IMAGE = "update usat_image_temp set contents = ?, fileType = ? where imageName = ?";
	
	private static final String DELETE_IMAGE = "delete from usat_image_temp where imageName = ? and protected = 'N'";
	
	public ImageDAO() {
		super();
	}

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private Collection<PersistentImageIntf> objectFactory(ResultSet rs) throws UsatException {
        ArrayList<PersistentImageIntf> images = new ArrayList<PersistentImageIntf>();
        
        try {
            while (rs.next()){
            	PersistentImageTO i = new PersistentImageTO();
            	
            	String tempS = rs.getString("imageName");
            	if (tempS != null) {
            		i.setImageName(tempS.trim());
            	}
            	
            	tempS = rs.getString("fileType");
            	if (tempS != null) {
            		i.setImageFileType(tempS.trim());
            	}
            	
            	tempS = rs.getString("protected");
            	if (tempS != null) {
            		if (tempS.trim().equalsIgnoreCase("Y")) {
            			i.setProtectedImage(true);
            		}
            	}
            	
            	byte[] tempContents = rs.getBytes("contents");
            	if (tempContents != null) {
            		i.setImageContents(tempContents);
            	}
            	images.add(i);
            }
        }
        catch (SQLException sqle){
            throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
        }
        return images;
	}
	
	/**
	 * 
	 * @param imageName
	 * @return
	 * @throws UsatException
	 */
	public PersistentImageIntf fetchImageByName(String imageName) throws UsatException {
		Connection conn = null;
		PersistentImageIntf image = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(ImageDAO.SELECT_IMAGE);
            ps.setString(1, imageName);
            
            ResultSet rs = ps.executeQuery();
            
            Collection<PersistentImageIntf> images = this.objectFactory(rs);
            if (images.size() == 1) {
            	image = images.iterator().next();
            }
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
        return image;		
	}

	/**
	 * 
	 * @param imageName
	 * @return
	 * @throws UsatException
	 */
	public int fetchImageCountByName(String imageName) throws UsatException {
		Connection conn = null;
		int count = 0;
		
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
            PreparedStatement ps = conn.prepareStatement(ImageDAO.SELECT_IMAGE_COUNT);
            ps.setString(1, imageName);
            
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
            	count = rs.getInt(1);
            }
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
        return count;		
	}
	
	
	/**
	 * 
	 * @param source
	 * @throws UsatException
	 */
	public void insertImage(PersistentImageIntf source) throws UsatException {
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
		
            PreparedStatement ps = conn.prepareStatement(ImageDAO.INSERT_IMAGE);
            ps.setString(1, source.getImageName());
            ps.setString(2, source.getImageFileType());
            
            Timestamp ts = new Timestamp(System.currentTimeMillis());
            ps.setTimestamp(3,ts);
            ps.setBytes(4, source.getImageContents());

            int numInserted = ps.executeUpdate();
            
            if (numInserted != 1) {
            	throw new UsatException("ImageDAO::insertImage() Failed to insert temp image!");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
	}
	
	/**
	 * 
	 * @param imageName
	 * @return
	 * @throws UsatException
	 */
	public int deleteImage(String imageName) throws UsatException {
		Connection conn = null;
		int numDeleted = 0;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
		
            PreparedStatement ps = conn.prepareStatement(ImageDAO.DELETE_IMAGE);
            ps.setString(1, imageName);

            numDeleted = ps.executeUpdate();
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
        return numDeleted;
	}
	
	/**
	 * 
	 * @param source
	 * @return
	 * @throws UsatException
	 */
	public int updateImage(PersistentImageIntf source) throws UsatException {
		Connection conn = null;
		int numUpdated = 0;
		if (source == null) {
			return numUpdated;
		}
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
	        PreparedStatement ps = conn.prepareStatement(ImageDAO.UPDATE_IMAGE);
	        // update usat_image_temp set contents = ?, fileType = ? where imageName = ?
	        
	        if (source.getImageContents() == null) {
	        	ps.setNull(1, Types.BINARY);
	        }
	        else {
	        	ps.setBytes(1, source.getImageContents());
	        }
	        if (source.getImageFileType() == null) {
	        	ps.setNull(2, Types.CHAR);
	        }
	        else {
	        	ps.setString(2,source.getImageFileType());
	        }
	        
	        ps.setString(3, source.getImageName());
	        
	        numUpdated = ps.executeUpdate();
	        
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);   
        }
        return numUpdated;
		
	}
}
