package com.gannett.usatoday.adminportal.integration.campaigns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usatoday.adminportal.campaigns.PublishingServerBO;
import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerIntf;
import com.gannett.usatoday.adminportal.integration.USATodayDAO;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.integration.PromotionTO;

public class XtrntPromoUpdaterDAO extends USATodayDAO {

    private static final String INSERT_PROMO = "insert into XTRNTPROMO (PubCode, KeyCode, ImageType, ImageName, FulfillText, FulfillURL, AltName) values (?, ?, ?, ?, ?, ?, ?)";
    // Delete  // APE - Temporaray addition of the AB Split test to not delete those records.
    //                - should be removed once this tool suppors A/B Test configuration.
    private static final String DELETION_EXCLUSIONS = "'DELIVERY_NOTIFY'";
    private static final String DELETE_PROMO_BY_KEYCODE_PUBCODE = "delete from XTRNTPROMO where PubCode = ? AND KeyCode = ? AND (ImageType NOT IN("+ XtrntPromoUpdaterDAO.DELETION_EXCLUSIONS + "))";
    private static final String DELETE_PROMO_BY_KEYCODE_PUBCODE_IMAGETYPE = "delete from XTRNTPROMO where PubCode = ? AND KeyCode = ? AND ImageType = ?";

    // select
	private static final String SELECT_COUNT_OF_IMAGE_USE_BY_OTHER_CAMPAIGNS = "select count(*) from XTRNTPROMO where ImageName = ? AND KeyCode <> ? AND PubCode <> ?";
    private static final String SELECT_ALL_PROMOS_FOR_PUB_KEYCODE = "select PubCode, KeyCode, ImageType, ImageName, FulfillText, FulfillURL, AltName from XTRNTPROMO where PubCode = ? AND (KeyCode = ?)";
    
    public int deleteFromExtranetAdmin(String pubcode, String keycode, String imageType ) throws UsatException {
        int numDeleted = 0;
       
		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
			
			numDeleted = this.delete(pubcode, keycode, imageType, conn);
        }
		catch (UsatException ue) {
			throw ue;
		}
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());        	
		}
        finally{
        	if (conn != null){
        		this.cleanupConnection(conn);
        	}
        }
        return numDeleted;
    }

    /**
     * 
     * @param pubcode
     * @param keycode
     * @param imageType
     * @return
     * @throws UsatException
     */
    public int deleteFromTestServers(String pubcode, String keycode, String imageType ) throws UsatException {
        int numDeleted = 0;
        
		Connection conn = null;
		try {
			
			Collection<PublishingServerIntf> servers = PublishingServerBO.fetchTestServers();
			for(PublishingServerIntf server : servers) {
				try {
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookup());
				} catch (Exception exp) {
					System.out.println("failed to get JNDI connection for : " + server.getJNDILookup());
					System.out.println("attempting to get JNDI connection for v2 : " + server.getJNDILookupV2());
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookupV2());
				}
				numDeleted = this.delete(pubcode, keycode, imageType, conn);
			}
			
        }
		catch (UsatException ue) {
			throw ue;
		}
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());        	
		}
        finally{
        	if (conn != null){
        		this.cleanupConnection(conn);
        	}
        }
        return numDeleted;
    }
    
    /**
     * 
     * @param pubcode
     * @param keycode
     * @param imageType
     * @return
     * @throws UsatException
     */
    public int deleteFromProductionServers(String pubcode, String keycode, String imageType ) throws UsatException {
        int numDeleted = 0;
       
		Connection conn = null;
		try {
			
			Collection<PublishingServerIntf> servers = PublishingServerBO.fetchProductionServers();
			for(PublishingServerIntf server : servers) {
				try {
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookup());
				} catch (Exception exp) {
					System.out.println("failed to get JNDI connection for : " + server.getJNDILookup());
					System.out.println("attempting to get JNDI connection for v2 : " + server.getJNDILookupV2());
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookupV2());
				}
				numDeleted = this.delete(pubcode, keycode, imageType, conn);
			}
			
        }
		catch (UsatException ue) {
			throw ue;
		}
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());        	
		}
        finally{
        	if (conn != null){
        		this.cleanupConnection(conn);
        	}
        }
        return numDeleted;
    }

    /**
     * 
     * @param pubcode
     * @param keycode
     * @param imageType
     * @return
     * @throws UsatException
     */
    private int delete(String pubcode, String keycode, String imageType, Connection conn ) throws UsatException {
        int numDeleted = 0;
       
     
        try {        
      
            PreparedStatement ps = null;

            if (imageType == null || imageType.trim().length()== 0) {
                ps = conn.prepareStatement(XtrntPromoUpdaterDAO.DELETE_PROMO_BY_KEYCODE_PUBCODE);
                ps.setString(1, pubcode);
                ps.setString(2, keycode);
            }
            else {
                ps = conn.prepareStatement(XtrntPromoUpdaterDAO.DELETE_PROMO_BY_KEYCODE_PUBCODE_IMAGETYPE);
                ps.setString(1, pubcode);
                ps.setString(2, keycode);
                ps.setString(3, imageType);
            }
            
            numDeleted = ps.executeUpdate();
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);
        }  
     
        return numDeleted;
    }

    /**
     * 
     * @param xp
     * @param conn
     * @throws UsatException
     */
	private void insert(PromotionIntf xp, Connection conn) throws UsatException {
    	if (conn == null) {
    		throw new UsatException("XtrntPromoUpdaterDAO::insert() No database connection!");
    	}
    	
        try {        
 
            PreparedStatement ps = conn.prepareStatement(XtrntPromoUpdaterDAO.INSERT_PROMO);

            //PubCode, KeyCode, ImageType, ImageName, FulfillText, FulfillURL, AltName
            ps.setString(1, xp.getPubCode());
            ps.setString(2, xp.getKeyCode());
            ps.setString(3, xp.getType());
            ps.setString(4, xp.getName());
            ps.setString(5, xp.getFulfillText());
            ps.setString(6, xp.getFulfillUrl());
            ps.setString(7, xp.getAltName());
            
            int numRows = ps.executeUpdate();
                        
            if (numRows != 1){
                throw new UsatException("XtrntPromoUpdaterDAO::insert() insert failed (numrows not 1) for promo: " + xp.getPubCode() + ", " + xp.getKeyCode() + ", " + xp.getType());
            }
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);
        }  
	}
    
	/**
	 * 
	 * @param promotion
	 * @throws UsatException
	 */
	public void insertIntoExtranetAdmin(PromotionIntf promotion) throws UsatException {
        
        Connection conn = null;
        try {        
            conn = this.connectionManager.getExtranetAdminDBConnection();
            
            this.insert(promotion, conn);
                         
        }
        catch (UsatException ue) {
        	throw ue;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
        	if (conn != null){
        		this.cleanupConnection(conn);
        	}
        }  
	}

	/**
	 * 
	 * @param promotion
	 * @throws UsatException
	 */
	public void insertIntoTestServers(PromotionIntf promotion) throws UsatException {
        
		Connection conn = null;
		try {
			
			Collection<PublishingServerIntf> servers = PublishingServerBO.fetchTestServers();
			for(PublishingServerIntf server : servers) {
				try {
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookup());
				} catch (Exception exp) {
					System.out.println("failed to get JNDI connection for : " + server.getJNDILookup());
					System.out.println("attempting to get JNDI connection for v2 : " + server.getJNDILookupV2());
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookupV2());
				}
	            this.insert(promotion, conn);
			}
			
        }
        catch (UsatException ue) {
        	throw ue;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
        	if (conn != null){
        		this.cleanupConnection(conn);
        	}
        }  
	}

	/**
	 * 
	 * @param promotion
	 * @throws UsatException
	 */
	public void insertIntoProductionServers(PromotionIntf promotion) throws UsatException {
        
		Connection conn = null;
		try {
			
			Collection<PublishingServerIntf> servers = PublishingServerBO.fetchProductionServers();
			for(PublishingServerIntf server : servers) {
				try {
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookup());
				} catch (Exception exp) {
					System.out.println("failed to get JNDI connection for : " + server.getJNDILookup());
					System.out.println("attempting to get JNDI connection for v2 : " + server.getJNDILookupV2());
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookupV2());
				}
	            this.insert(promotion, conn);
			}
			
        }
        catch (UsatException ue) {
        	throw ue;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
        	if (conn != null){
        		this.cleanupConnection(conn);
        	}
        }  
	}
	
	/**
	 * 
	 * @param name
	 * @param keycode
	 * @return
	 * @throws UsatException
	 */
    public int fetchCountOfPromosByNameExcludingKeycode( String name, String pubCode, String keycode) throws UsatException {
        int count = 0;

		Connection conn = null;
		try {
			conn = this.connectionManager.getExtranetAdminDBConnection();
             PreparedStatement ps = conn.prepareStatement(XtrntPromoUpdaterDAO.SELECT_COUNT_OF_IMAGE_USE_BY_OTHER_CAMPAIGNS);
            
             //where Name = ? AND KeyCode <> ?
             ps.setString(1, name);
             ps.setString(2, keycode);
             ps.setString(3,pubCode);
            
             ResultSet rs = ps.executeQuery();
            
             rs.next();
             count = rs.getInt(1);
         }
         catch (Exception e) {
             e.printStackTrace();
             throw new UsatException(e.getMessage());
         }
         finally {
             this.cleanupConnection(conn);   
         }
  
         return count;        
    }

    /**
     * 
     * @param pubcode
     * @param keycode
     * @return
     * @throws UsatException
     */
    public Collection<PromotionIntf> fetchPromosByPubcodeKeycode(String pubcode, String keycode) throws UsatException {
        
        Collection<PromotionIntf> promos = null;
		Connection conn = null;
		try {
 			 conn = this.connectionManager.getExtranetAdminDBConnection();
             PreparedStatement ps = conn.prepareStatement(XtrntPromoUpdaterDAO.SELECT_ALL_PROMOS_FOR_PUB_KEYCODE);
            
             ps.setString(1, pubcode);
             ps.setString(2, keycode);
            
             ResultSet rs = ps.executeQuery();
            
             promos = this.objectFactory(rs);
                          
         }
         catch (UsatException e){
             throw e; 
         }
         catch (Exception e) {
             e.printStackTrace();
             throw new UsatException(e.getMessage());
         }
         finally {
             this.cleanupConnection(conn);   
         }
  
         return promos;        
    }

    /**
     * 
     * @param rs
     * @return
     * @throws UsatException
     */
    protected Collection<PromotionIntf> objectFactory(ResultSet rs) throws UsatException {
        ArrayList<PromotionIntf> al = new ArrayList<PromotionIntf>();
       
        try {
            while (rs.next()){
           
                PromotionTO xp = new PromotionTO();
               
                String promoType = rs.getString("ImageType");
                
                xp.setType(promoType);
                                 
                String temp = rs.getString("AltName");
                if (temp != null) {
                    xp.setAltName(temp);
                }
                
                temp = rs.getString("FulfillText");
                if (temp != null) {
                    xp.setFulfillText(temp);
                }
                
                temp = rs.getString("FulfillURL");                 
                if (temp != null) {
                    xp.setFulfillUrl(temp);
                }

                temp = rs.getString("KeyCode");
                if (temp != null) {
                    xp.setKeyCode(temp);
                }                 
               
                temp = rs.getString("PubCode");
                if (temp != null) {
                    xp.setPubCode(temp);
                }
                                 
                temp = rs.getString("ImageName");
                if (temp != null) {
                    xp.setName(temp);
                }
                                                                  
                al.add(xp);
            }
        }
        catch (SQLException sqle){
            throw new UsatException("XtrntPromoUpdaterDAO:objectFactory() :" + sqle.getMessage() + "     " + sqle.toString());
        }
        return al;
    }
    
}
