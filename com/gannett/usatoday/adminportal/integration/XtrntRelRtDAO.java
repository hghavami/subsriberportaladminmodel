package com.gannett.usatoday.adminportal.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usatoday.adminportal.campaigns.to.RelRtCodeTO;
import com.usatoday.UsatException;

public class XtrntRelRtDAO extends USATodayDAO {

	private static final String SELECT_ALL_BY_PUB_AND_RATECODE = "select PubCode, FreqofDelivery, DelMethod, PiaRateCode, RelPeriodLength, RelOfferDesc from XTRNTRELRT where PubCode = ? and PiaRateCode = ? and RelRateCode = ?";

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws UsatException
	 */
	private Collection<RelRtCodeTO> objectFactory(ResultSet rs) throws UsatException {
        ArrayList<RelRtCodeTO> al = new ArrayList<RelRtCodeTO>();
        
        try {
            while (rs.next()){
                RelRtCodeTO r = new RelRtCodeTO();

                String tempStr = null;

                tempStr = rs.getString("PubCode");
                if (tempStr == null) {
                	tempStr = "";
                }
                r.setPubCode(tempStr);
                
                tempStr = rs.getString("FreqofDelivery");
                if (tempStr == null) {
                	tempStr = "";
                }
                r.setFrequencyOfDelivery(tempStr.trim());

                tempStr = rs.getString("DelMethod");
                if (tempStr == null) {
                	tempStr = "";
                }
                r.setDeliveryMethod(tempStr.trim());
                
                tempStr = rs.getString("PiaRateCode");
                if (tempStr == null) {
                	tempStr = "";
                }
                r.setPiaRateCode(tempStr.trim());


                tempStr = rs.getString("RelPeriodLength");
                if (tempStr == null) {
                	tempStr = "";
                }
                r.setRelPeriodLength(tempStr.trim());
                
                tempStr = rs.getString("RelOfferDesc");
                if (tempStr == null) {
                	tempStr = "";
                }
                r.setRelOfferDescription(tempStr.trim());
                
                al.add(r);
            }
        }
        catch (SQLException sqle){
            throw new UsatException(sqle.getMessage() + "     " + sqle.toString());
        }
        return al;
	}

	/**
	 * 
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
    public RelRtCodeTO getRatesForPubAndRateCode(String pubCode, String rateCode) throws UsatException {
    	Collection<RelRtCodeTO> results = null;
        RelRtCodeTO result = null;

        Connection conn = null;
        try {
        	conn = this.connectionManager.getEsubProductionDBConnection(null);
        	
            PreparedStatement ps = conn.prepareStatement(XtrntRelRtDAO.SELECT_ALL_BY_PUB_AND_RATECODE);
            ps.setString(1, pubCode);
            ps.setString(2, rateCode);
            ps.setString(3, rateCode);
            
            ResultSet rs = ps.executeQuery();

            results = this.objectFactory(rs);
            
            if (results.size() == 1) {
            	result = results.iterator().next();
            }
            else {
            	if (results.size() > 1) {
            		throw new UsatException("Too many rates returned for pub/ratecode: " + pubCode + "/" + rateCode);
            	}
            	else {
            		throw new UsatException("No rates returned for pub/ratecode: " + pubCode + "/" + rateCode);
            	}
            }
        }
        catch (Exception e){
            throw new UsatException(e.getMessage());
        }
        finally {
            if (conn != null) {
                this.cleanupConnection(conn);
            }
        }
        return result;
    }
	
}
