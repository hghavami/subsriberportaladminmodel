package com.gannett.usatoday.adminportal.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Collection;

import com.gannett.usatoday.adminportal.campaigns.PublishingServerBO;
import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerIntf;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.PersistentProductIntf;

public class ProductUpdaterDAO extends USATodayDAO {

	private static final String UPDATE_PRODUCT = "UPDATE usat_product SET customer_service_phone = ?, max_days_in_future_to_fulfill = ?, hold_delay_days = ?, default_keycode = ?, expired_offer_keycode = ? WHERE id =?";
	
	/**
	 * 
	 * @param product
	 * @return
	 * @throws UsatException
	 */
    public int updateProductOnTestServers(PersistentProductIntf product) throws UsatException {
        int numUpdated = 0;
        
        if (product.getID() < 0) {
        	throw new UsatException("Cannot update a new product, must save.");
        }
        
		Connection conn = null;
		try {
			
			Collection<PublishingServerIntf> servers = PublishingServerBO.fetchTestServers();
			for(PublishingServerIntf server : servers) {
				try {
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookup());
				} catch (Exception exp) {
					System.out.println("failed to get JNDI connection for : " + server.getJNDILookup());
					System.out.println("attempting to get JNDI connection for v2 : " + server.getJNDILookupV2());
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookupV2());
				}
				numUpdated = this.update(product, conn);
			}
			
        }
		catch (UsatException ue) {
			throw ue;
		}
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());        	
		}
        finally{
        	if (conn != null){
        		this.cleanupConnection(conn);
        	}
        }
        return numUpdated;
    }
    
    /**
     * 
     * @param product
     * @return
     * @throws UsatException
     */
    public int updateProductOnProductionServers(PersistentProductIntf product ) throws UsatException {
        int numUpdated = 0;
       
		Connection conn = null;
		try {
			
			Collection<PublishingServerIntf> servers = PublishingServerBO.fetchProductionServers();
			for(PublishingServerIntf server : servers) {
				try {
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookup());
				} catch (Exception exp) {
					System.out.println("failed to get JNDI connection for : " + server.getJNDILookup());
					System.out.println("attempting to get JNDI connection for v2 : " + server.getJNDILookupV2());
					conn = this.connectionManager.getSpecifiedDBConnection(server.getJNDILookupV2());
				}
				numUpdated = this.update(product, conn);
			}
			
        }
		catch (UsatException ue) {
			throw ue;
		}
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());        	
		}
        finally{
        	if (conn != null){
        		this.cleanupConnection(conn);
        	}
        }
        return numUpdated;
    }

    private int update(PersistentProductIntf product, Connection conn ) throws UsatException {
        int numUpdated = 0;
       
     
        try {        
      
            PreparedStatement ps = null;

            ps = conn.prepareStatement(ProductUpdaterDAO.UPDATE_PRODUCT);

            //customer_service_phone = ?, max_days_in_future_to_fulfill = ?, hold_delay_days = ?, default_keycode = ?, expired_offer_keycode = ? WHERE id =?
            ps.setString(1, product.getCustomerServicePhone());
            ps.setInt(2, product.getMaxDaysInFutureBeforeFulfillment());
            ps.setInt(3, product.getHoldDaysDelay());
            ps.setString(4, product.getDefaultKeycode());
            ps.setString(5, product.getExpiredOfferKeycode());
            
            ps.setInt(6, product.getID());
            
            numUpdated = ps.executeUpdate();
            
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new UsatException(e.getMessage());
        }
        finally {
            this.cleanupConnection(conn);
        }  
     
        return numUpdated;
    }
    
}
