package com.gannett.usatoday.adminportal.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.usatoday.UsatException;

public class ProductionStatsDAO extends USATodayDAO {
	public static final String FETCH_NEW_START_COUNT = "select count(*) from XTRNTDWLD where PubCode = ? and TranRecTyp = '01'";
	public static final String FETCH_RENEWAL_COUNT = "select count(*) from XTRNTDWLD where PubCode = ? and TranRecTyp = '02'";

	/**
	 * 
	 * @param productCode
	 * @return
	 * @throws UsatException
	 */
	public int fetchNumberOfNewStarts(String productCode) throws UsatException {
		int newStarts = 0;
        Connection conn = null;
        try {
        	conn = this.connectionManager.getEsubProductionDBConnection(null);
        	
            PreparedStatement ps = conn.prepareStatement(ProductionStatsDAO.FETCH_NEW_START_COUNT);
            ps.setString(1, productCode);
            
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                newStarts = rs.getInt(1);            	
            }
        }
        catch (Exception e){
            throw new UsatException(e.getMessage());
        }
        finally {
            if (conn != null) {
                this.cleanupConnection(conn);
            }
        }
		
		return newStarts;
	}
	
	/**
	 * 
	 * @param productCode
	 * @return
	 * @throws UsatException
	 */
	public int fetchNumberOfRenewals(String productCode) throws UsatException {
		int count = 0;
        Connection conn = null;
        try {
        	conn = this.connectionManager.getEsubProductionDBConnection(null);
        	
            PreparedStatement ps = conn.prepareStatement(ProductionStatsDAO.FETCH_RENEWAL_COUNT);
            ps.setString(1, productCode);
            
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
            	count = rs.getInt(1);            	
            }
        }
        catch (Exception e){
            throw new UsatException(e.getMessage());
        }
        finally {
            if (conn != null) {
                this.cleanupConnection(conn);
            }
        }		
		return count;
	}
}
