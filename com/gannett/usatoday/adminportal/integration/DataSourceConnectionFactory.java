/*
 * Created on Mar 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.gannett.usatoday.adminportal.integration;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;

import com.usatoday.UsatException;
import com.usatoday.businessObjects.util.mail.EmailAlert;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public final class DataSourceConnectionFactory {
	
	private static boolean jndiAlertSent = false;
    

//    private static int openedConns = 0;
  //  private static int closedConns = 0;

	/**
	 * 
	 */
	public DataSourceConnectionFactory() {
		super();
	}

    public String getExatranetAdminDataSourceName(){
        //String jndiName = "java:comp/env/ExtranetAdmin"  // was like this
    	String jndiName = "jdbc/ExtranetAdmin";
        String temp = System.getProperty("com.usatoday.admindatasource");
        if (temp != null && temp.length() > 0){
            jndiName = temp.trim();
        }
        return jndiName;
    }
    public String getEsubTestDataSourceName(){
//        String jndiName = "java:comp/env/esub_test";
      String jndiName = "jdbc/esub_test";
      String temp = System.getProperty("com.usatoday.esubtestdatasource");
        if (temp != null && temp.length() > 0){
            jndiName = temp.trim();
        }
        return jndiName;
    }
    public String getEsubProdDataSourceName(){
//        String jndiName = "java:comp/env/esub_production";
        String jndiName = "jdbc/esub_production";
        String temp = System.getProperty("com.usatoday.esubproddatasource");
        if (temp != null && temp.length() > 0){
            jndiName = temp.trim();
        }
        return jndiName;
    }
    
    public java.sql.Connection getExtranetAdminDBConnection() throws UsatException {
        java.sql.Connection conn = null;
        
        try {
            String jndiName = getExatranetAdminDataSourceName();
            
            conn = getJNDIDataSource(jndiName);
            
            // if the indicator that an alert has been set, and we get here
            // then we can assume all is well again so set the flag back again
            if (DataSourceConnectionFactory.jndiAlertSent) {
                DataSourceConnectionFactory.jndiAlertSent = false;
            }
        }
        catch (UsatException dbe){
            if (DataSourceConnectionFactory.jndiAlertSent == false) {
                DataSourceConnectionFactory.jndiAlertSent = true;

                DataSourceConnectionFactory.sendAlert("Unable to establish DB connection via JNDI To Extranet Admin data source", dbe.getMessage());              
                System.out.println(dbe.getMessage());
            }
            
            // faled to obtain JNDI connection set flag to attempt a direct JDBC connection
            //conn = null;
            throw dbe;          
        }
        
        return conn;
    }
    
	public java.sql.Connection getEsubTestDBConnection(String jndiName) throws UsatException {
		java.sql.Connection conn = null;
		
		try {
            String jndiNameTemp = getEsubTestDataSourceName();
            if (jndiName != null && jndiName.trim().length() > 0) {
                jndiNameTemp = jndiName.trim();
            }
            
			conn = getJNDIDataSource(jndiNameTemp);
            
			// if the indicator that an alert has been set, and we get here
			// then we can assume all is well again so set the flag back again
			if (DataSourceConnectionFactory.jndiAlertSent) {
				DataSourceConnectionFactory.jndiAlertSent = false;
			}
		}
		catch (UsatException dbe){
			if (DataSourceConnectionFactory.jndiAlertSent == false) {
				DataSourceConnectionFactory.jndiAlertSent = true;

                DataSourceConnectionFactory.sendAlert("Unable to establish DB connection via JNDI To Esub data source", dbe.getMessage());				
				System.out.println(dbe.getMessage());
			}
			
			// faled to obtain JNDI connection set flag to attempt a direct JDBC connection
			//conn = null;
            throw dbe;			
		}
		
		return conn;
	}

	public java.sql.Connection getEsubProductionDBConnection(String jndiName) throws UsatException {
		java.sql.Connection conn = null;
		
		try {
            String jndiNameTemp = getEsubProdDataSourceName();
            if (jndiName != null && jndiName.trim().length() > 0) {
                jndiNameTemp = jndiName.trim();
            }
            
			conn = getJNDIDataSource(jndiNameTemp);
            
			// if the indicator that an alert has been set, and we get here
			// then we can assume all is well again so set the flag back again
			if (DataSourceConnectionFactory.jndiAlertSent) {
				DataSourceConnectionFactory.jndiAlertSent = false;
			}
		}
		catch (UsatException dbe){
			if (DataSourceConnectionFactory.jndiAlertSent == false) {
				DataSourceConnectionFactory.jndiAlertSent = true;

                DataSourceConnectionFactory.sendAlert("Unable to establish DB connection via JNDI To Esub data source", dbe.getMessage());				
				System.out.println(dbe.getMessage());
			}
			
			// faled to obtain JNDI connection set flag to attempt a direct JDBC connection
			//conn = null;
            throw dbe;			
		}
		
		return conn;
	}

    /**
     * 
     * @param conn
     */
	public void closeConnection(Connection conn) {
		try {
			if (conn != null) {
				if(!conn.isClosed()){
					conn.close();
				}
			}
		}
		catch (Exception e){
			System.out.println("Exception closing db connection: " + e.getMessage());
		}
	}

	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
    public java.sql.Connection getSpecifiedDBConnection(String jndiName) throws UsatException {
        java.sql.Connection conn = null;
        
        try {
            if (jndiName == null || jndiName.trim().length()== 0) {
            	throw new UsatException("No JNDI Name supplied.");
            }
            
            conn = getJNDIDataSource(jndiName);
            
            // if the indicator that an alert has been set, and we get here
            // then we can assume all is well again so set the flag back again
            if (DataSourceConnectionFactory.jndiAlertSent) {
                DataSourceConnectionFactory.jndiAlertSent = false;
            }
        }
        catch (UsatException dbe){
            if (DataSourceConnectionFactory.jndiAlertSent == false) {
                DataSourceConnectionFactory.jndiAlertSent = true;

                //DataSourceConnectionFactory.sendAlert("Unable to establish DB connection via JNDI To data source:" + jndiName + ". ", dbe.getMessage());              
                System.out.println("Unable to establish DB connection via JNDI To data source:" + jndiName + ".  " + dbe.getMessage());
            }
            
            // faled to obtain JNDI connection set flag to attempt a direct JDBC connection
            //conn = null;
            throw dbe;          
        }
        
        return conn;
    }
    
	
	/**
	 * 
	 * @param jndiName
	 * @return
	 * @throws UsatException
	 */
	private java.sql.Connection getJNDIDataSource(String jndiName)throws UsatException {
		java.sql.Connection conn = null;
		Object dataSourceObj = null;
		try {
			javax.naming.InitialContext ic = new javax.naming.InitialContext();
            
			dataSourceObj = ic.lookup(jndiName);

			if (dataSourceObj != null) {
				javax.sql.DataSource ds = (javax.sql.DataSource)dataSourceObj;

				try {
					// first try the container managed credentials
					conn = ds.getConnection();
				}
				catch (SQLException f1){
					throw f1;
				}
			}
		}
		catch (NamingException ne){	
			UsatException dbc  = new UsatException("NamingException during Data source lookup: " + ne.getExplanation(), ne);
			throw dbc;
		}
		catch (SQLException sqle){
			UsatException dbc  = new UsatException("SQL Exception during Data source lookup: ERROR CODE: " + sqle.getErrorCode() + "; ERR MESSAGE: " + sqle.getMessage() + "; SQL STATE:" + sqle.getSQLState(), sqle);
			throw dbc;
		}
		
		return conn;
	}
	
    private static void sendAlert ( String subject, String message ) {
        
        com.usatoday.businessObjects.util.mail.EmailAlert alert = new EmailAlert();
        alert.setBodyText(message);
        alert.setSubject(subject);
        String alertReceivers = System.getProperty("com.usatoday.alerts.receivers");
        if (alertReceivers == null) {
            alertReceivers = "aeast@usatoday.com";
        }
        alert.setReceiverList(alertReceivers);
        String alertSender = System.getProperty("com.usatoday.alerts.sender");
        if (alertSender == null) {
            alertSender = "DBConnectionFactory.Class@usatoday.com";
        }
        alert.setSender(alertSender);
        
        alert.sendAlert();
    }
}
