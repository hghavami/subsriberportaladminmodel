package com.gannett.usatoday.adminportal.campaigns;

import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.campaigns.intf.KeyCodeIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignTOIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditablePromotionsSetBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableCreditCardPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
import com.gannett.usatoday.adminportal.campaigns.to.PersistentImageTO;
import com.gannett.usatoday.adminportal.campaigns.to.UsatCampaignTO;
import com.gannett.usatoday.adminportal.campaigns.utils.CampaignFTPUtilities;
import com.gannett.usatoday.adminportal.campaigns.utils.CampaignPublisherFailure;
import com.gannett.usatoday.adminportal.integration.campaigns.ImageDAO;
import com.gannett.usatoday.adminportal.integration.campaigns.UsatCampaignDAO;
import com.gannett.usatoday.adminportal.integration.campaigns.XtrntPromoUpdaterDAO;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

public class UsatCampaignBO implements UsatCampaignIntf {

	private int primaryKey = -1;
	private int campaignState = 0;
	private String createdBy = null;
	private String updatedBy = null;
	private int type = 0;
	private DateTime createdTime = null;
	private DateTime updatedTime = null;
	private String pubCode = null;
	private String keyCode = null;
	private String campaignName = null;
	private String vanityURL = null;
	private String redirectURL = null;
	private String groupName = null;
	private String redirectToPage = "TERMS";
	
	private USATProductBO product = null;

	private String errorMessage = null;
	private Collection<CampaignPublisherFailure> publishFailures = null;
	
	private EditablePromotionSetIntf promotions = null;
	
	private UsatCampaignTOIntf original = null;
	
	public static final UsatCampaignBO createNewVanityCampaign() {
		UsatCampaignBO bo = new UsatCampaignBO();
		try {
			bo.setType(UsatCampaignIntf.BASIC_VANITY_URL);
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return bo;
	}
	
	private UsatCampaignBO () {
		super();
	}
	
	private UsatCampaignBO(UsatCampaignTOIntf source) {

		this.primaryKey = source.getPrimaryKey();
		this.campaignState = source.getCampaignState();
		this.createdBy = source.getCreatedBy();
		this.updatedBy = source.getUpdatedBy();
		this.type = source.getType();
		this.createdTime = source.getCreatedTime();
		this.updatedTime = source.getUpdatedTime();
		this.pubCode = source.getPubCode();
		this.keyCode = source.getKeyCode();
		this.campaignName = source.getCampaignName();
		this.vanityURL = source.getVanityURL();
		this.redirectURL = source.getRedirectURL();
		this.groupName = source.getGroupName();
		this.redirectToPage = source.getRedirectToPage();

		this.original = source;
		
		try {
			// initialize Promos
			this.promotions = EditablePromotionsSetBO.fetchPromotionSetForCampaign(this);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return The collection of all campaigns in the system
	 * @throws UsatException
	 */
	public static Collection<UsatCampaignIntf> fetchAllCampaigns() throws UsatException {
		ArrayList<UsatCampaignIntf> campaigns = new ArrayList<UsatCampaignIntf>();
		
		UsatCampaignDAO dao = new UsatCampaignDAO();
		
		Collection<UsatCampaignTOIntf> toCollection = dao.fetchAllCampaigns();

		if (toCollection != null && toCollection.size() > 0) {
			for (UsatCampaignTOIntf cTO : toCollection) {
				if (cTO.getKeyCode().equalsIgnoreCase(PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE)) {
					// skip default keycodes, they can only be fetched from the home page.
					continue;
				}
				UsatCampaignBO cBO = new UsatCampaignBO(cTO);
				campaigns.add(cBO);
			}
		}
		
		return campaigns;
	}
	
	/**
	 * 
	 * @param pubCode
	 * @return
	 * @throws UsatException
	 */
	public static int fetchCountOfCampaignsForPub(String pubCode) throws UsatException {
		UsatCampaignDAO dao = new UsatCampaignDAO();
		int numCampaigns = dao.fetchCountOfCampaignsForPub(pubCode);
		// subtract one for the default settings
		numCampaigns--;
		return  numCampaigns;
	}

	/**
	 * 
	 * @param vanity
	 * @return
	 * @throws UsatException
	 */
	public static int fetchCountOfCampaignsForVanity(String vanity) throws UsatException {
		UsatCampaignDAO dao = new UsatCampaignDAO();
		return dao.fetchCountOfCampaignsForVanity(vanity);
	}

	/**
	 * 
	 * @param nameLike
	 * @param groupLike
	 * @param keycodeLike
	 * @param vanityLike
	 * @param publication
	 * @param publishingStatus
	 * @param createdAfter
	 * @param createdBefore
	 * @return
	 * @throws UsatException
	 */
	public static Collection<UsatCampaignIntf> fetchCampaignsMeetingSpecificCriteria(String nameLike, String groupLike, String keycodeLike, String vanityLike, String publication, String publishingStatus, DateTime createdAfter, DateTime createdBefore) throws UsatException {
		ArrayList<UsatCampaignIntf> campaigns = new ArrayList<UsatCampaignIntf>();
		
		UsatCampaignDAO dao = new UsatCampaignDAO();
		
		Collection<UsatCampaignTOIntf> toCollection = dao.fetchAllCampaignsMeetingSpecificCriteria(nameLike, groupLike, keycodeLike, vanityLike, publication, publishingStatus, createdAfter, createdBefore);

		if (toCollection != null && toCollection.size() > 0) {
			for (UsatCampaignTOIntf cTO : toCollection) {
				// if working wiht a product that is a 'default' then check that no defaults show up in results.
				if (cTO.getKeyCode().equalsIgnoreCase(PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE)) {
					// skip default keycodes, they can only be fetched from the home page.
					continue;
				}
				UsatCampaignBO cBO = new UsatCampaignBO(cTO);
				campaigns.add(cBO);
			}
		}
		
		return campaigns;
	}

	public static Collection<UsatCampaignIntf> fetchCampaignsMeetingGeneralCriteria(String criteria) throws UsatException {
		ArrayList<UsatCampaignIntf> campaigns = new ArrayList<UsatCampaignIntf>();
		
		UsatCampaignDAO dao = new UsatCampaignDAO();
		
		Collection<UsatCampaignTOIntf> toCollection = dao.fetchAllCampaignsMeetingGeneralCriteria(criteria);

		if (toCollection != null && toCollection.size() > 0) {
			for (UsatCampaignTOIntf cTO : toCollection) {
				if (cTO.getKeyCode().equalsIgnoreCase(PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE)) {
					// skip default keycodes, they can only be fetched from the home page.
					continue;
				}
				UsatCampaignBO cBO = new UsatCampaignBO(cTO);
				campaigns.add(cBO);
			}
		}
		
		return campaigns;
	}

	/**
	 * 
	 * @param groupName
	 * @return
	 * @throws UsatException
	 */
	public static Collection<UsatCampaignIntf> fetchCampaignsInGroup(String groupName) throws UsatException {
		ArrayList<UsatCampaignIntf> campaigns = new ArrayList<UsatCampaignIntf>();
		
		UsatCampaignDAO dao = new UsatCampaignDAO();
		
		Collection<UsatCampaignTOIntf> toCollection = dao.fetchAllCampaignsInGroup(groupName);

		if (toCollection != null && toCollection.size() > 0) {
			for (UsatCampaignTOIntf cTO : toCollection) {
				if (cTO.getKeyCode().equalsIgnoreCase(PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE)) {
					// skip default key codes, they can only be fetched from the home page.
					continue;
				}
				UsatCampaignBO cBO = new UsatCampaignBO(cTO);
				campaigns.add(cBO);
			}
		}
		
		return campaigns;
	}

	public static UsatCampaignIntf fetchCampaignForPubAndKeycode(String pubCode, String keyCode) throws UsatException {
		UsatCampaignIntf campaign = null;
		
		if (pubCode == null || keyCode == null) {
			throw new UsatException("No publication/keycode specified");
		}
		
		UsatCampaignDAO dao = new UsatCampaignDAO();
		
		UsatCampaignTOIntf to = dao.fetchForPubAndKeyCode(pubCode, keyCode);

		if (to != null) {
			campaign = new UsatCampaignBO(to);
		}
		
		return campaign;
	}
	
	public static UsatCampaignIntf fetchUTDefaultCampaign() throws UsatException {

		String defaultKeycode = PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE;
		
		return UsatCampaignBO.fetchCampaignForPubAndKeycode("UT", defaultKeycode);
	}

	public static UsatCampaignIntf fetchSportsWeeklyDefaultCampaign() throws UsatException {
		String defaultKeycode = PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE;
		
		/*
		 * try {
			PortalApplicationRuntimeConfigurations config = new PortalApplicationRuntimeConfigurations();
			
			if (config.getSportsWeeklytDefaultKeyCode() != null) {
				defaultKeycode =  config.getSportsWeeklytDefaultKeyCode().getValue();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		*/
		return UsatCampaignBO.fetchCampaignForPubAndKeycode("BW", defaultKeycode);
	}
	
	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public static Collection<String> fetchAllCampaignGroupNames(String filter) throws UsatException {
		Collection<String> groupNames = null;
		
		UsatCampaignDAO dao = new UsatCampaignDAO();

		groupNames = dao.fetchAllCampaignGroupNamesInUse(filter);
		return groupNames;
	}
	
	/**
	 * 
	 * @return
	 * @throws UsatException
	 */
	public static Collection<String> fetchAllCampaignGroupNamesForPub(String filter, String pubCode) throws UsatException {
		Collection<String> groupNames = null;
		
		UsatCampaignDAO dao = new UsatCampaignDAO();

		groupNames = dao.fetchAllCampaignGroupNamesInUseForPub(filter, pubCode);
		return groupNames;
	}
	
	/**
	 * 
	 * @param primaryKey
	 * @return
	 * @throws UsatException
	 */
	public static UsatCampaignIntf fetchCampaign(int primaryKey) throws UsatException {

		UsatCampaignIntf campaign = null;
		UsatCampaignDAO dao = new UsatCampaignDAO();
		UsatCampaignTOIntf to = dao.fetchCampaign(primaryKey);

		if (to != null) {
			campaign = new UsatCampaignBO(to);
		}
		
		return campaign;
	}

	public void clearAllPromotionConfigs() {
		if (this.promotions != null) {
			this.promotions.clearAllPromoConfigurations();
		}
	}

	/**
	 * 
	 */
	public boolean delete() throws UsatException {
		
		if (this.primaryKey < 0 || "00000".equals(this.keyCode)) { 
			// if never saved, or default settings campaign simply return
			return false;
		}
		
		UsatCampaignDAO dao = new UsatCampaignDAO();
		UsatCampaignTO to = new UsatCampaignTO(this);
		int numRowsDeleted = dao.delete(to);

		boolean deleted = false;

		XtrntPromoUpdaterDAO promotionDAO = new XtrntPromoUpdaterDAO();
		
		if (numRowsDeleted == 1) {
			// this.setCampaignState(UsatCampaignIntf.) // deleted?
			// mark as not persisted.

			// delete XTRNTPROMO Records for pub / keycode in all databases.
			try {
				promotionDAO.deleteFromExtranetAdmin(this.pubCode, this.keyCode, null);
			}
			catch(Exception e) {
				System.out.println("Failed to delete form XTRNTPROMO from ExtranetAdmin db: " + this.campaignName);
			}
			if (this.campaignState == UsatCampaignIntf.STAGED_STATE || this.campaignState == UsatCampaignIntf.PUBLISHED_STATE
					|| this.campaignState == UsatCampaignIntf.REPUBLISH_STATE) {
				try {
					promotionDAO.deleteFromTestServers(this.pubCode, this.keyCode, null);
				}
				catch(Exception e) {
					System.out.println("Failed to delete form XTRNTPROMO from Test Servers db: " + this.campaignName);
				}
			}
			if (this.campaignState == UsatCampaignIntf.PUBLISHED_STATE
					|| this.campaignState == UsatCampaignIntf.REPUBLISH_STATE) {
				try {
					promotionDAO.deleteFromProductionServers(this.pubCode, this.keyCode, null);
				}
				catch(Exception e) {
					System.out.println("Failed to delete form XTRNTPROMO from Productoin Servers db: " + this.campaignName);
				}
			}
			
			CampaignFTPUtilities ftpUtils = new CampaignFTPUtilities();
			
			ftpUtils.cleanUpCampaignFromAllServers(this);
			
			this.primaryKey = -1;
			
			// finally clear all promotions in memory
			this.getPromotionSet().clearAllPromoConfigurations();
			
			deleted = true;
		}
		
		return deleted;
	}

	/**
	 *  Returns true if campaign is valid (basically if the vanity is available
	 */
	public boolean getIsCampaignValid() {
		
		// determine if vanity is in use if it is, it may just be that this campaign owns it
		// 
		boolean campaignValid = true;
		CampaignFTPUtilities vanityValidator = new CampaignFTPUtilities();
		
		this.errorMessage = null;
		
		boolean vanityInUse = false;
		
		// if new campaign
        if (this.primaryKey < 0) {
        	
        	if (this.vanityURL != null && this.vanityURL.trim().length() > 0) {
	        	vanityInUse = vanityValidator.isVanityInUse(this.vanityURL);
	        	if (!vanityInUse) {
					// no campaign on prod server now check if unpublished campaign is using vanity.
					try {
						Collection<UsatCampaignIntf> matchingVanityCampaigns = UsatCampaignBO.fetchCampaignsMeetingSpecificCriteria(null, null, null, this.vanityURL, null, null, null, null);
						
						for (UsatCampaignIntf tempC : matchingVanityCampaigns) {
							if (tempC.getVanityURL() != null && tempC.getVanityURL().equalsIgnoreCase(this.vanityURL)){
		   		        		this.errorMessage = "The vanity, " + this.vanityURL + ", is already in use by a campaign named: " + tempC.getCampaignName() + " pub/keycode " + tempC.getPubCode()+ "/" + tempC.getKeyCode() + ". The vanity has been set back to its original value. ";
		   		        		this.vanityURL = this.original.getVanityURL();
		   		        		campaignValid = false;
		   		        		break;
							}
							
						}
					}
					catch (Exception e) {
						e.printStackTrace();
						// assume valid if fail to find other one
						campaignValid = true;
					}
	        	}
	        	else {
	        		this.errorMessage = "The vanity, " + this.vanityURL + ", is already in use.";
	        		campaignValid = false;
	        	}
        	}
        }
        else {  // previously saved campaign
    		if (!this.getIsChanged()) {
                campaignValid = true;
            }
    		else { // something changed
    			// if vanity changed validate it
    			boolean vanityChanged = this.isVanityChanged();
    			if (vanityChanged) {
    				if (!vanityValidator.isVanityInUse(this.vanityURL)) {
    					// no campaign on prod server now check if unpublished campaign is using vanity.
    					try {
    						Collection<UsatCampaignIntf> matchingVanityCampaigns = UsatCampaignBO.fetchCampaignsMeetingSpecificCriteria(null, null, null, this.vanityURL, null, null, null, null);
    						
    						for (UsatCampaignIntf tempC : matchingVanityCampaigns) {
    							if (tempC.getVanityURL() != null && tempC.getVanityURL().equalsIgnoreCase(this.vanityURL)){
    		   		        		this.errorMessage = "The vanity, " + this.vanityURL + ", is already in use by a campaign named: " + tempC.getCampaignName() + " pub/keycode " + tempC.getPubCode()+ "/" + tempC.getKeyCode() + ". The vanity has been set back to its original value. ";
    		   		        		this.vanityURL = this.original.getVanityURL();
                					campaignValid = false;
    		   		        		break;
    							}
    							
    						}
    					}
    					catch (Exception e) {
							e.printStackTrace();
							// assume valid if fail to find other one
        					campaignValid = true;
						}
    				}
    				else {
   		        		this.errorMessage = "The vanity, " + this.vanityURL + ", is already in use. The vanity has been set back to its original value.";
   		        		this.vanityURL = this.original.getVanityURL();
   		        		campaignValid = false;
    				}
    			}
    			
    			if (!this.keyCode.equalsIgnoreCase(this.original.getKeyCode())) {
					// check for keycode in use already
					try {
						
						KeyCodeIntf code = KeyCodeBO.fetchKeycodeForPubAndKeycode(this.getPubCode(), this.getKeyCode());
						
						if (code == null) {
							campaignValid = false;
	   		        		this.errorMessage = "The keycode " + this.getKeyCode() + " was not found for this publication.";
	   		        		this.keyCode = this.original.getKeyCode();
						}
						else {
							// else code was found and it's valid
							Collection<UsatCampaignIntf> matchingPubKeycodeCampaigns = UsatCampaignBO.fetchCampaignsMeetingSpecificCriteria(null, null, this.keyCode, null, this.pubCode, null, null, null);
							
							for (UsatCampaignIntf tempC : matchingPubKeycodeCampaigns) {
								if (tempC.getPubCode().equalsIgnoreCase(this.getPubCode()) && tempC.getKeyCode().equalsIgnoreCase(this.getKeyCode())){
			   		        		this.errorMessage = "The pubcode/keycode combination already exists in another campaign named: " + tempC.getCampaignName()  + ". The keycode has been set back to its original value. ";
			   		        		this.keyCode = this.original.getKeyCode();
			   		        		campaignValid = false;
			   		        		break;
								}
								
							}
						}
					}
					catch (Exception e) {
						e.printStackTrace();
   		        		this.errorMessage = "Error validating keycode: " + this.getKeyCode() + " " + e.getMessage();
   		        		this.keyCode = this.original.getKeyCode();
						campaignValid = false;
					}
    			}
    		}
        }
        
		return campaignValid;
	}

	/**
	 * compares field by field with original to see if changed.
	 */
	public boolean getIsChanged() {
		boolean campaignChanged = false;
		
		// if no original than must be new likewise with primary key
		if (this.original == null || this.primaryKey < 0) {
			return true;
		}
		
		try {
			if (!this.campaignName.equalsIgnoreCase(original.getCampaignName())) {
				return true;
			}
		}
		catch (NullPointerException npe) {
			if (original.getCampaignName() == null && this.campaignName == null) {
				; // ignore both were null
			}
			else {
				return true;
			}
		}

		try {
			if (this.campaignState != this.original.getCampaignState()) {
				return true;
			}
		}
		catch (NullPointerException npe) {
			return true;
		}

		try {
			if ( (this.groupName == null && this.original.getGroupName() != null) ||
					(this.groupName != null && !this.groupName.equalsIgnoreCase(this.original.getGroupName()))) {
				return true;
			}
		}
		catch (NullPointerException npe) {
			return true;
		}
		
		try {
			if ( !this.keyCode.equalsIgnoreCase(this.original.getKeyCode())) {
				return true;
			}
		}
		catch (NullPointerException npe) {
			return true;
		}

		try {
			if ( !this.pubCode.equalsIgnoreCase(this.original.getPubCode())) {
				return true;
			}
		}
		catch (NullPointerException npe) {
			return true;
		}

		try {
			if ( (this.redirectToPage == null && this.original.getRedirectToPage() != null) || 
					(this.redirectToPage != null && !this.redirectToPage.equalsIgnoreCase(this.original.getRedirectToPage())) ) {
				return true;
			}
		}
		catch (NullPointerException npe) {
			return true;
		}

		try {
			if ( (this.redirectURL == null && this.original.getRedirectURL() != null) || 
					(this.redirectURL != null && !this.redirectURL.equalsIgnoreCase(this.original.getRedirectURL()))   ) {
				return true;
			}
		}
		catch (NullPointerException npe) {
			return true;
		}

		try {
			if ( this.type != this.original.getType()) {
				return true;
			}
		}
		catch (NullPointerException npe) {
			return true;
		}

		if (this.isVanityChanged()) {
			return true;
		}

		if (this.getPromotionSet().getIsChanged()) {
			return true;
		}
		
		return campaignChanged;
	}

	public EditablePromotionSetIntf getPromotionSet() {
		if (this.promotions == null) {
			this.promotions = new EditablePromotionsSetBO();
			this.promotions.setPubCode(this.getPubCode());
			this.promotions.setKeyCode(this.getKeyCode());
		}
		return promotions;
	}

	/**
	 * Validates and saves the campaign (insert or update)
	 * 
	 * @param saveCampaignOnly - if true, the XTRNTPROMO table is not updated, just the usat_campaign table (for status changes and group association changes)
	 * @throws UsatException
	 */
	public void save(boolean saveCampaignOnly) throws UsatException {
		UsatCampaignDAO dao = new UsatCampaignDAO();
		
		boolean changed = this.getIsChanged();
		boolean valid = this.getIsCampaignValid();
		
		boolean cleanupOldVanity = false;
		String oldVanity = null;
		
		// only update if it existed previously and is valid and modified
		if (this.primaryKey > -1 && changed && valid) {
			
			// pre-existing campaign
			if (this.isVanityChanged()) {
				cleanupOldVanity = true;
				oldVanity = this.original.getVanityURL();
			}
			
			// update existing
			UsatCampaignTOIntf toIntf = null;
			boolean keycodeChanged = false;
			if (!this.getKeyCode().equalsIgnoreCase(this.original.getKeyCode())) {
				keycodeChanged = true;
			}
			if (!saveCampaignOnly || this.isVanityChanged() || keycodeChanged) {
				// changed state of a pulished campaign to republish state if doing a full promo save (not just a status update or name change)
				if (this.campaignState == UsatCampaignIntf.PUBLISHED_STATE) {
					this.campaignState = UsatCampaignIntf.REPUBLISH_STATE;
				}
			}
			UsatCampaignTO to = new UsatCampaignTO(this);
			toIntf = dao.update(to);
			
			if (!saveCampaignOnly) {
				// delete and reInsert XTRNT PROMO Recrods from XTRNTADMIN DB Only
				XtrntPromoUpdaterDAO promotionDAO = new XtrntPromoUpdaterDAO();
				promotionDAO.deleteFromExtranetAdmin(this.pubCode, this.keyCode, null);
	
				// reInsert new records
				for (EditablePromotionIntf promo : this.getPromotionSet().getPromoConfigsCollection()) {
					if (promo instanceof EditableCreditCardPromotionIntf) {
						// special handling for credit cards
						EditableCreditCardPromotionIntf cCard = (EditableCreditCardPromotionIntf)promo;
						Collection<EditablePromotionIntf> cards = cCard.getCardPromosForSaving();
						for (EditablePromotionIntf card : cards) {
							promotionDAO.insertIntoExtranetAdmin(card);
						}
					}
					else {
						promotionDAO.insertIntoExtranetAdmin(promo);
					}
				}
			}

			Collection<EditableImagePromotionIntf> images = this.getPromotionSet().getAllImagePromotions();
			if (images.size() > 0) {
				ImageDAO iDao = new ImageDAO();
				for (EditableImagePromotionIntf i : images) {
					try {
						if (i.getImageFileType() == null) {
							// ignore anything that doesn't have a file type specified
							continue;
						}
						PersistentImageTO iTO = new PersistentImageTO();
						iTO.setImageContents(i.getImageContents());
						iTO.setImageFileType(i.getImageFileType());
						iTO.setImageName(i.getImagePathString());
						
						// update the cover graphics images otherwise delete and reinsert
						if ((i.getImagePathString().indexOf(EditableImagePromotionIntf.UT_COVER_IMAGE_NAME) > -1) || (i.getImagePathString().indexOf(EditableImagePromotionIntf.SW_COVER_IMAGE_NAME) > -1)) {
							iDao.updateImage(iTO);
						}
						else {
							// delete any old image with this name
							iDao.deleteImage(i.getImagePathString());
							// insert the new
							iDao.insertImage(iTO);
						}
					}
					catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
			}
			this.updatedTime = toIntf.getUpdatedTime();
			// reset the original
			this.original = new UsatCampaignTO(this);
			
			if (cleanupOldVanity) {
				CampaignFTPUtilities ftpUtil = new CampaignFTPUtilities();
				ftpUtil.cleanUpOldVanityFromAllServers(oldVanity);
			}
		}
		else {
			if (valid && this.primaryKey < 0) {
				// insert new
				UsatCampaignTO to = new UsatCampaignTO(this);
				UsatCampaignTOIntf toIntf = null;
				toIntf = dao.insert(to);

				if (!saveCampaignOnly) {
					// delete and reInsert XTRNT PROMO Recrods from XTRNTADMIN DB Only
					XtrntPromoUpdaterDAO promotionDAO = new XtrntPromoUpdaterDAO();
					promotionDAO.deleteFromExtranetAdmin(this.pubCode, this.keyCode, null);
	
					// reInsert new records
					for (EditablePromotionIntf promo : this.getPromotionSet().getPromoConfigsCollection()) {
						if (promo instanceof EditableCreditCardPromotionIntf) {
							// special handling for credit cards
							EditableCreditCardPromotionIntf cCard = (EditableCreditCardPromotionIntf)promo;
							Collection<EditablePromotionIntf> cards = cCard.getCardPromosForSaving();
							for (EditablePromotionIntf card : cards) {
								promotionDAO.insertIntoExtranetAdmin(card);
							}
						}
						else {
							promotionDAO.insertIntoExtranetAdmin(promo);
						}
					}
				}
				
				// Save images locally.
				Collection<EditableImagePromotionIntf> images = this.getPromotionSet().getAllImagePromotions();
				if (images.size() > 0) {
					ImageDAO iDao = new ImageDAO();
					for (EditableImagePromotionIntf i : images) {
						try {
							if (i.getImageFileType() == null) {
								// ignore anything that doesn't have a file type specified
								continue;
							}
							PersistentImageTO iTO = new PersistentImageTO();
							iTO.setImageContents(i.getImageContents());
							iTO.setImageFileType(i.getImageFileType());
							iTO.setImageName(i.getImagePathString());
							// delete any old image with this name
							iDao.deleteImage(i.getImagePathString());
							// insert the new
							iDao.insertImage(iTO);
						}
						catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}
				}
				
				this.primaryKey = toIntf.getPrimaryKey();
				this.createdTime = toIntf.getCreatedTime();
				this.updatedTime = toIntf.getUpdatedTime();
				
				// reset the original
				this.original = new UsatCampaignTO(this);
			}
		}

	}

	public void setCampaignGroup(String groupNameInput) throws UsatException {
		if (groupNameInput != null) {
			String temp = groupNameInput.trim();
			if (temp.length() > 60) {
				throw new UsatException("Group Name cannot be more than 60 characters.");
			}
			this.groupName = temp;
		}
		else {
			this.groupName = "";
		}
	}

	public void setCampaignName(String name) throws UsatException {
		if (name != null) {
			String temp = name.trim();
			if (temp.length() > 60) {
				throw new UsatException("Campaign Name cannot be more than 60 characters.");
			}
			this.campaignName = temp;
		}
		else {
			this.campaignName = "";
		}
	}

	public void setCampaignState(int state) throws UsatException {
		this.campaignState = state;
	}

	public void setCreatedBy(String userID) throws UsatException {
		this.createdBy = userID;
	}

	public void setCreatedTime(DateTime created) throws UsatException {
		this.createdTime = created;
	}

	public void setKeyCode(String keyCode) throws UsatException {
		this.keyCode = keyCode;
	}

	public void setPromotionSet(EditablePromotionSetIntf newSet) {
		if (this.promotions != null && this.promotions != newSet) {
			this.promotions.clearAllPromoConfigurations();
		}
		// make a copy of the set
		this.promotions = new EditablePromotionsSetBO(newSet);
	}

	public void setPubCode(String publication) throws UsatException {
		if (publication != null) {
			this.pubCode = publication.trim();
		}
	}

	public void setRedirectToPage(String page) throws UsatException {
		if (page == null) {
			throw new UsatException("Invalid Redirect To Page.");
		}
		if (page.equalsIgnoreCase("TERMS")) {
			this.redirectToPage = "TERMS";
		}
		else {
			this.redirectToPage = "WELCOME";
		}
	}

	public void setRedirectURL(String redirectURL) throws UsatException {
		this.redirectURL = redirectURL;
	}

	public void setType(int campaignType) throws UsatException {
		this.type = campaignType;
	}

	public void setUpdatedBy(String userID) throws UsatException {
		this.updatedBy = userID;
	}

	public void setUpdatedTime(DateTime lastUpdated) throws UsatException {
		this.updatedTime = lastUpdated;
	}

	public void setVanityURL(String vanity) throws UsatException {
		this.vanityURL = vanity;
	}

	public String getCampaignName() {
		return this.campaignName;
	}

	public int getCampaignState() {
		return this.campaignState;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public DateTime getCreatedTime() {
		return this.createdTime;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public int getPrimaryKey() {
		return this.primaryKey;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public String getRedirectToPage() {
		return this.redirectToPage;
	}

	public String getRedirectURL() {
		return this.redirectURL;
	}

	public int getType() {
		return this.type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public DateTime getUpdatedTime() {
		return this.updatedTime;
	}

	public String getVanityURL() {
		return this.vanityURL;
	}

	public String getCampaignStateDescription() {
		String statusString = "New";
		switch (this.campaignState) {
		case UsatCampaignIntf.PUBLISHED_STATE:
			statusString = "In Production";
			break;
		case UsatCampaignIntf.REPUBLISH_STATE:
			statusString = "Republish";
			break;
		case UsatCampaignIntf.STAGED_STATE:
			statusString = "In Test";
			break;

		default:
			break;
		}
		return statusString;
	}

	private boolean isVanityChanged() {
		boolean vanityChanged = false;
		try {
			if ( (this.vanityURL == null && this.original.getVanityURL() != null) || 
					!this.vanityURL.equalsIgnoreCase(this.original.getVanityURL())) {
				vanityChanged = true;
			}
		}
		catch (NullPointerException npe) {
			vanityChanged = true;
		}		
		return vanityChanged;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public Collection<CampaignPublisherFailure> getPublishFailures() {
		if (publishFailures == null) {
			publishFailures = new ArrayList<CampaignPublisherFailure>();
		}
		return publishFailures;
	}

	public void setPublishFailures(
			Collection<CampaignPublisherFailure> publishFailures) {
		this.publishFailures = publishFailures;
	}

	public USATProductBO getProduct() throws UsatException {
		if (this.product == null) {
			if (this.pubCode != null) {
				this.product = USATProductBO.fetchProduct(this.pubCode);
			}
		}
		return product;
	}
}
