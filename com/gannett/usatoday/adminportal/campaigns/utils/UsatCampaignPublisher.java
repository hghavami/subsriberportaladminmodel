package com.gannett.usatoday.adminportal.campaigns.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.enterprisedt.net.ftp.FileTransferClient;
import com.enterprisedt.net.ftp.FileTransferOutputStream;
import com.enterprisedt.net.ftp.WriteMode;
import com.enterprisedt.util.debug.Level;
import com.enterprisedt.util.debug.Logger;
import com.gannett.usatoday.adminportal.appConfig.PortalApplicationRuntimeConfigurations;
import com.gannett.usatoday.adminportal.appConfig.intf.PortalApplicationSettingIntf;
import com.gannett.usatoday.adminportal.campaigns.PublishingServerBO;
import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableCreditCardPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionIntf;
import com.gannett.usatoday.adminportal.integration.campaigns.XtrntPromoUpdaterDAO;
import com.usatoday.UsatException;
import com.usatoday.businessObjects.util.mail.SmtpMailSender;
import com.usatoday.util.constants.UsaTodayConstants;

public class UsatCampaignPublisher extends CampaignFTPUtilities {

	/**
	 * 
	 * @param campaigns
	 * @return
	 * @throws UsatException
	 */
	public Collection<CampaignPublisherFailure> publishCampaignsToTestServers(Collection<UsatCampaignIntf> campaigns) throws UsatException { 
		ArrayList<CampaignPublisherFailure> failures = new ArrayList<CampaignPublisherFailure>();

		Collection<PublishingServerIntf> testServers = PublishingServerBO.fetchTestServers();

		// iterate over campaigns and publish to test servers
		for (UsatCampaignIntf c : campaigns) {
			boolean promoRecordsInserted = false;
			try {
				XtrntPromoUpdaterDAO promoDAO = new XtrntPromoUpdaterDAO();
				// clear out any existing promotions
				promoDAO.deleteFromTestServers(c.getPubCode(), c.getKeyCode(), null);
				for (EditablePromotionIntf promo : c.getPromotionSet().getPromoConfigsCollection()) {
					if (promo instanceof EditableCreditCardPromotionIntf) {
						// special handling for credit cards
						EditableCreditCardPromotionIntf cCard = (EditableCreditCardPromotionIntf)promo;
						Collection<EditablePromotionIntf> cards = cCard.getCardPromosForSaving();
						for (EditablePromotionIntf card : cards) {
							promoDAO.insertIntoTestServers(card);
						}
					}
					else {
						promoDAO.insertIntoTestServers(promo);
					}					
				}
				promoRecordsInserted = true;
				
				for (PublishingServerIntf server : testServers) {

					if (PublishingServerIntf.FTP_PUBLISH.equalsIgnoreCase(server.getPublishingMethod())) {
						Collection<CampaignPublisherFailure> errors = this.ftpToServer(server, c);  // change signature as needed
						if (errors.size() > 0) {
							c.setPublishFailures(errors);
							failures.addAll(errors);
						}
					}
					else {
						// file copy
						Collection<CampaignPublisherFailure> errors = this.writeCampaignHTMLToDisk(server, c);  // change signature as needed
						if (errors.size() > 0) {
							c.setPublishFailures(errors);
							failures.addAll(errors);
						}
					}
					//clear the cache on the target server
					this.clearServerCache(server);
				} // end for each publishing server
				
	            if (c.getCampaignState() == UsatCampaignIntf.NEW_STATE || 
	            		c.getCampaignState() ==  UsatCampaignIntf.STAGED_STATE)  {               
	                    c.setCampaignState(UsatCampaignIntf.STAGED_STATE);
                }   
                else {
                   	c.setCampaignState(UsatCampaignIntf.REPUBLISH_STATE);
                }		  
	                       
	            // update status of campaign only
                c.save(true);				
				
			}
			catch (Exception e) {
				CampaignPublisherFailure f = new CampaignPublisherFailure();
				f.setCampaign(c);
				f.setErrorReason(e.getMessage());
				f.setUnderlyingException(e);
				StringBuilder msgStr = new StringBuilder();
				msgStr.append("Note the failed campaign and contact USA TODAY IT for assistance. ");
				if (promoRecordsInserted) {
					msgStr.append(" Note that records were inserted into the Promotion table.");
				}
				f.setRecommendedAction(msgStr.toString());
				failures.add(f);
			}
		} // end for each campaign
		
		
		
		return failures;
	}
	
	/**
	 * 
	 * @param campaigns
	 * @return
	 * @throws UsatException
	 */
	public Collection<CampaignPublisherFailure> publishCampaignsToProductionServers(Collection<UsatCampaignIntf> campaigns) throws UsatException { 
		ArrayList<CampaignPublisherFailure> failures = new ArrayList<CampaignPublisherFailure>();

		Collection<PublishingServerIntf> prodServers = PublishingServerBO.fetchProductionServers();

		// iterate over campaigns and publish to test servers
		for (UsatCampaignIntf c : campaigns) {
			boolean promoRecordsInserted = false;
			try {
				XtrntPromoUpdaterDAO promoDAO = new XtrntPromoUpdaterDAO();
				// clear out any existing promotions
				promoDAO.deleteFromProductionServers(c.getPubCode(), c.getKeyCode(), null);
				for (EditablePromotionIntf promo : c.getPromotionSet().getPromoConfigsCollection()) {
					if (promo instanceof EditableCreditCardPromotionIntf) {
						// special handling for credit cards
						EditableCreditCardPromotionIntf cCard = (EditableCreditCardPromotionIntf)promo;
						Collection<EditablePromotionIntf> cards = cCard.getCardPromosForSaving();
						for (EditablePromotionIntf card : cards) {
							promoDAO.insertIntoProductionServers(card);
						}
					}
					else {
						promoDAO.insertIntoProductionServers(promo);
					}
				}
				promoRecordsInserted = true;
				
				for (PublishingServerIntf server : prodServers) {

					if (PublishingServerIntf.FTP_PUBLISH.equalsIgnoreCase(server.getPublishingMethod())) {
						Collection<CampaignPublisherFailure> errors = this.ftpToServer(server, c);  // change signature as needed
						if (errors.size() > 0) {
							c.setPublishFailures(errors);
							failures.addAll(errors);
						}
					}
					else {
						// file copy
						Collection<CampaignPublisherFailure> errors = this.writeCampaignHTMLToDisk(server, c);  // change signature as needed
						if (errors.size() > 0) {
							c.setPublishFailures(errors);
							failures.addAll(errors);
						}
					}
					//clear the cache on the target server
					this.clearServerCache(server);					
					
				} // end for each publishing server
				
				c.setCampaignState(UsatCampaignIntf.PUBLISHED_STATE);
	                            
				// update status of campaign only
                c.save(true);				
				
			}
			catch (Exception e) {
				CampaignPublisherFailure f = new CampaignPublisherFailure();
				f.setCampaign(c);
				f.setErrorReason(e.getMessage());
				f.setUnderlyingException(e);
				StringBuilder msgStr = new StringBuilder();
				msgStr.append("Note the failed campaign and contact USA TODAY IT for assistance. ");
				if (promoRecordsInserted) {
					msgStr.append(" Note that records were inserted into the Promotion table.");
				}
				f.setRecommendedAction(msgStr.toString());
				failures.add(f);
			}
		} // end for each campaign
		
		this.sendEmailAlert(campaigns);
		
		return failures;
	}
	
	/**
	 * 
	 * @param ps
	 * @param c
	 * @return
	 */
	private Collection<CampaignPublisherFailure> ftpToServer(PublishingServerIntf ps, UsatCampaignIntf c) {
		ArrayList<CampaignPublisherFailure> failures = new ArrayList<CampaignPublisherFailure>();
		
		com.enterprisedt.net.ftp.FileTransferClient ftp = null;

		if (UsaTodayConstants.debug) {
			Logger.setLevel(Level.DEBUG);
		}
		else {
			Logger.setLevel(Level.FATAL);
		}
		
		try {

			ftp = new FileTransferClient();

			ftp.getAdvancedFTPSettings().setConnectMode(FTPConnectMode.PASV);
			
			ftp.setRemoteHost(ps.getHost());
			ftp.setPassword(ps.getFTPPwd());
			ftp.setUserName(ps.getFTPUid());
			
			ftp.connect();
			
			ftp.setContentType(FTPTransferType.ASCII);

			if (ps.getPublishRootDir() != null && ps.getPublishRootDir().trim().length() > 0) {
				ftp.changeDirectory(ps.getPublishRootDir().trim());
			}

			// if a vanity URL exists for campaign
			if (c.getVanityURL() != null && c.getVanityURL().trim().length() > 0) {
				try {
					ftp.createDirectory(c.getVanityURL());
				}
				catch (FTPException ftpe){
					// ignore if the directory already exists.
					if (!(ftpe.getReplyCode() == 550 || ftpe.getReplyCode() == 521) ) {
                        System.out.println("FTP Error Code: " + ftpe.getReplyCode() + " Message: " + ftpe.getMessage());
						throw ftpe;
					}
				}

				ftp.changeDirectory(c.getVanityURL());

				// new
				String html = null;
                if (c.getRedirectToPage().equals("WELCOME") || c.getRedirectToPage().trim().equals("")) {
                     html = this.generateIndexHTMLContents(ps.getRedirectURL(), c);                    
                }
                else if (c.getRedirectToPage().equals("TERMS")) {
                    html = this.generateIndexHTMLContents(ps.getRedirectURL1(), c);                    
                }
            
                
				OutputStream out = ftp.uploadStream("index.html", WriteMode.OVERWRITE); 
				try {
					out.write(html.getBytes());
				}
				finally {
				    out.close();
				}
            
				// ftp images
				ftp.changeToParentDirectory();
			} // end if a vanity URL exists
        
			ftp.changeDirectory("images/marketingimages");
        
			// set up passive ASCII transfers
			System.out.println("Setting up BINARY transfers");
			ftp.setContentType(FTPTransferType.BINARY);

			Collection<EditableImagePromotionIntf> images = c.getPromotionSet().getAllImagePromotions();
			if (images != null && images.size() > 0) {
				
				for (EditableImagePromotionIntf xp : images) {
					// possible no image setting...
					if (xp.getImagePathString().trim().length()== 0 || (xp.getImagePathString().indexOf("shim.gif")>0)) {
						continue;
					}
                
					java.io.File tempFile = new java.io.File(xp.getImagePathString());

					String sourceImagePathName = tempFile.getName();
					//String sourceDir = ConfigurePromotionServlet.getLocalPreviewImagePublishDir();
					System.out.println("Name = " + sourceImagePathName);
					
					boolean alreadyPublshed = this.checkImageOnServer(ps, sourceImagePathName);
					
					if (alreadyPublshed) {
						// overwrite if different
						byte [] existingImage = this.getImageFromServer(ps, sourceImagePathName);
						if (existingImage == null || (existingImage.length != xp.getImageContents().length)) {
							ArrayList<String> tempImages = new ArrayList<String>();
							tempImages.add(xp.getImagePathString());
							this.deleteImagesFromServer(ps, tempImages);
							alreadyPublshed = false;
						}
					}
					// if a source file is found then publish it
					if (!alreadyPublshed) {
						FileTransferOutputStream fots = ftp.uploadStream(sourceImagePathName);
//						ftp.uploadFile(tempFile.getAbsolutePath(), tempFile.getName());
						try {
							fots.write(xp.getImageContents());
						}
						catch (Exception e) {
							CampaignPublisherFailure f = new CampaignPublisherFailure();
							f.setCampaign(c);
							f.setErrorReason("The image " + xp.getImagePathString() + " could not be sent to the server: " + ps.getHost());
							f.setRecommendedAction("Edit the campaign details and upload a new copy of the image. You may need to rename it.");
							failures.add(f);
						}
						finally {
							fots.close();
						}
						
					}
					
				} // end for more images
			} // end if need to process images
                    
			try {
				ftp.disconnect(true);
			}
			catch (Exception e) {
				// can't do anything about this, but don't report error
			}
                                
		}
		catch (Exception e) {
			if (ftp != null && ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (Exception exp) {}
			}
			CampaignPublisherFailure f = new CampaignPublisherFailure();
			f.setCampaign(c);
			f.setErrorReason(e.getMessage());
			f.setUnderlyingException(e);
			f.setRecommendedAction("Failed to fully FTP Publish the campaign. If promblem persists contact IT for asssitance.");
			failures.add(f);
		}
		
		
		return failures;
	}

	/**
	 * 
	 * @param out
	 * @param redirectURL
	 * @param c
	 */
    private String generateIndexHTMLContents(String redirectURL, UsatCampaignIntf c) {
        String cName = c.getCampaignName();
        if (cName != null) {
            cName = cName.replaceAll("-", " ");
            cName = cName.trim();
        }

        StringBuilder html = new StringBuilder();
        
        html.append("<html><head>\n");
        
        // google analytics
        html.append("<script type=\"text/javascript\">var _gaq = _gaq || [];_gaq.push(['_setAccount', 'UA-23158809-1']);_gaq.push(['_trackPageview']);(function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })();</script>\n");
        
            
        html.append("<META HTTP-EQUIV=\"EXPIRES\" CONTENT=\"0\"> </head>\n");

        html.append("<body onload=\"javascript:setTimeout('doRedirect()', 200);\">\n");
        
        html.append("<!-- SiteCatalyst code version: H.10. Copyright 1997-2007 Omniture, Inc. More info available at  http://www.omniture.com --> \n");

        html.append("<script language=\"JavaScript\" src=\"/include/s_code.js\"></script>\n");
        
        html.append("<script language=\"JavaScript\"><!-- \n");
    	
        html.append("s.pageName=\"Vanity URL : "+ cName +"\"\n");
        
        html.append("s.server=\"\"\n");
        
        html.append("s.channel=\"\"\n");
        
        html.append("s.pageType=\"\"\n");
        
       // html.append("s.prop1=\"\"\n");
       // html.append("s.prop2=\"\"\n");
       // html.append("s.prop3=\"\"\n");
       // html.append("s.prop4=\"\"\n");
       // html.append("s.prop5=\"\"\n");
        html.append("s.prop41=\"" + c.getKeyCode() +" : Vanity URL : "+ cName +"\"\n");
       // html.append("s.prop42=\"\"\n");

        html.append("/* E-commerce Variables */\n");
        
    	// set up campaign tracking variable
        html.append("s.campaign=\""+ c.getKeyCode() +"\"\n");
     //   html.append("s.events=\"\"\n");
     //   html.append("s.products=\"\"\n");
     //   html.append("s.purchaseID=\"\"\n");
     //   html.append("s.eVar1=\"\"\n");
     //   html.append("s.eVar2=\"\"\n");
     //   html.append("s.eVar3=\"\"\n");
     //   html.append("s.eVar4=\"\"\n");
     //   html.append("s.eVar5=\"\"\n");

        html.append("var s_code=s.t();if(s_code)document.write(s_code)\n//--></script>\n<script language=\"JavaScript\"><!-- \n");
        html.append("if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\\!-'+'-')//--></script><noscript><a href=\"http://www.omniture.com\" title=\"Web Analytics\"><img src=\"https://102.112.2O7.net/b/ss/ganusatodaycirc/1/H.10--NS/0\" height=\"1\" width=\"1\" border=\"0\" alt=\"\" /></a></noscript><!--/DO NOT REMOVE/--> \n");
        html.append("<!-- End SiteCatalyst code version: H.10. -->\n");
        html.append("<script language=\"JavaScript\">\n");
        html.append("function doRedirect() {\n");
        //html.append("   document.location = \"" + redirectURL + "?pub=");
        html.append("   window.location.replace(\"" + redirectURL + "?pub=");
        
        html.append(c.getPubCode());
        
        html.append("&keycode=");
        
        html.append(c.getKeyCode());
        
        html.append("\");\n");
        html.append("}\n");
        html.append("</script>\n");
        html.append("<noscript>\n");
        html.append("<p><font face=\"Arial\">We're sorry.&nbsp;  Your browser either does not support JavaScript");
        html.append(" or it has been turned off.&nbsp;  Our website will not function properly without JavaScript enabled.&nbsp;");
        html.append("    Please call 1-800-USA-0001 Mon. - Fri. 6:30AM - 12:00AM Eastern to process your subscription request.&nbsp;  Thank you.</font></p>\n");
        html.append("</noscript>\n");

        /*
        html.append("<br><br><a href=\"/subscriptions/order/checkout.faces?pub=");
        html.append(c.getPubCode());
        html.append("&keycode=");
        html.append(c.getKeyCode());
        html.append("\">If you are not automatically redirected within 3 seconds, follow this link to view the offer page</a>");
		*/
        
        html.append("</body>\n");
        html.append("</html>");

        return html.toString();
    }

    /**
     * 
     * @param c
     * @param ps
     * @throws UsatException
     */
    private Collection<CampaignPublisherFailure> writeCampaignHTMLToDisk(PublishingServerIntf ps, UsatCampaignIntf c) throws UsatException {
		ArrayList<CampaignPublisherFailure> failures = new ArrayList<CampaignPublisherFailure>();
        // imageDir not required for this type of campaign
        try {
            String temp = ps.getPublishRootDir();
            if (!temp.endsWith(java.io.File.separator)) {
                temp = temp + java.io.File.separator;
            }

            // if a vanity exists then make the requiremed directory            
            if (c.getVanityURL().trim().length() > 0) {
                java.io.File f = new java.io.File(temp + c.getVanityURL());
                
                if (!f.exists()) {
                    f.mkdir();
                }
                f = new java.io.File(f.getAbsolutePath() + java.io.File.separator + "index.html");
                if (!f.exists()) {
                    f.createNewFile();
                }
                
                FileOutputStream fileOS = new FileOutputStream(f);
                
                String html = null;
                if (c.getRedirectToPage().equals("WELCOME") || c.getRedirectToPage().trim().equals("")) {
                    html = this.generateIndexHTMLContents(ps.getRedirectURL(), c);                    
                }
                else if (c.getRedirectToPage().equals("TERMS")) {
                    html = this.generateIndexHTMLContents(ps.getRedirectURL1(), c);                    
                }
                
                fileOS.write(html.getBytes());
                
                fileOS.close();
            }
            
			URL url = this.getClass().getResource("/images/marketingimages");
			System.out.println("SourceDir = '" + url.toExternalForm() + "'");

			String tempImageDir = url.toExternalForm();
            
            StringBuffer imageDir = new StringBuffer(temp);
            imageDir.append("images/marketingimages/");
            this.copyPromoImages(tempImageDir, imageDir.toString(), c);

        }
        catch (Exception e){
        	CampaignPublisherFailure f = new CampaignPublisherFailure();
        	f.setCampaign(c);
        	f.setErrorReason(e.getMessage());
        	f.setUnderlyingException(e);
        	f.setRecommendedAction("Report the problem to USA TODAY IT Businsess Solutions Extranet team.");
            failures.add(f);
        }    
        return failures;
    }    

    /**
     * 
     * @param sourceDir
     * @param destDir
     * @param c
     */
    private void copyPromoImages(String sourceDir, String destDir, UsatCampaignIntf c) {
        
    	Collection<EditableImagePromotionIntf> imagePromos = c.getPromotionSet().getAllImagePromotions();
    	
        if (imagePromos == null || imagePromos.size() == 0) {
            return;
        }

        for(EditableImagePromotionIntf xp : imagePromos) {
            // if an image type of promo
               
            java.io.File tempFile = new java.io.File(xp.getName());
            
            String extranetImagePathName = null;
            if (destDir.endsWith(java.io.File.separator)) {
                extranetImagePathName = destDir + tempFile.getName();
            }
            else {
                extranetImagePathName = destDir + java.io.File.separator + tempFile.getName();
            }
            String sourceImagePathName = null;
            if (sourceDir.endsWith(java.io.File.separator)) {
                sourceImagePathName = sourceDir + tempFile.getName();
            }
            else {
                sourceImagePathName = sourceDir + java.io.File.separator + tempFile.getName();
            }
            
            java.io.File f = new java.io.File(extranetImagePathName);
            
            // only save if it does not exist
            if (!f.exists()) {
                java.io.File sourceFile = new java.io.File(sourceImagePathName);
                // if source file does not exist assume it's been processed
                if (sourceFile.exists()) {
                    // copy the file    
                    try {                    
                        InputStream in = new FileInputStream(sourceFile);
                        OutputStream out = new FileOutputStream(f);
    
                        // Transfer bytes from in to out
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = in.read(buf)) > 0) {
                            out.write(buf, 0, len);
                        }
                        in.close();
                        out.close();

                    }
                    catch (Exception e) {
                        System.out.println("Failed to copy source file: " + sourceFile.getAbsolutePath() + " to destination: " + f.getAbsolutePath() + "  Reason: " + e.getMessage());
                    }
                    
                    // delete the source file
                    sourceFile.delete();
                }
            }// end if file alrady exists
        } // end while
    }
    
    
    /**
     * 
     * @param sourceDir
     * @param destDir
     * @param c
     */
    private void clearServerCache(PublishingServerIntf ps) {
    	String publishingServer = ps.getPublicHostURL();    	
    	String urlServer = publishingServer + "/include/configReset/clearCacheAdminPortal.jsp";    	
	       
        try{
        	
			System.out.println("Begin Clearing Cache for..." + urlServer);
			// Construct data
	        String data = URLEncoder.encode("key1", "UTF-8") + "=" + URLEncoder.encode("value1", "UTF-8");
	        data += "&" + URLEncoder.encode("key2", "UTF-8") + "=" + URLEncoder.encode("value2", "UTF-8");
	    
	        // Send data
	        URL url = new URL(urlServer);
	      
	        URLConnection conn = url.openConnection();
	        conn.setDoOutput(true);
	        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
	        wr.write(data);
	        wr.flush();
	    
	        // Get the response
	        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        @SuppressWarnings("unused")
			String line;
	        while ((line = rd.readLine()) != null) {
	            // Process line...
	        }
	        wr.close();
	        rd.close();
	        System.out.println("Cache Cleared for server..." + urlServer);
							
		}
		catch (Exception e) {
			 System.out.println("Error clearing cache..." + urlServer);						
		
		}
    }
    
    
    
    private void sendEmailAlert(Collection<UsatCampaignIntf> publishedCampaigns) {
    	if (publishedCampaigns != null && publishedCampaigns.size() > 0) {
    		PortalApplicationRuntimeConfigurations config = new PortalApplicationRuntimeConfigurations();
    		PortalApplicationSettingIntf emailsRecipients = config.getSetting(PortalApplicationSettingIntf.PORTAL_ADMIN_PUBLISH_ALERT_RECIPIENTS);

    		// if setting for recipients contains a value.
    		if (emailsRecipients != null && emailsRecipients.getValue().trim().length() > 0) {
    			try {
    				SmtpMailSender mail = new SmtpMailSender();
    				
    				mail.setMessageSubject("Extranet Admin Application Alert - Campaigns Published");
    				StringBuilder msg = new StringBuilder();
    				msg.append(publishedCampaigns.size());
    				msg.append(" campaign(s) published to production at: ");
    				DateTime d = new DateTime();
    				msg.append(d.toString("MM/dd/yyyy hh:mm a"));
    				msg.append("\n\n");
    				
    				for (UsatCampaignIntf c : publishedCampaigns) {
    					
    					msg.append("Campaign Name:\t").append(c.getCampaignName()).append("\n");
    					msg.append("Campaign Keycode:\t").append(c.getKeyCode()).append("\n");
    					msg.append("Campaign Pub:\t").append(c.getPubCode()).append("\n");
    					if (c.getVanityURL() != null && c.getVanityURL().trim().length() > 0) {
    						msg.append("Campaign URL:\thttp://");
    						msg.append(config.getProductionDomain().getValue());
    						msg.append("/").append(c.getVanityURL()).append("\n");
    					}
    					else {
    						if (c.getRedirectToPage() != null && c.getRedirectToPage().trim().equalsIgnoreCase("TERMS")) {
    							msg.append("Campaign URL:\thttp://");
    							msg.append(config.getProductionDomain().getValue());
    							msg.append("/subscriptions/order/checkout.faces?pub=").append(c.getPubCode()).append("&keycode=" + c.getKeyCode() + "\n");
    						}
    						else {
    							msg.append("Campaign URL:\thttp://");
    							msg.append(config.getProductionDomain().getValue());
    							msg.append("/subscriptions/order/checkout.faces?pub=").append(c.getPubCode()).append("&keycode=" + c.getKeyCode() + "\n");
    						}
    					}
    					msg.append("Campaign Creator:\t").append(c.getCreatedBy()).append("\n");
    					msg.append("Current User:\t").append(c.getUpdatedBy()).append("\n");
    					msg.append("___________________________________________  \n");    					
    					
    				}
    				
    				mail.setMessageText(msg.toString());
    				mail.addTORecipient(emailsRecipients.getValue());
    				
    				mail.setSender(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS);
    				
    				// usat.mail.smtp.host                               
    				PortalApplicationRuntimeConfigurations settings = new PortalApplicationRuntimeConfigurations();
    				PortalApplicationSettingIntf setting = settings.getSetting("usat.mail.smtp.host");
    				String smtpServer = "";
    				if (setting != null) {
    					smtpServer = setting.getValue();
    				}
    				mail.setMailServerHost(smtpServer);
    				
    				mail.sendMessage();
    			}
    			catch (Exception e) {
    				e.printStackTrace();
				}
    		}
    	}
    }
    
    public void clearProductionServerCache() throws UsatException {
		Collection<PublishingServerIntf> prodServers = PublishingServerBO.fetchProductionServers();

		for (PublishingServerIntf server : prodServers) {
			//clear the cache on the target server
			this.clearServerCache(server);					
		} // end for each publishing server
    }
    
    public void clearTestServerCache()  throws UsatException {
		Collection<PublishingServerIntf> testServers = PublishingServerBO.fetchTestServers();

		for (PublishingServerIntf server : testServers) {
			//clear the cache on the target server
			this.clearServerCache(server);					
		} // end for each publishing server
    }
}
