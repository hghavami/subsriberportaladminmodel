package com.gannett.usatoday.adminportal.campaigns.utils;

import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;

public class CampaignPublisherFailure {

	private UsatCampaignIntf campaign = null;
	private String errorReason = null;
	private String recommendedAction = null;
	private Exception underlyingException = null;
	public UsatCampaignIntf getCampaign() {
		return campaign;
	}
	public String getErrorReason() {
		return errorReason;
	}
	public String getRecommendedAction() {
		return recommendedAction;
	}
	public Exception getUnderlyingException() {
		return underlyingException;
	}
	public void setCampaign(UsatCampaignIntf campaign) {
		this.campaign = campaign;
	}
	public void setErrorReason(String errorReason) {
		this.errorReason = errorReason;
	}
	public void setRecommendedAction(String recommendedAction) {
		this.recommendedAction = recommendedAction;
	}
	public void setUnderlyingException(Exception underlyingException) {
		this.underlyingException = underlyingException;
	}
	
}
