package com.gannett.usatoday.adminportal.campaigns.utils;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;

import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.enterprisedt.net.ftp.FileTransferClient;
import com.gannett.usatoday.adminportal.campaigns.PublishingServerBO;
import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usatoday.adminportal.integration.campaigns.XtrntPromoUpdaterDAO;
import com.usatoday.UsatException;
import com.usatoday.util.constants.UsaTodayConstants;

public class CampaignFTPUtilities {

	/**
	 * 
	 * @param vanityURL
	 * @return
	 */
	public boolean isVanityInUse(String vanityURL) {
		boolean inUse = false;
		
		if (vanityURL == null) {
			return inUse;
		}
		
		try {
			PublishingServerCache cache = new PublishingServerCache();
			
			Collection<PublishingServerIntf> prodServers = cache.getProductionServers();

			for (PublishingServerIntf server : prodServers) {
				if (!inUse) {
					inUse = this.checkVanityOnServer(server, vanityURL);
				}
			}			
		}
		catch (Exception exp) {
			System.out.println("Excpetion validating URL: " + exp.getMessage());
		}

		return inUse;
	}

	/**
	 * All images published be campaign tool are in the /images/marketingimages folder on the web server.
	 * If a file is not found there it does not exists.
	 * @param vanityURL
	 * @return
	 */
	public boolean isImageOnProductionServer(String imageURL) {
		boolean inUse = false;
		
		try {
			PublishingServerCache cache = new PublishingServerCache();
			
			Collection<PublishingServerIntf> prodServers = cache.getProductionServers();

			for (PublishingServerIntf server : prodServers) {
				if (!inUse) {
					inUse = this.checkImageOnServer(server, imageURL);
				}
			}
		}
		catch (Exception exp) {
			System.out.println("Excpetion validating URL: " + exp.getMessage());
		}

		return inUse;
	}
	
	/**
	 * 
	 * @param imageURL
	 * @return
	 */
	public byte[] retrieveImageFromProductionServer(String imageURL) {
		byte [] contents = null;

		try {
			PublishingServerCache cache = new PublishingServerCache();
			
			PublishingServerIntf prodServer = cache.getAProductionServer();

			contents = this.getImageFromServer(prodServer, imageURL);
			
		}
		catch (Exception exp) {
			System.out.println("Excpetion rerieving image "+ imageURL + ": " + exp.getMessage());
		}
		
		return contents;
	}
	

	/**
	 * 
	 * @param imageURL
	 * @return
	 */
	public byte[] retrieveImageFromTestServer(String imageURL) {
		byte [] contents = null;

		try {
			PublishingServerCache cache = new PublishingServerCache();
			
			PublishingServerIntf testServer = cache.getTestServers().iterator().next();

			contents = this.getImageFromServer(testServer, imageURL);
			
		}
		catch (Exception exp) {
			System.out.println("Excpetion rerieving image from test "+ imageURL + ": " + exp.getMessage());
		}
		
		return contents;
	}
	
	/**
	 * 
	 * @param ps
	 * @param url
	 * @return
	 */
	protected byte[] getImageFromServer(PublishingServerIntf ps, String url) {
		byte [] contents = null;

		com.enterprisedt.net.ftp.FileTransferClient ftp = null;

		try {
			ftp = new FileTransferClient();

			ftp.setTimeout(180000);
			
			ftp.setRemoteHost(ps.getHost());
			ftp.setPassword(ps.getFTPPwd());
			ftp.setUserName(ps.getFTPUid());
			
			ftp.connect();
			
			ftp.setContentType(FTPTransferType.BINARY);

			if (ps.getPublishRootDir() != null && ps.getPublishRootDir().trim().length() > 0) {
				ftp.changeDirectory(ps.getPublishRootDir().trim());
			}
    
			if (UsaTodayConstants.debug) {
				System.out.println("Remote FTP System Type:  " + ftp.getSystemType());
			}
			
			ftp.getAdvancedFTPSettings().setConnectMode(FTPConnectMode.PASV);

			// if a vanity URL exists for campaign
            if (url != null && url.trim().length() > 0) {
                
                try {
                	String fileName = url;
                	int indexOfPath = url.indexOf("images/marketingimages/");
                	if (indexOfPath > -1) {
                		// if full path specified
                		if (indexOfPath == 0) {
                			// no leading slash so do nothing
                			fileName = url;
                		}
                		else {
                			// remove leading slash
                			fileName = url.substring(1);
                		}
                	}
                	else {
                		ftp.changeDirectory("images/marketingimages");
                	}
                	contents = ftp.downloadByteArray(fileName);
	               	
                }
                catch (FTPException ftpe) {
                    // exception means it does not exist so it's fine
                    //ftpe.printStackTrace();
                }
            } // end if a vanity URL exists
						
            try {
            	ftp.disconnect(true);
            }
            catch (Exception e) {
            	; // can't do anything.
			}
        }  // end try            
         catch (Exception e) {
		       System.out.println("CampaignFTPUtilities::getImageFromServer - exception getting image.");
		       if (ftp != null && ftp.isConnected()) {
		           try {
		               ftp.disconnect();
		           }catch (Exception exp) {
		               ; // ignore
		           }
		       }
    	}  //end catch
         		
		
		return contents;
	}
	
	/**
	 * 
	 * @param ps
	 * @param vanity
	 * @return
	 */
	protected boolean checkVanityOnServer(PublishingServerIntf ps, String vanity) {


        boolean vanityInUse = false;
        
		com.enterprisedt.net.ftp.FileTransferClient ftp = null;

		try {
			ftp = new FileTransferClient();
			
			ftp.setRemoteHost(ps.getHost());
			ftp.setPassword(ps.getFTPPwd());
			ftp.setUserName(ps.getFTPUid());
			
			ftp.connect();
    
            if (ps.getPublishRootDir() != null && ps.getPublishRootDir().trim().length() > 0) {
                ftp.changeDirectory(ps.getPublishRootDir().trim());
                //System.out.println(ftp.getLastValidReply().getReplyText());
            }  // end if

			// if a vanity URL exists for campaign
            if (vanity != null && vanity.trim().length() > 0) {
                
                try {
	               	ftp.changeDirectory(vanity);
	               	//System.out.println(ftp.getLastValidReply().getReplyText() + " code:"+ ftp.getLastValidReply().getReplyCode());
	                
	               	// if successful chdir then the vanity is truly in use.
	               	// otherwise an exception would get thrown.
               	    vanityInUse = true;
                }
                catch (FTPException ftpe) {
                    // exception means it does not exist so it's fine
                    ;
                }
            } // end if a vanity URL exists

            ftp.disconnect();
            //ftp.quit();
            //System.out.println(ftp.getLastValidReply().getReplyText());            
        }  // end try            
         catch (Exception e) {
		       System.out.println("CampaignFTPUtilities::checkVanityOnServer - Failed to check vanity.");
		       if (ftp != null) {
		           try {
		        	   ftp.disconnect();
		           }catch (Exception exp) {
		               ; // ignore
		           }
		       }
    	}  //end catch
         
        return vanityInUse;		
	}
	
	/**
	 * 
	 * @param ps
	 * @param vanity
	 * @return
	 */
	protected boolean deleteVanityFromServer(PublishingServerIntf ps, String vanity) {

		if (vanity == null || vanity.trim().length() == 0) {
			return true;
		}
        boolean removedVanity = false;
        
		com.enterprisedt.net.ftp.FileTransferClient ftp = null;

		try {
			ftp = new FileTransferClient();
			
			ftp.setRemoteHost(ps.getHost());
			ftp.setPassword(ps.getFTPPwd());
			ftp.setUserName(ps.getFTPUid());
			
			ftp.connect();
    
            if (ps.getPublishRootDir() != null && ps.getPublishRootDir().trim().length() > 0) {
                ftp.changeDirectory(ps.getPublishRootDir().trim());
            }  // end if

			// if a vanity URL exists for campaign
            if (vanity != null && vanity.trim().length() > 0) {
                
                try {
	               	ftp.changeDirectory(vanity);
	             
	                
	               	ftp.deleteFile("index.html");  
	                
		            // go back up a directory
		            ftp.changeToParentDirectory();
		            // remove the URL directory
		            ftp.deleteDirectory(vanity);
					
					removedVanity = true;
                }
                catch (FTPException ftpe) {
                    // exception means it does not exist so it's fine
                    ftpe.printStackTrace();
                }
            } // end if a vanity URL exists
						
            ftp.disconnect();
        }  // end try            
         catch (Exception e) {
		       System.out.println("CampaignFTPUtilities::checkVanityOnServer - Failed to check vanity.");
		       if (ftp != null) {
		           try {
		               ftp.disconnect();
		           }catch (Exception exp) {
		               ; // ignore
		           }
		       }
    	}  //end catch
         
        return removedVanity;		
	}

	/**
	 * 
	 * @param ps
	 * @param imageName
	 * @throws UsatException
	 */
	protected void deleteImagesFromServer(PublishingServerIntf ps, Collection<String> imageNames) throws UsatException {
        
		if (ps == null || imageNames == null || imageNames.size() == 0) {
			return;
		}

		com.enterprisedt.net.ftp.FileTransferClient ftp = null;

		try {
			ftp = new FileTransferClient();
			
			ftp.setRemoteHost(ps.getHost());
			ftp.setPassword(ps.getFTPPwd());
			ftp.setUserName(ps.getFTPUid());
			
			ftp.connect();

			if (ps.getPublishRootDir() != null && ps.getPublishRootDir().trim().length() > 0) {
				ftp.changeDirectory(ps.getPublishRootDir().trim());
			}  // end if

			//ftp.changeDirectory("images");
		
			// If the image is not used in another stage in the current campaign, or 
			// by another campaign, then delete the image. 

			for (String image : imageNames) {
				try {
					// remove the leading '/'
					System.out.println("Deleting image: " + image);
					ftp.deleteFile(image.substring(1));												
				}
				catch (Exception exp) {
					System.out.println(image + " image not on server: " + ps.getHost());
				}
			}
							
			ftp.disconnect();
		}  // end try    
		        
		 catch (Exception e) {
			   System.out.println("ftpDeleteCampaignImage - Failed to delete campaign.");
		}  //end catch
	}  // end ftpDeleteCampaignImage
	
	/**
	 * 
	 * @param ps
	 * @param url
	 * @return
	 */
	protected boolean checkImageOnServer(PublishingServerIntf ps, String url) {

        boolean imageOnServer = false;
        
		com.enterprisedt.net.ftp.FileTransferClient ftp = null;

		try {

			ftp = new FileTransferClient();

       		if (UsaTodayConstants.debug) {
       			LogManager.getRootLogger().setLevel(Level.DEBUG);
       		}
			
			ftp.setTimeout(180000);

			ftp.setRemoteHost(ps.getHost());
			ftp.setPassword(ps.getFTPPwd());
			ftp.setUserName(ps.getFTPUid());
			
			ftp.connect();
    
            if (ps.getPublishRootDir() != null && ps.getPublishRootDir().trim().length() > 0) {
                ftp.changeDirectory(ps.getPublishRootDir().trim());
            }  // end if

            
			ftp.getAdvancedFTPSettings().setConnectMode(FTPConnectMode.PASV);
            
			// if a vanity URL exists for campaign
            if (url != null && url.trim().length() > 0) {
                
                try {
	               	ftp.changeDirectory("images/marketingimages");

	               	//FTPFile[] ftpFiles = ftp.directoryList();
	               	String[] ftpFiles = ftp.directoryNameList();
	               	for (int i = 0; i < ftpFiles.length && !imageOnServer; i++) {
	               		String fName = ftpFiles[i];
	               		int index = url.lastIndexOf('/');
	               		String urlFileName = url.substring(index+1);
	               		if (UsaTodayConstants.debug) {
	               			System.out.println("Comparing_file: " + fName + "   To File: " + urlFileName);
	               		}
	               		if (fName.equalsIgnoreCase(urlFileName)) {
	               			imageOnServer = true;
	               		}
	               	}
	               	
                }
                catch (FTPException ftpe) {
                    // exception means it does not exist so it's fine
                    ftpe.printStackTrace();
                }
            } // end if a vanity URL exists

            if (ftp != null && ftp.isConnected()) {
            	ftp.disconnect(true);
            }
        }  // end try            
         catch (Exception e) {
		       System.out.println("CampaignFTPUtilities::checkImageOnServer - exception checking for image." + e.getMessage());
		       e.printStackTrace();
		       if (ftp != null && ftp.isConnected()) {
		           try {
		               ftp.disconnect();
		           }catch (Exception exp) {
		               ; // ignore
		           }
		       }
    	}  //end catch
         
        return imageOnServer;		
	}

	/**
	 * 
	 * @param vanity
	 * @throws UsatException
	 */
	public void cleanUpOldVanityFromAllServers(String vanity) throws UsatException {
		Collection<PublishingServerIntf> publishingServers = PublishingServerBO.fetchAllActiveServers();
		for (PublishingServerIntf server: publishingServers) {
			try {
				this.deleteVanityFromServer(server, vanity);
			}
			catch (Exception e) {
				System.out.println("Failed to delete old vanity from server: " + server.getHost() + "  Possibly never existed there. " + e.getMessage() );
			}
		}
	}	
	/**
	 * 
	 * @param campaign
	 * @return
	 * @throws UsatException
	 */
	public boolean cleanUpCampaignFromAllServers(UsatCampaignIntf campaign) throws UsatException {
		boolean fullyCleaned = true;

		if (campaign == null) {
			return true;
		}
		
		Collection<EditableImagePromotionIntf> images = campaign.getPromotionSet().getAllImagePromotions();
		
		String vanity = campaign.getVanityURL();
		
		XtrntPromoUpdaterDAO promotionDAO = new XtrntPromoUpdaterDAO();
		
		// clean off test and production servers
		Collection<PublishingServerIntf> publishingServers = PublishingServerBO.fetchAllActiveServers();
		for (PublishingServerIntf server: publishingServers) {
			try {
				this.deleteVanityFromServer(server, vanity);
			}
			catch (Exception e) {
				fullyCleaned = false;
				e.printStackTrace();
			}
			// for each image in the campaign/delete it from server
			ArrayList<String> imagesReadyToDelete = new ArrayList<String>();
			for (EditableImagePromotionIntf image : images) {
				try {
					if (image.getName().indexOf("/shim.gif") > -1) {
						// ignore any shim.gif images
						continue;
					}
					int usedCount = promotionDAO.fetchCountOfPromosByNameExcludingKeycode(image.getName(), image.getPubCode(), image.getKeyCode());
					if (usedCount == 0) {
						// delete the image files.
						imagesReadyToDelete.add(image.getName());
					}
				}
				catch (Exception e) {
					fullyCleaned = false;
					e.printStackTrace();
				}
			}

			try {
				// delete the qualifying images
				this.deleteImagesFromServer(server, imagesReadyToDelete);
			}
			catch (Exception e) {
				fullyCleaned = false;
				e.printStackTrace();
			}
		}
		
		return fullyCleaned;
	}
	
	/**
	 *  This method would be invoked when an image is changed or replaced for a vanity, but the
	 *  campaign was not deleted.
	 *  
	 * @param image the image to clean up
	 */
	public void cleanUpImageFromAllServers(EditableImagePromotionIntf image) {
		
		XtrntPromoUpdaterDAO promotionDAO = new XtrntPromoUpdaterDAO();
		
		// verify image not in use by other campaigns
		ArrayList<String> imagesReadyToDelete = new ArrayList<String>();
		try {
			if (image.getName().indexOf("/shim.gif") == -1) {
				int usedCount = promotionDAO.fetchCountOfPromosByNameExcludingKeycode(image.getName(), image.getPubCode(), image.getKeyCode());
				if (usedCount == 0) {
					// delete the image files.
					imagesReadyToDelete.add(image.getName());
				}
			}
			// clean off test and production servers
			
			Collection<PublishingServerIntf> publishingServers = PublishingServerBO.fetchAllActiveServers();
			for (PublishingServerIntf server: publishingServers) {
				try {
					// delete the image
					this.deleteImagesFromServer(server, imagesReadyToDelete);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
