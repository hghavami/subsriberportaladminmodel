package com.gannett.usatoday.adminportal.campaigns.utils;

import java.util.Collection;

import com.gannett.usatoday.adminportal.campaigns.PublishingServerBO;
import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerIntf;

public class PublishingServerCache {

	private static Collection<PublishingServerIntf>  productionServers = null;
	private static Collection<PublishingServerIntf>  testServers = null;
	
	
	public PublishingServerCache() {
		super();
		
		if (PublishingServerCache.productionServers == null) {
			loadServers();
		}
	}
	
	private void loadServers() {
		try {
			PublishingServerCache.productionServers = PublishingServerBO.fetchProductionServers();
			PublishingServerCache.testServers = PublishingServerBO.fetchTestServers();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Collection<PublishingServerIntf> getProductionServers() {
		return PublishingServerCache.productionServers;
	}
	public Collection<PublishingServerIntf> getTestServers() {
		return PublishingServerCache.testServers;
	}
	
	public void reloadCache() {
		PublishingServerCache.productionServers.clear();
		PublishingServerCache.productionServers = null;
		PublishingServerCache.testServers.clear();
		PublishingServerCache.testServers = null;
		
		loadServers();
	}
	
	public PublishingServerIntf getAProductionServer() {
		PublishingServerIntf s = null;
		if (PublishingServerCache.productionServers != null && PublishingServerCache.productionServers.size() > 0) {
			s = PublishingServerCache.productionServers.iterator().next();
		}
		return s;
	}

	public PublishingServerIntf getATestServer() {
		PublishingServerIntf s = null;
		if (PublishingServerCache.testServers != null && PublishingServerCache.testServers.size() > 0) {
			s = PublishingServerCache.testServers.iterator().next();
		}
		return s;
	}

}
