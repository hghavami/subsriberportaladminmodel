package com.gannett.usatoday.adminportal.campaigns.promotions;

import java.net.URL;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

import com.gannett.usatoday.adminportal.campaigns.intf.PersistentImageIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.utils.CampaignFTPUtilities;
import com.gannett.usatoday.adminportal.campaigns.utils.PublishingServerCache;
import com.gannett.usatoday.adminportal.integration.campaigns.ImageDAO;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.util.constants.UsaTodayConstants;

public class EditableImagePromotionBO extends EditablePromotionBO implements
		EditableImagePromotionIntf {
	
	private byte[] imageContents = null;
	private String imageFileType = null;
	
	public EditableImagePromotionBO() {
		super();
	}

	public EditableImagePromotionBO(PromotionIntf promotion) {
		super(promotion);
		this.imagePromo = true;
	}

	public void setImageAltText(String altText) throws UsatException {
        this.setAltName(altText);
	}

	public void setImageLinkToURL(String link) throws UsatException {
		this.setFulfillUrl(link);
	}

	public void setImagePathString(String path) throws UsatException {
		this.setName(path);
	}

	public String getImageAltText() {
        return this.getAltName();
	}

	public String getImageLinkToURL() {
        return this.getFulfillUrl();
	}

	public String getImagePathString() {
        return this.getName();
	}

	public byte[] getImageContents() {
		
		if (this.imageContents == null) {
			// check production
			// then check test 
			// then check the database
			if (this.getImagePathString() != null && this.getImagePathString().length() > 0) {
				// load the image contents from a production server.
				PublishingServerCache cache = new PublishingServerCache();
				PublishingServerIntf pubServer = cache.getAProductionServer();
				
				if (pubServer != null) {
					HttpClient client = new HttpClient();
					GetMethod method = null; 
					try{
						URL imageURL = new URL(pubServer.getPublicHostURL() + this.getImagePathString());
						if (imageURL != null) {
							if (UsaTodayConstants.debug) {
								System.out.println("EditableImagePromotion::getImageContents() - Retrieving image contents from : " + imageURL.toExternalForm() + "(" + pubServer.getPublicHostURL() + this.getImagePathString() + ")");
							}
							method = new GetMethod(imageURL.toExternalForm());
							int statusCode = -99;
							
							statusCode = client.executeMethod(method);

							if (statusCode == HttpStatus.SC_OK) {
								this.imageContents = method.getResponseBody();
								this.imageFileType = method.getResponseHeader("Content-Type").getValue();
							}
							
						}
					}
					catch (Exception e) {
						this.imageContents = null; // clear any image contents
						this.imageFileType = null;
					}
  					finally {
						// Release the connection.
						try {
							method.releaseConnection();
						}
						catch (Exception eee) {
							; //ignore
						}
					} // end finally
				}
				if (this.imageContents == null) {
					CampaignFTPUtilities ftpUtil = new CampaignFTPUtilities();
					// try to get from test servers
					if (UsaTodayConstants.debug) {
						System.out.println("Retrieving image contents via FTP");
					}
					this.imageContents = ftpUtil.retrieveImageFromTestServer(this.getImagePathString());
					if (imageContents == null) {
						// try from temp database
						try {
							if (UsaTodayConstants.debug) {
								System.out.println("Retrieving image contents from Temp Database :" + this.getImagePathString());
							}
							
							ImageDAO dao = new ImageDAO();
							PersistentImageIntf image = dao.fetchImageByName(this.getImagePathString());
							if (image != null) {
								this.imageContents = image.getImageContents();
								this.imageFileType = image.getImageFileType();
							}
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		else {
			if (UsaTodayConstants.debug) {
				System.out.println("Using In Memory contents for : " + this.getImagePathString() + ", Size: " + this.imageContents.length + " bytes, Keycode: " + this.getKeyCode());
			}			
		}
		return imageContents;
	}

	public void setImageContents(byte[] imageContents) {
		this.imageContents = imageContents;
	}

	public boolean isShimImage() {
		if (this.getImagePathString() != null && this.getImagePathString().indexOf("/shim.gif")>-1) {
			return true;
		}
		return false;
	}

	public String getImageFileType() {
		return imageFileType;
	}

	public void setImageFileType(String imageFileType) {
		this.imageFileType = imageFileType;
	}

}
