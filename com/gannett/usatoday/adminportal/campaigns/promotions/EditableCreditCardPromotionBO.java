package com.gannett.usatoday.adminportal.campaigns.promotions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableCreditCardPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionIntf;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentTypentf;
import com.usatoday.businessObjects.products.promotions.PromotionBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentTypeBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentTypeBean;

public class EditableCreditCardPromotionBO extends EditablePromotionBO implements
		EditableCreditCardPromotionIntf {
	
    private boolean acceptVisa = false;
    private boolean acceptMasterCard = false;
    private boolean acceptAmericanExpress = false;
    private boolean acceptDiners = false;
    private boolean acceptDiscovery = false;
	
    private HashMap<String, EditablePromotionIntf> creditCards = new HashMap<String, EditablePromotionIntf>();
	
	public EditableCreditCardPromotionBO() {
		super();
        try {
            super.setType(PromotionIntf.CREDIT_CARD);
        }
        catch (Exception e) {
        	;
		}
	}

	public EditableCreditCardPromotionBO(PromotionIntf promotion) {
		super(promotion);
        try {
            super.setType(PromotionIntf.CREDIT_CARD);
            // add this card to list of cards
            this.addCard(promotion);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
	}

	/**
	 * 
	 * @param promotion
	 * @throws UsatException
	 */
	public void addCard(PromotionIntf promotion) throws UsatException {
        if (promotion == null || !promotion.getType().equalsIgnoreCase(PromotionIntf.CREDIT_CARD)) {
            throw new UsatException("CreditCardPromo-addCardToPromo() - invalid card");
        }
        
        EditablePromotionBO newP = new EditablePromotionBO(promotion);
        newP.setType(PromotionIntf.CREDIT_CARD);
        newP.setName(promotion.getName());
        newP.setFulfillText(promotion.getFulfillText());
        
        this.creditCards.put(newP.getName(), newP);
        
        int type = Integer.parseInt(promotion.getName());
        switch (type) {
             case CreditCardPromotionIntf.AMEX:
                 this.acceptAmericanExpress = true;
                 break;
             case CreditCardPromotionIntf.VISA:
                 this.acceptVisa = true;
                 break;
             case CreditCardPromotionIntf.MC:
                 this.acceptMasterCard = true;                        
                 break;
             case CreditCardPromotionIntf.DISCOVERY:
                 this.acceptDiscovery = true;
                 break;
             case CreditCardPromotionIntf.DINERS:
                 this.acceptDiners = true;                        
                 break;        
             default :
                 break;
         }        
    }
	
	/**
	 * 
	 * @param type
	 */
    public void addCardToPromo(String type) {
        
    	if (type == null) {
    		return;
    	}
    	
    	EditablePromotionBO xp = new  EditablePromotionBO();
        
        try {
            
            xp.setType(PromotionIntf.CREDIT_CARD);
            xp.setPubCode(this.getPubCode());
            xp.setKeyCode(this.getKeyCode());
            
            if (type.equalsIgnoreCase(EditableCreditCardPromotionIntf.AMEX)) {
                xp.setName(EditableCreditCardPromotionIntf.AMEX);
                xp.setFulfillText(EditableCreditCardPromotionIntf.AMEX_TEXT);
                this.acceptAmericanExpress = true;            	
            }
            else if (type.equalsIgnoreCase(EditableCreditCardPromotionIntf.VISA)) {
                xp.setName(EditableCreditCardPromotionIntf.VISA);
                xp.setFulfillText(EditableCreditCardPromotionIntf.VISA_TEXT);
                this.acceptVisa = true;
            }
            else if (type.equalsIgnoreCase(EditableCreditCardPromotionIntf.MC)) {
                xp.setName(EditableCreditCardPromotionIntf.MC);
                xp.setFulfillText(EditableCreditCardPromotionIntf.MC_TEXT);
                this.acceptMasterCard = true;
            }
            else if (type.equalsIgnoreCase(EditableCreditCardPromotionIntf.DISCOVERY)) {
                xp.setName(EditableCreditCardPromotionIntf.DISCOVERY);
                xp.setFulfillText(EditableCreditCardPromotionIntf.DISCOVERY_TEXT);
                this.acceptDiscovery = true;
            }
            else if (type.equalsIgnoreCase(EditableCreditCardPromotionIntf.DINERS)) {
                xp.setName(EditableCreditCardPromotionIntf.DINERS);
                xp.setFulfillText(EditableCreditCardPromotionIntf.DINERS_TEXT);
                this.acceptDiners = true;                        
            	
            }
             
             this.creditCards.put(xp.getName(), xp);
             
        }
        catch (UsatException ue) {
            // do nothing
        }
    }
	
	public boolean acceptsAmEx() {
		return this.acceptAmericanExpress;
	}

	public boolean acceptsDiners() {
		return this.acceptDiners;
	}

	public boolean acceptsDiscovery() {
		return this.acceptDiscovery;
	}

	public boolean acceptsMasterCard() {
		return this.acceptMasterCard;
	}

	public boolean acceptsVisa() {
		return this.acceptVisa;
	}

	public boolean cardInPromotion(String type) {
        boolean inPromotion = false;

        int typeInt = this.convertStringTypeToIntType(type);
        switch (typeInt) {
        	case CreditCardPromotionIntf.VISA:
        	    inPromotion = this.acceptsVisa();
        		break;
        	case CreditCardPromotionIntf.MC:
        	    inPromotion = this.acceptsMasterCard();
        		break;
        	case CreditCardPromotionIntf.AMEX:
        	    inPromotion = this.acceptsAmEx();
        		break;
        	case CreditCardPromotionIntf.DISCOVERY:
        	    inPromotion = this.acceptsDiscovery();
        		break;
        	case CreditCardPromotionIntf.DINERS:
        	    inPromotion = this.acceptsDiners();
        		break;
        	default:
        	    inPromotion = false;
        }
        return inPromotion;
	}

    private int convertStringTypeToIntType (String type){
        int ccTypeInt = -1;
        
        if (type.equalsIgnoreCase("X")) {
            ccTypeInt = CreditCardPromotionIntf.AMEX;
        }
        else if (type.equalsIgnoreCase("V")) {
            ccTypeInt = CreditCardPromotionIntf.VISA;
        }
        else if (type.equalsIgnoreCase("M")) {
            ccTypeInt = CreditCardPromotionIntf.MC;
        }
        else if (type.equalsIgnoreCase("D")) {
            ccTypeInt = CreditCardPromotionIntf.DISCOVERY;
        }
        else if (type.equalsIgnoreCase("C")) {
            ccTypeInt = CreditCardPromotionIntf.DINERS;
        }
        
        return ccTypeInt;
        
    }
	
	public String getAcceptedCardsString() {
        StringBuilder sBuf = new StringBuilder();
        boolean needComma = false;
        if (this.acceptsAmEx()){
            sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.AMEX]);
            needComma = true;
        }
        if (this.acceptsVisa()){
            if (needComma) {
                sBuf.append(", ");
            }
            sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.VISA]);
            needComma = true;
        }
        if (this.acceptsMasterCard()){
            if (needComma) {
                sBuf.append(", ");
            }
            sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.MC]);
            needComma = true;
        }
        if (this.acceptsDiscovery()){
            if (needComma) {
                sBuf.append(", ");
            }
            sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.DISCOVERY]);
            needComma = true;
        }
        if (this.acceptsDiners()){
            if (needComma) {
                sBuf.append(", ");
            }
            sBuf.append(CreditCardPromotionIntf.BANK_NAMES[CreditCardPromotionIntf.DINERS]);
        }
        
        return sBuf.toString();
	}

	public Collection<CreditCardPaymentTypeBean> getCreditCardPaymentTypesBeans()
			throws UsatException {
        Collection<EditablePromotionIntf> ccBO = this.creditCards.values();
        
        ArrayList<CreditCardPaymentTypeBean> beans = new ArrayList<CreditCardPaymentTypeBean>();
        if (ccBO != null) {
            Iterator<EditablePromotionIntf> itr = ccBO.iterator();
            while(itr.hasNext()) {
                PromotionBO bo = (PromotionBO)itr.next();
                CreditCardPaymentTypeBean bean = new CreditCardPaymentTypeBean();
                bean.setLabel(bo.getFulfillText());
                bean.setValue(bo.getName());
                // figure out type and set image file accordingly.
                Collection<CreditCardPaymentTypentf> ccTypes = CreditCardPaymentTypeBO.getCreditCardPaymentTypes();
                Iterator<CreditCardPaymentTypentf> cItr = ccTypes.iterator();
                while(cItr.hasNext()) {
                    Object obj = cItr.next();
                    CreditCardPaymentTypeBO cbo = (CreditCardPaymentTypeBO) obj;
                    if (cbo.getDescription().equalsIgnoreCase(bo.getFulfillText())) { // PROMOTION OBJECTS STORE CC NAME
                        bean.setImageFile(cbo.getImagePath());
                        break;
                    }
                }
                //bean.setImageFile(bo.get)
                beans.add(bean);
            }
        }
        return beans;
	}
	
	public Collection<EditablePromotionIntf> getCardPromosForSaving() {
		
		if (this.creditCards != null) {
			return this.creditCards.values();
		}
		return null;
	}

	@Override
	public void setKeyCode(String string) throws UsatException {
		// TODO Auto-generated method stub
		super.setKeyCode(string);
		for (EditablePromotionIntf card : this.creditCards.values()) {
			card.setKeyCode(string);
		}
	}

	@Override
	public void setPubCode(String string) throws UsatException {
		// TODO Auto-generated method stub
		super.setPubCode(string);
		for (EditablePromotionIntf card : this.creditCards.values()) {
			card.setPubCode(string);
		}
	}

}
