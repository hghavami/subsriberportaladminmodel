package com.gannett.usatoday.adminportal.campaigns.promotions;

import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableEZPayPromotionIntf;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

public class EditableEZPayPromotionBO extends EditablePromotionBO implements
		EditableEZPayPromotionIntf {

	public EditableEZPayPromotionBO() {
		super();
	}

	public EditableEZPayPromotionBO(PromotionIntf promotion) {
		super(promotion);
		
	}

	public String getCustomText() {
		return this.getFulfillText();
	}

	public void setCustomText(String text) throws UsatException {
        if (text != null) {
            this.setFulfillText(text);
        }
        else {
            this.setFulfillText("<!-- no custom ezpay text -->");
        }
	}
}
