package com.gannett.usatoday.adminportal.campaigns.promotions.intf;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.EZPayPromotionIntf;

public interface EditableEZPayPromotionIntf extends EditablePromotionIntf,
		EZPayPromotionIntf {

	public void setCustomText(String text) throws UsatException;
}
