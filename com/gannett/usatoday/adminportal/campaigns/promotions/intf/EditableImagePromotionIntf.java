package com.gannett.usatoday.adminportal.campaigns.promotions.intf;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf;

public interface EditableImagePromotionIntf extends 
					ImagePromotionIntf, EditablePromotionIntf {

	public static final String UT_COVER_IMAGE_NAME = "usat_covers.jpg";
	public static final String SW_COVER_IMAGE_NAME = "bbw_covers.jpg";
	
	public void setImagePathString(String path) throws UsatException;
    
    public void setImageLinkToURL(String link) throws UsatException;
    
    public void setImageAltText(String altText) throws UsatException;

    public byte[] getImageContents();
    
    public void setImageContents(byte[] contents);
    
    public boolean isShimImage();
    
	public String getImageFileType();

	public void setImageFileType(String imageFileType);
    
}
