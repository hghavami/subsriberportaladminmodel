package com.gannett.usatoday.adminportal.campaigns.promotions.intf;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

public interface EditablePromotionIntf extends PromotionIntf {

	public void setAltName(String string) throws UsatException;

	public void setFulfillText(String string)throws UsatException;

	public void setFulfillUrl(String string)  throws UsatException;	
	
	public void setKeyCode(String string)  throws UsatException;
	
	public void setName(String string)  throws UsatException;
	
	public void setPubCode(String string)  throws UsatException;
	
	public void setType(String string)  throws UsatException;	
}
