package com.gannett.usatoday.adminportal.campaigns.promotions.intf;

import java.util.Collection;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

public interface EditableCreditCardPromotionIntf extends
		CreditCardPromotionIntf, EditablePromotionIntf {
 
	public static final String AMEX = "0";
    public static final String VISA = "1";
    public static final String MC = "2";
    public static final String DISCOVERY = "3";
    public static final String DINERS = "4";

    public static final String AMEX_TEXT = "American Express";
    public static final String VISA_TEXT = "VISA";
    public static final String MC_TEXT = "Master Card";
    public static final String DISCOVERY_TEXT = "Discovery";
    public static final String DINERS_TEXT = "Diner's Club";

    public void addCardToPromo(String type) throws UsatException;
    public void addCard(PromotionIntf promotion) throws UsatException;
    
    public Collection<EditablePromotionIntf> getCardPromosForSaving();    
}
