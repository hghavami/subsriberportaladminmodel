package com.gannett.usatoday.adminportal.campaigns.promotions.intf;

import java.util.Collection;
import java.util.HashMap;

import com.usatoday.businessObjects.products.promotions.PromotionSet;

public interface EditablePromotionSetIntf {

	/**
	 * 
	 * @return The hash map of promo configs keyed of promo type
	 */
	public HashMap<String, EditablePromotionIntf> getPromoConfigsHashMap();
	public Collection<EditablePromotionIntf> getPromoConfigsCollection();
	public Collection<EditableImagePromotionIntf> getAllImagePromotions();
	
	public String getPubCode();
	public String getKeyCode();
	public void setPubCode(String pCode);
	public void setKeyCode(String kCode);
	
	public boolean getIsChanged();
	
	public void clearAllPromoConfigurations();
	
	public void clearLandingPageConfigurations();
	public void clearTermsPageConfigurations();
	public void clearPaymentPageConfigurations();
	public void clearCompletePageConfigurations();
	public void clearNewOrderPopOverlayConfigurations();	
	public void clearRateOverrideConfigurations();
	public void clearNavigationConfigurations();
	public void clearOnePageOrderConfigurations();
	public void clearForceEZPAYOverrides();
	public void clearProductImages();
	
    public EditableCreditCardPromotionIntf getPromoCreditCard();
    public void setPromoCreditCard(EditableCreditCardPromotionIntf promo);
    
    public EditableImagePromotionIntf getProductImage1();
    public void setProductImage1(EditableImagePromotionIntf promo);
    
    public EditableImagePromotionIntf getLandingPagePromoImage1();
    public void setLandingPagePromoImage1(EditableImagePromotionIntf promo);
    
    public EditableImagePromotionIntf getLandingPagePromoImage2();
    public void setLandingPagePromoImage2(EditableImagePromotionIntf promo);
    
    public EditableImagePromotionIntf getLandingPagePromoImage3();
    public void setLandingPagePromoImage3(EditableImagePromotionIntf promo);
    
    public EditableImagePromotionIntf getRightColumnUpperPromoImageLandingPage();
    public void setRightColumnUpperPromoImageLandingPage(EditableImagePromotionIntf promo);
    
    public EditableImagePromotionIntf getNewSubscriptionOrderPopUpOverlay();
    public void setNewSubscriptionOrderPopUpOverlay(EditableImagePromotionIntf promo);

    public EditableImagePromotionIntf getLandingPageFooterPromoImage();
    public void setLandingPageFooterPromoImage(EditableImagePromotionIntf promo);
        
    public EditableImagePromotionIntf getTemplateNavigationPromoImage();
    public void setTemplateNavigationPromoImage(EditableImagePromotionIntf promo);
    
    public EditableImagePromotionIntf getPromoImageComplete();
    public void setPromoImageComplete(EditableImagePromotionIntf promo);
    
    public EditableHTMLPromotionIntf getPromoLandingPageText();
    public void setPromoLandingPageText(EditableHTMLPromotionIntf promo);
    
    public EditableHTMLPromotionIntf getTermsAndConditionsText();
    public void setTermsAndConditionsText(EditableHTMLPromotionIntf promo);
    
    public EditableHTMLPromotionIntf getPromoEZPayCustomText();
    public void setPromoEZPayCustomText(EditableHTMLPromotionIntf promo);
    
    public EditablePromotionIntf getForceEZPAY();
    public void setForceEZPAY(EditablePromotionIntf promo);
    
    public EditablePromotionIntf getDeliveryNotification();
    public void setDeliveryNotification(EditablePromotionIntf promo);
    
	public EditableHTMLPromotionIntf getOnePagePromoSpot1Text();
	public void setOnePagePromoSpot1Text(EditableHTMLPromotionIntf promo);

	public EditableHTMLPromotionIntf getOnePageDisclaimerText();
	public void setOnePageDisclaimerText(EditableHTMLPromotionIntf promo);

	public EditableHTMLPromotionIntf getOnePageForceEZPayTermsText();
	public void setOnePageForceEZPayTermsText(EditableHTMLPromotionIntf promo);
	
	public EditablePromotionIntf getAllowOfferCodeOverrides();
	public void setAllowOfferCodeOverrides(EditablePromotionIntf promo);
	
	public EditableHTMLPromotionIntf getDynamicNavigationCustomerServiceHTML();
	public void setDynamicNavigationCustomerServiceHTML(EditableHTMLPromotionIntf promo);
	
	public EditableHTMLPromotionIntf getDynamicNavigationOrderEntryHTML();
	public void setDynamicNavigationOrderEntryHTML(EditableHTMLPromotionIntf promo);
	
	public PromotionSet getDefaultBrandingPubPromotionSet();
	public PromotionSet getDefaultPubPromotionSet();
}
