package com.gannett.usatoday.adminportal.campaigns.promotions.intf;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf;

public interface EditableHTMLPromotionIntf extends EditablePromotionIntf,
		HTMLPromotionIntf {

	public void setPromotionalHTML(String html) throws UsatException ;

}
