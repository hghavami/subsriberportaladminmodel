package com.gannett.usatoday.adminportal.campaigns.promotions;

import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionIntf;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

public class EditablePromotionBO implements EditablePromotionIntf {

    private String pubCode = "";
    private String keyCode = "";
    private String type = "";
    private String name = "";
    private String fulfillText = "";
    private String fulfillUrl = "";
    private String altName = "";

    protected boolean imagePromo = false; 
    protected boolean changed = true;
    
    /**
     * 
     * @param source
     * @return
     * @throws UsatException
     */
    public static EditablePromotionIntf createEditablePromotionBO(PromotionIntf source) throws UsatException {

        if (source == null) {
            return null;
        }
        
        EditablePromotionIntf promo = null;
        
        if (source.getType().equalsIgnoreCase(PromotionIntf.CREDIT_CARD)) {
			// and the card 
			EditableCreditCardPromotionBO ccp = new EditableCreditCardPromotionBO(source);
			promo = ccp;
        }
        else if (source.getType().equalsIgnoreCase(PromotionIntf.EZPAY_PROMO) ||
                source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_TEXT) ||
                source.getType().equalsIgnoreCase(PromotionIntf.TERMS_AND_CONDITIONS_TEXT) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1) ||
                source.getType().equalsIgnoreCase(PromotionIntf.DYNAMIC_NAVIGATION_CUSTSERV) ||
                source.getType().equalsIgnoreCase(PromotionIntf.DYNAMIC_NAVIGATION_ORDER_ENTRY) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_1) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_SPOT_2) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_HTML_VIDEO_SPOT_1) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_HTML_VIDEO_SPOT_1) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_HTML_SPOT_1) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_1) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_HTML_SPOT_2) ||
                source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT)) {
        	EditableHTMLPromotionBO hp = new EditableHTMLPromotionBO(source);
            promo = hp;
        }
        else if (source.getType().equalsIgnoreCase(PromotionIntf.PRODUCT_IMAGE_1) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_1) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_2) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_3) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_1) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_2) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_1) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_2) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_LEFT_COL_IMAGE_SPOT_1) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_VIDEO_POP_SPOT_1) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_VIDEO_POP_SPOT_1) ||
        		 source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_1) ||
                 source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_2) ||
                 source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_3) ||
                 source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120) ||
                 source.getType().equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90) ||
                 source.getType().equalsIgnoreCase(PromotionIntf.TEMPLATE_NAV_PROMO_150x90) ||
                 source.getType().equalsIgnoreCase(PromotionIntf.IMAGE_BOTTOM)||
                 source.getType().equalsIgnoreCase(PromotionIntf.IMAGE_COMPLETE) ||
                 source.getType().equalsIgnoreCase(PromotionIntf.ORDER_PATH_POP_OVERLAY) ) {
        	EditableImagePromotionBO ip = new EditableImagePromotionBO(source);
            promo = ip;
        }
        else {
        	EditablePromotionBO p = new EditablePromotionBO(source);
            promo = p;
        }
        return promo;
    }

    /**
     * 
     *
     */
    public EditablePromotionBO() {
		super();
	}

	/**
     * 
     * @param promotion
     */
	public EditablePromotionBO(PromotionIntf promotion) {
		this.altName = promotion.getAltName();
		this.fulfillText = promotion.getFulfillText();
		this.fulfillUrl = promotion.getFulfillUrl();
		this.keyCode = promotion.getKeyCode();
		this.pubCode = promotion.getPubCode();
		this.name = promotion.getName();
		this.type = promotion.getType();
		this.changed = false;
	}

	/**
	 * 
	 */
	public void setAltName(String string) throws UsatException {
		if (string == null)  {
			string = "";
		}
		else if (string.trim().length() > 50) {
            throw new UsatException("EditablePromotionBO::setAltName() - AltName max size = 50 chars");
        }
		altName = string.trim();
		this.changed = true;
	}

	/**
	 * @param string
	 */
	public void setFulfillText(String string) throws UsatException {
		if (string == null)  {
			fulfillText = "";
		}
		else {	
	        if (string.trim().length() > 1000) {
	        	throw new UsatException("EditablePromotionBO: Maximum length of fulfill text exceeded. Cannot exceed 1000 characters including any HTML tags. Current Size: " + string.trim().length());
	        }	
	        else    {
	        	fulfillText = string.trim();
	        }

		}
        this.changed = true;
		
	}

	/**
	 * @param string
	 */
	public void setFulfillUrl(String string)  throws UsatException {
		if (string == null){
			string = "";
		}
        if (string.trim().length() > 512) {
            throw new UsatException("EditablePromotionBO::setFulfillUrl() - FulfillUrl max size = 512 chars");
        }
		fulfillUrl = string.trim();
        this.changed = true;
	}

	/**
	 * @param string
	 */
	public void setKeyCode(String string)  throws UsatException {
        if (string == null || string.trim().length() > 5) {
            throw new UsatException("EditablePromotionBO::setKeycode() - Keycode max size = 5 chars");
        }
		this.keyCode = string.trim();
        this.changed = true;
	}

	/**
	 * @param string
	 */
	public void setName(String string)  throws UsatException {
        if (string == null || string.trim().length() > 100) {
            throw new UsatException("EditablePromotionBO::setName() - name max size = 100 chars");
        }
		this.name = string.trim();
        this.changed = true;
	}

	/**
	 * @param string
	 */
	public void setPubCode(String string)  throws UsatException {
        if (string == null || string.trim().length() > 2) {
            throw new UsatException("EditablePromotionBO::setPubcode() - pubcode max size = 2 chars: " + string);
        }
		this.pubCode = string.trim();
        this.changed = true;
	}

	/**
	 * @param string
	 */
	public void setType(String string)  throws UsatException {
        if (string == null || string.trim().length() > 50) {
            throw new UsatException("EditablePromotionBO::setType() - type max size = 50 chars");
        }
		type = string.trim();
		if (type.equalsIgnoreCase(PromotionIntf.PRODUCT_IMAGE_1) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_1) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_2) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_IMAGE_SPOT_3) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_1) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_IMAGE_SPOT_2) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_1) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_RIGHT_COL_IMAGE_SPOT_2) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_THANK_YOU_LEFT_COL_IMAGE_SPOT_1) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_RIGHT_COL_VIDEO_POP_SPOT_1) ||
			type.equalsIgnoreCase(PromotionIntf.ONE_PAGE_LEFT_COL_VIDEO_POP_SPOT_1) ||
			type.equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_1) ||
			type.equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_2) ||
			type.equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_3) ||
			type.equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120) ||
			type.equalsIgnoreCase(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90) ||
			type.equalsIgnoreCase(PromotionIntf.TEMPLATE_NAV_PROMO_150x90) ||
			type.equalsIgnoreCase(PromotionIntf.IMAGE_BOTTOM)||
			type.equalsIgnoreCase(PromotionIntf.IMAGE_COMPLETE) ||
			type.equalsIgnoreCase(PromotionIntf.ORDER_PATH_POP_OVERLAY) ) {
			this.imagePromo = true;
		}	 
		else {
			this.imagePromo = false;
		}
		this.changed = true;
	}

	public String getAltName() {
		return this.altName;
	}

	public String getFulfillText() {
		return this.fulfillText;
	}

	public String getFulfillUrl() {
		return this.fulfillUrl;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public String getName() {
		return this.name;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public String getType() {
		return this.type;
	}

}
