package com.gannett.usatoday.adminportal.campaigns.promotions;

import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableHTMLPromotionIntf;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

public class EditableHTMLPromotionBO extends EditablePromotionBO implements
		EditableHTMLPromotionIntf {

	public EditableHTMLPromotionBO() {
		super();
		
	}

	public EditableHTMLPromotionBO(PromotionIntf promotion) {
		super(promotion);
	}

	public String getPromotionalHTML() {
        return this.getFulfillText();
	}
	
	public void setPromotionalHTML(String html) throws UsatException {
		this.setFulfillText(html);
	}

}
