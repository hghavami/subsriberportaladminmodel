package com.gannett.usatoday.adminportal.campaigns.promotions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableCreditCardPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableHTMLPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
import com.gannett.usatoday.adminportal.integration.campaigns.XtrntPromoUpdaterDAO;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionManager;
import com.usatoday.businessObjects.products.promotions.PromotionSet;

public class EditablePromotionsSetBO implements EditablePromotionSetIntf {

	private PromotionSet defaultBrandingPubPromotionSet = null;
	private PromotionSet defaultPubPromotionSet = null;	
	
	HashMap<String, EditablePromotionIntf> promos = new HashMap<String, EditablePromotionIntf>();
    private String pubCode = null;
    private String keyCode = null;
    
    private boolean changed = false;
    
    public static EditablePromotionSetIntf fetchPromotionSetForCampaign(UsatCampaignIntf c) throws UsatException {
        XtrntPromoUpdaterDAO dao = new XtrntPromoUpdaterDAO();
        
        Collection<PromotionIntf> promos = dao.fetchPromosByPubcodeKeycode(c.getPubCode(), c.getKeyCode());
        
        EditablePromotionSetIntf set = null;
        if (promos.size() == 0) {
        	set = new EditablePromotionsSetBO();
        	set.setPubCode(c.getPubCode());
        	set.setKeyCode(c.getKeyCode());
        }
        else {
            set = new EditablePromotionsSetBO(promos, c.getPubCode(), c.getKeyCode());
        }
        
        return set;
    	
    }

    /**
     * 
     *
     */
    public EditablePromotionsSetBO() {
    	super();
    }
    
    /**
     * 
     * @param promotions
     * @param pubCode
     * @param keyCode
     */
    public EditablePromotionsSetBO(Collection<PromotionIntf> promotions, String pubCode, String keyCode) {
        super();
        
        this.pubCode = pubCode;
        this.keyCode = keyCode;
                
        for (PromotionIntf p : promotions) {
            if (this.promos.containsKey(p.getType())) {
                if (p.getType().equalsIgnoreCase(PromotionIntf.CREDIT_CARD)) {
                    EditableCreditCardPromotionBO cBO = (EditableCreditCardPromotionBO)this.promos.get(p.getType());
                    try {
                        cBO.addCard(p);
                    }
                    catch (Exception e) {
                        System.out.println("Failed to add card to promotion set: " + e.getMessage());
                    }
                }
            }
            else {
                try {
                    this.promos.put(p.getType(), EditablePromotionBO.createEditablePromotionBO(p));
                }
                catch (Exception e) {
                    System.out.println("Failed to create PromotionBO: " + e.getMessage());
                }
            }        	
        }
        
        initializeDefaultSets();
    }
    
    public EditablePromotionsSetBO(EditablePromotionSetIntf original) {
        super();
        
        this.pubCode = original.getPubCode();
        this.keyCode = original.getKeyCode();
        
        for (EditablePromotionIntf p : original.getPromoConfigsCollection()) {
            if (this.promos.containsKey(p.getType())) {
                if (p.getType().equalsIgnoreCase(PromotionIntf.CREDIT_CARD)) {
                    EditableCreditCardPromotionBO cBO = (EditableCreditCardPromotionBO)this.promos.get(p.getType());
                    try {
                        cBO.addCard(p);
                    }
                    catch (Exception e) {
                        System.out.println("Failed to add card to promotion set: " + e.getMessage());
                    }
                }
            }
            else {
                try {
                    this.promos.put(p.getType(), EditablePromotionBO.createEditablePromotionBO(p));
                }
                catch (Exception e) {
                    System.out.println("Failed to create PromotionBO: " + e.getMessage());
                }
            }        	
        }
        
        initializeDefaultSets();
    }

    private void initializeDefaultSets() {
        try {
        	// get this product
        	SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(this.pubCode);
        	// get branding product (may be the same)
        	SubscriptionProductIntf brandingProduct = SubscriptionProductBO.getSubscriptionProduct(product.getBrandingPubCode());
        	
        	// if product code doesn't match the branding pub code set up both defaults
        	if (!product.getProductCode().equalsIgnoreCase(product.getBrandingPubCode())) {
            	// set the default branding promotions set
        		this.defaultBrandingPubPromotionSet = PromotionManager.getInstance().getPromotionsForOffer(brandingProduct.getProductCode(), PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);
        		
        	}
        		
    		//  set default set if this offer is not for the default keycode 
    		if (!this.keyCode.equalsIgnoreCase(PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE)) {
    			this.defaultPubPromotionSet = PromotionManager.getInstance().getPromotionsForOffer(product.getProductCode(), PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);
    		}
        }
        catch (Exception e) {
        	e.printStackTrace();
		}
    	
    }
    public void clearAllPromoConfigurations() {
		this.getPromos().clear();
		this.changed = true;
	}

    public void clearProductImages() {
		try {
            this.getPromos().remove(PromotionIntf.PRODUCT_IMAGE_1);			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        this.changed = true;    	
    }
    
	public void clearCompletePageConfigurations() {
		try {
            this.getPromos().remove(PromotionIntf.IMAGE_COMPLETE);			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        this.changed = true;
	}

	public void clearPaymentPageConfigurations() {
		try {
            this.getPromos().remove(PromotionIntf.CREDIT_CARD);
            this.getPromos().remove(PromotionIntf.EZPAY_PROMO);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        this.changed = true;
	}

	public void clearNewOrderPopOverlayConfigurations() {
		try {
            this.getPromos().remove(PromotionIntf.ORDER_PATH_POP_OVERLAY);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        this.changed = true;
	}
	
	public void clearOnePageOrderConfigurations() {
		try {
            this.getPromos().remove(PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT);
            this.getPromos().remove(PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1);
            this.getPromos().remove(PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT);
            this.getPromos().remove(PromotionIntf.ALLOW_OFFER_OVERRIDE);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        this.changed = true;
		
	}
	
	public void clearRateOverrideConfigurations() {
		try {
            this.getPromos().remove(PromotionIntf.FORCE_EZPAY_OFFER);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        this.changed = true;
	}

	public void clearLandingPageConfigurations() {
        try {
            this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_TEXT);
            this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_1);
            this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_2);
            this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_3);
            this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120);  /// footer image
            this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90);
        }catch (Exception e) {
			e.printStackTrace();
        }
	}

	public void clearNavigationConfigurations() {
		try {
            this.getPromos().remove(PromotionIntf.TEMPLATE_NAV_PROMO_150x90);
            this.getPromos().remove(PromotionIntf.DYNAMIC_NAVIGATION_CUSTSERV);
            this.getPromos().remove(PromotionIntf.DYNAMIC_NAVIGATION_ORDER_ENTRY);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        this.changed = true;
	}

	public void clearTermsPageConfigurations() {
		try {
            this.getPromos().remove(PromotionIntf.TERMS_AND_CONDITIONS_TEXT);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        this.changed = true;
	}

	public EditablePromotionIntf getDeliveryNotification() {
        try {
            return (EditablePromotionIntf)this.getPromos().get(PromotionIntf.DELIVERY_NOTIFICATION);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditablePromotionIntf getForceEZPAY() {
        try {
            return (EditablePromotionIntf)this.getPromos().get(PromotionIntf.FORCE_EZPAY_OFFER);
        }
        catch (Exception e) {
            return null;
        }
	}

	public boolean getIsChanged() {
		return this.changed;
	}

	public EditableImagePromotionIntf getLandingPagePromoImage1() {
        try {
            return (EditableImagePromotionIntf)this.getPromos().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_1);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableImagePromotionIntf getLandingPagePromoImage2() {
        try {
            return (EditableImagePromotionIntf)this.getPromos().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_2);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableImagePromotionIntf getLandingPagePromoImage3() {
        try {
            return (EditableImagePromotionIntf)this.getPromos().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_3);
        }
        catch (Exception e) {
            return null;
        }
	}

	public HashMap<String, EditablePromotionIntf> getPromoConfigsHashMap() {
		return this.promos;
	}

	public Collection<EditablePromotionIntf> getPromoConfigsCollection() {
		return this.promos.values();
	}

	public EditableCreditCardPromotionIntf getPromoCreditCard() {
        try {
            return (EditableCreditCardPromotionIntf)this.getPromos().get(PromotionIntf.CREDIT_CARD);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableHTMLPromotionIntf getPromoEZPayCustomText() {
        try {
            return (EditableHTMLPromotionIntf)this.getPromos().get(PromotionIntf.EZPAY_PROMO);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableImagePromotionIntf getPromoImageComplete() {
        try {
            return (EditableImagePromotionIntf)this.getPromos().get(PromotionIntf.IMAGE_COMPLETE);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableHTMLPromotionIntf getDynamicNavigationOrderEntryHTML() {
        try {
            return (EditableHTMLPromotionIntf)this.getPromos().get(PromotionIntf.DYNAMIC_NAVIGATION_ORDER_ENTRY);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableHTMLPromotionIntf getDynamicNavigationCustomerServiceHTML() {
        try {
            return (EditableHTMLPromotionIntf)this.getPromos().get(PromotionIntf.DYNAMIC_NAVIGATION_CUSTSERV);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableHTMLPromotionIntf getPromoLandingPageText() {
        try {
            return (EditableHTMLPromotionIntf)this.getPromos().get(PromotionIntf.LANDING_PAGE_PROMO_TEXT);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableImagePromotionIntf getLandingPageFooterPromoImage() {
        try {
            return (EditableImagePromotionIntf)this.getPromos().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableImagePromotionIntf getRightColumnUpperPromoImageLandingPage() {
        try {
            return (EditableImagePromotionIntf)this.getPromos().get(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableImagePromotionIntf getTemplateNavigationPromoImage() {
        try {
            return (EditableImagePromotionIntf)this.getPromos().get(PromotionIntf.TEMPLATE_NAV_PROMO_150x90);
        }
        catch (Exception e) {
            return null;
        }
	}

	public EditableHTMLPromotionIntf getTermsAndConditionsText() {
        try {
            return (EditableHTMLPromotionIntf)this.getPromos().get(PromotionIntf.TERMS_AND_CONDITIONS_TEXT);
        }
        catch (Exception e) {
            return null;
        }
	}

	public void setDeliveryNotification(EditablePromotionIntf promo) {
		try {
			
			this.getPromos().remove(PromotionIntf.DELIVERY_NOTIFICATION);
			
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.DELIVERY_NOTIFICATION, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setForceEZPAY(EditablePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.FORCE_EZPAY_OFFER);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.FORCE_EZPAY_OFFER, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setLandingPagePromoImage1(
			EditableImagePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_1);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_1, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setLandingPagePromoImage2(
			EditableImagePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_2);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_2, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setLandingPagePromoImage3(EditableImagePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_3);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_3, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setPromoCreditCard(EditableCreditCardPromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.CREDIT_CARD);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.CREDIT_CARD, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setPromoEZPayCustomText(EditableHTMLPromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.EZPAY_PROMO);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.EZPAY_PROMO, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void setPromoImageComplete(EditableImagePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.IMAGE_COMPLETE);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.IMAGE_COMPLETE, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setDynamicNavigationOrderEntryHTML(EditableHTMLPromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.DYNAMIC_NAVIGATION_ORDER_ENTRY);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.DYNAMIC_NAVIGATION_ORDER_ENTRY, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setDynamicNavigationCustomerServiceHTML(EditableHTMLPromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.DYNAMIC_NAVIGATION_CUSTSERV);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.DYNAMIC_NAVIGATION_CUSTSERV, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setPromoLandingPageText(EditableHTMLPromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_TEXT);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.LANDING_PAGE_PROMO_TEXT, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setLandingPageFooterPromoImage(
			EditableImagePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setRightColumnUpperPromoImageLandingPage(
			EditableImagePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setTemplateNavigationPromoImage(EditableImagePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.TEMPLATE_NAV_PROMO_150x90);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.TEMPLATE_NAV_PROMO_150x90, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setTermsAndConditionsText(EditableHTMLPromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.TERMS_AND_CONDITIONS_TEXT);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.TERMS_AND_CONDITIONS_TEXT, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getKeyCode() {
		return keyCode;
	}

	public String getPubCode() {
		return pubCode;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public HashMap<String, EditablePromotionIntf> getPromos() {
		return promos;
	}

	public void setPromos(HashMap<String, EditablePromotionIntf> promos) {
		this.promos = promos;
	}

	/**
	 * Returns a collection of image type promotions
	 */
	public Collection<EditableImagePromotionIntf> getAllImagePromotions() {
		ArrayList<EditableImagePromotionIntf> imagePromos = new ArrayList<EditableImagePromotionIntf>();
		
		for (EditablePromotionIntf promo : this.promos.values()) {
			if (promo instanceof EditableImagePromotionIntf) {
				imagePromos.add((EditableImagePromotionIntf)promo);
			}
		}
		
		return imagePromos;
	}

	public EditableImagePromotionIntf getNewSubscriptionOrderPopUpOverlay() {
        try {
            return (EditableImagePromotionIntf)this.getPromos().get(PromotionIntf.ORDER_PATH_POP_OVERLAY);
        }
        catch (Exception e) {
            return null;
        }
	}

	public void setNewSubscriptionOrderPopUpOverlay(
			EditableImagePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.ORDER_PATH_POP_OVERLAY);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.ORDER_PATH_POP_OVERLAY, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public EditableHTMLPromotionIntf getOnePagePromoSpot1Text() {
        try {
            return (EditableHTMLPromotionIntf)this.getPromos().get(PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1);
        }
        catch (Exception e) {
        	e.printStackTrace();
            return null;
        }
	}

	public void setOnePagePromoSpot1Text(EditableHTMLPromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public EditableHTMLPromotionIntf getOnePageDisclaimerText() {
        try {
            return (EditableHTMLPromotionIntf)this.getPromos().get(PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT);
        }
        catch (Exception e) {
            return null;
        }
	}

	public void setOnePageDisclaimerText(EditableHTMLPromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void clearForceEZPAYOverrides() {
		try {
            this.getPromos().remove(PromotionIntf.FORCE_EZPAY_OFFER);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
        this.changed = true;
	}

	public EditableHTMLPromotionIntf getOnePageForceEZPayTermsText() {
        try {
            return (EditableHTMLPromotionIntf)this.getPromos().get(PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT);
        }
        catch (Exception e) {
            return null;
        }
	}

	public void setOnePageForceEZPayTermsText(EditableHTMLPromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public PromotionSet getDefaultBrandingPubPromotionSet() {
		return defaultBrandingPubPromotionSet;
	}

	public PromotionSet getDefaultPubPromotionSet() {
		return defaultPubPromotionSet;
	}

	public EditablePromotionIntf getAllowOfferCodeOverrides() {
        try {
            return (EditablePromotionIntf)this.getPromos().get(PromotionIntf.ALLOW_OFFER_OVERRIDE);
        }
        catch (Exception e) {
            return null;
        }
	}

	public void setAllowOfferCodeOverrides(EditablePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.ALLOW_OFFER_OVERRIDE);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.ALLOW_OFFER_OVERRIDE, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@Override
	public EditableImagePromotionIntf getProductImage1() {
        try {
            return (EditableImagePromotionIntf)this.getPromos().get(PromotionIntf.PRODUCT_IMAGE_1);
        }
        catch (Exception e) {
            return null;
        }
	}

	@Override
	public void setProductImage1(EditableImagePromotionIntf promo) {
		try {
			this.getPromos().remove(PromotionIntf.PRODUCT_IMAGE_1);
			if (promo != null) {
		        promo.setPubCode(this.pubCode);
		        promo.setKeyCode(this.keyCode);
				this.getPromos().put(PromotionIntf.PRODUCT_IMAGE_1, promo);
			}
			this.changed = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
