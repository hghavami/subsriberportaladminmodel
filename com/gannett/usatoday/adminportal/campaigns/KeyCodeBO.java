package com.gannett.usatoday.adminportal.campaigns;

import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usatoday.adminportal.campaigns.intf.KeyCodeIntf;
import com.gannett.usatoday.adminportal.integration.XtrntkeyrtDAO;
import com.usatoday.UsatException;
import com.usatoday.util.constants.UsaTodayConstants;

public class KeyCodeBO implements KeyCodeIntf {

	private String sourceCode = "";
    private String promoCode = "";
    private String contestCode = "";
    private String offerDescription = "";
    private String pubCode = "";
    
    public static Collection<KeyCodeIntf> fetchKeycodesForUT() throws UsatException {
    	ArrayList<KeyCodeIntf> keycodes = new ArrayList<KeyCodeIntf>();
    	
    	XtrntkeyrtDAO dao = new XtrntkeyrtDAO();
    	
    	Collection<KeyCodeIntf> codes = dao.getKeycodesForPub(UsaTodayConstants.UT_PUBCODE);
    	
    	for (KeyCodeIntf c : codes) {
    		KeyCodeBO b = new KeyCodeBO(c);
    		keycodes.add(b);
    	}
    	
    	return keycodes;
    }
    
    public static Collection<KeyCodeIntf> fetchKeycodesForSW() throws UsatException {
    	ArrayList<KeyCodeIntf> keycodes = new ArrayList<KeyCodeIntf>();
    	
    	XtrntkeyrtDAO dao = new XtrntkeyrtDAO();
    	
    	Collection<KeyCodeIntf> codes = dao.getKeycodesForPub(UsaTodayConstants.SW_PUBCODE);
    	
    	for (KeyCodeIntf c : codes) {
    		KeyCodeBO b = new KeyCodeBO(c);
    		keycodes.add(b);
    	}
    	
    	return keycodes;
    }

    public static KeyCodeIntf fetchKeycodeForPubAndKeycode(String pub, String keycode) throws UsatException {
    	KeyCodeIntf code = null;
    	
    	XtrntkeyrtDAO dao = new XtrntkeyrtDAO();
    	
    	code = dao.getKeycodeForPubAndKeycode(pub, keycode);
    	
    	return code;
    }
    
    public static Collection<KeyCodeIntf> fetchKeycodesForPub(String pub) throws UsatException {
    	ArrayList<KeyCodeIntf> keycodes = new ArrayList<KeyCodeIntf>();
    	
    	XtrntkeyrtDAO dao = new XtrntkeyrtDAO();
    	
    	Collection<KeyCodeIntf> codes = dao.getKeycodesForPub(pub);
    	
    	for (KeyCodeIntf c : codes) {
    		KeyCodeBO b = new KeyCodeBO(c);
    		keycodes.add(b);
    	}
    	
    	return keycodes;
    }

	@SuppressWarnings("unused")
	private KeyCodeBO() {
		super();
	}
	
	public KeyCodeBO(KeyCodeIntf source) {
		this.sourceCode = source.getSourceCode();
		this.promoCode = source.getPromoCode();
		this.contestCode = source.getContestCode();
		this.offerDescription = source.getOfferDescription();
		this.pubCode = source.getPubCode();
	}
	
	public String getContestCode() {
		return this.contestCode;
	}

	public String getKeyCode() {
        return (new String(sourceCode + promoCode + contestCode));
	}

	public String getOfferDescription() {
		return this.offerDescription;
	}

	public String getPromoCode() {
		return this.promoCode;
	}

	public String getSourceCode() {
		return this.sourceCode;
	}

	public int compareTo(KeyCodeIntf o) {
		return this.getKeyCode().compareTo(o.getKeyCode());
	}

	public String getPubCode() {
		return pubCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

}
