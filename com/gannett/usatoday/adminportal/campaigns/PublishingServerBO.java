package com.gannett.usatoday.adminportal.campaigns;

import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerTOIntf;
import com.gannett.usatoday.adminportal.integration.campaigns.PublishingServerDAO;
import com.usatoday.UsatException;

public class PublishingServerBO implements PublishingServerIntf {

    private int serverId = 0;
    private boolean active = false;
    private String host = null;
    private String uid = null;
    private String pwd = null;
    private String serverType = null;
    private String publishingMethod = null;
    private String publishRootDir = null;
    private String redirectURL = null;
    private String redirectURL1 = null;    
    private String JNDILookup = null;
    private String JNDILookupV2 = null;
    private String publicHostURL = null;
	

    /**
     * 
     * @return A collection of the active test servers
     * @throws UsatException
     */
    public static Collection<PublishingServerIntf> fetchTestServers() throws UsatException {
    
    	ArrayList<PublishingServerIntf> testServers = new ArrayList<PublishingServerIntf>();
    	// query the database using the PublishingServerDAO to get all test servers
    	// convert the collection of transfer objects to BO objects.
    	PublishingServerDAO dao = new PublishingServerDAO();
    	Collection<PublishingServerTOIntf> serverTOs = dao.fetchTestServers();
    	for (PublishingServerTOIntf to : serverTOs) {
    		PublishingServerBO s = new PublishingServerBO(to);
    		testServers.add(s);
    	}
    	
    	return testServers;
    	
    }

    /**
     * 
     * @return A collection of all active publishing servers
     * @throws UsatException
     */
    public static Collection<PublishingServerIntf> fetchAllActiveServers() throws UsatException {
        
    	ArrayList<PublishingServerIntf> servers = new ArrayList<PublishingServerIntf>();
    	// query the database using the PublishingServerDAO to get all test servers
    	// convert the collection of transfer objects to BO objects.
    	PublishingServerDAO dao = new PublishingServerDAO();
    	Collection<PublishingServerTOIntf> serverTOs = dao.fetchAllActiveServers();
    	for (PublishingServerTOIntf to : serverTOs) {
    		PublishingServerBO s = new PublishingServerBO(to);
    		servers.add(s);
    	}
    	
    	return servers;
    	
    }

    /**
     * 
     * @return A collection of all active production servers
     * @throws UsatException
     */
    public static Collection<PublishingServerIntf> fetchProductionServers() throws UsatException {
    	ArrayList<PublishingServerIntf> prodServers = new ArrayList<PublishingServerIntf>();
    	// query the database using the PublishingServerDAO to get all prod servers
    	// convert the collection of transfer objects to BO objects.
    	PublishingServerDAO dao = new PublishingServerDAO();
    	Collection<PublishingServerTOIntf> serverTOs = dao.fetchProductionServers();
    	for (PublishingServerTOIntf to : serverTOs) {
    		PublishingServerBO s = new PublishingServerBO(to);
    		prodServers.add(s);
    	}
    	return prodServers;
    }
    
	public PublishingServerBO(PublishingServerIntf source) {
		super();
		this.serverId = source.getServerId();
		this.active = source.isActive();
		this.host = source.getHost();
		this.JNDILookup = source.getJNDILookup();
		this.JNDILookupV2 = source.getJNDILookupV2();
		this.publicHostURL = source.getPublicHostURL();
		this.publishingMethod = source.getPublishingMethod();
		this.publishRootDir = source.getPublishRootDir();
		this.redirectURL = source.getRedirectURL();
		this.redirectURL1 = source.getRedirectURL1();
		this.serverType = source.getServerType();
		this.uid = source.getFTPUid();
		this.pwd = source.getFTPPwd();
	}

	public String getFTPPwd() {
		return this.pwd;
	}

	public String getFTPUid() {
		return this.uid;
	}

	public String getHost() {
		return this.host;
	}

	public String getJNDILookup() {
		return this.JNDILookup;
	}

	public String getPublicHostURL() {
		return this.publicHostURL;
	}

	public String getPublishRootDir() {
		return this.publishRootDir;
	}

	public String getPublishingMethod() {
		return this.publishingMethod;
	}

	public String getRedirectURL() {
		return this.redirectURL;
	}

	public String getRedirectURL1() {
		return this.redirectURL1;
	}

	public int getServerId() {
		return this.serverId;
	}

	public String getServerType() {
		return this.serverType;
	}

	public boolean isActive() {
		return this.active;
	}

	public String getJNDILookupV2() {
		return JNDILookupV2;
	}
}
