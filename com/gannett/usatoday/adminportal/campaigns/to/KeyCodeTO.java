package com.gannett.usatoday.adminportal.campaigns.to;

import java.io.Serializable;

import com.gannett.usatoday.adminportal.campaigns.intf.KeyCodeIntf;

public class KeyCodeTO implements KeyCodeIntf, Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 295655489108169584L;
	private String sourceCode = "";
    private String promoCode = "";
    private String contestCode = "";
    private String offerDescription = "";
    private String pubCode = "";

	public String getContestCode() {
		return this.contestCode;
	}

	public String getKeyCode() {
        return (new String(sourceCode + promoCode + contestCode));
	}

	public String getOfferDescription() {
		return this.offerDescription;
	}

	public String getPromoCode() {
		return this.promoCode;
	}

	public String getSourceCode() {
		return this.sourceCode;
	}

	public void setContestCode(String contestCode) {
		this.contestCode = contestCode;
	}

	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public int compareTo(KeyCodeIntf o) {
		return this.getKeyCode().compareTo(o.getKeyCode());
	}

	public String getPubCode() {
		return pubCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

}
