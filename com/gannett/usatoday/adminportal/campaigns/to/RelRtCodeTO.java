package com.gannett.usatoday.adminportal.campaigns.to;

import java.io.Serializable;

import com.gannett.usatoday.adminportal.campaigns.intf.RelRtCodeIntf;

public class RelRtCodeTO implements RelRtCodeIntf, Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 295655489108169585L;
	private String pubCode = "";
	private String deliveryMethod = "";
	private String frequencyOfDelivery = "";
	private String piaRateCode = "";
	private String relOfferDescription = "";
	private String relPeriodLength = "";
	

	public String getPubCode() {
		return pubCode;
	}

	public String getDeliveryMethod() {
		return this.deliveryMethod;
	}

	public String getFrequencyOfDelivery() {
		return this.frequencyOfDelivery;
	}

	public String getPiaRateCode() {
		return this.piaRateCode;
	}

	public String getRelOfferDescription() {
		return this.relOfferDescription;
	}

	public String getRelPeriodLength() {
		return this.relPeriodLength;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public void setFrequencyOfDelivery(String frequencyOfDelivery) {
		this.frequencyOfDelivery = frequencyOfDelivery;
	}

	public void setPiaRateCode(String piaRateCode) {
		this.piaRateCode = piaRateCode;
	}

	public void setRelOfferDescription(String relOfferDescription) {
		this.relOfferDescription = relOfferDescription;
	}

	public void setRelPeriodLength(String relPeriodLength) {
		this.relPeriodLength = relPeriodLength;
	}


}
