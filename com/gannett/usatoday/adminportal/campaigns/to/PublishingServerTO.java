package com.gannett.usatoday.adminportal.campaigns.to;

import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerTOIntf;

public class PublishingServerTO implements PublishingServerTOIntf {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7076440813215150366L;
	private int serverId = 0;
    private boolean active = false;
    private String host = null;
    private String uid = null;
    private String pwd = null;
    private String serverType = null;
    private String publishingMethod = null;
    private String publishRootDir = null;
    private String redirectURL = null;
    private String redirectURL1 = null;    
    private String JNDILookup = null;
    private String JNDILookupV2 = null;
    private String publicHostURL = null;
	
	
	public void setActive(boolean active) {
		this.active = active;
	}

	public void setFTPPwd(String ftpPwd) {
		this.pwd = ftpPwd;
	}

	public void setFTPUid(String ftpUid) {
		this.uid = ftpUid;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setJNDILookup(String jndiName) {
		this.JNDILookup = jndiName;
	}

	public void setPublicHostURL(String hostURL) {
		this.publicHostURL = hostURL;
	}

	public void setPublishRootDir(String rootDir) {
		this.publishRootDir = rootDir;
	}

	public void setPublishingMethod(String method) {
		this.publishingMethod = method;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	public void setRedirectURL1(String redirectURL1) {
		this.redirectURL1 = redirectURL1;
	}

	public void setServerId(int primaryKey) {
		this.serverId = primaryKey;
	}

	public void setServerType(String type) {
		this.serverType = type;
	}

	public String getFTPPwd() {
		return this.pwd;
	}

	public String getFTPUid() {
		return this.uid;
	}

	public String getHost() {
		return this.host;
	}

	public String getJNDILookup() {
		return this.JNDILookup;
	}

	public String getPublicHostURL() {
		return this.publicHostURL;
	}

	public String getPublishRootDir() {
		return this.publishRootDir;
	}

	public String getPublishingMethod() {
		return this.publishingMethod;
	}

	public String getRedirectURL() {
		return this.redirectURL;
	}

	public String getRedirectURL1() {
		return this.redirectURL1;
	}

	public int getServerId() {
		return this.serverId;
	}

	public String getServerType() {
		return this.serverType;
	}

	public boolean isActive() {
		return this.active;
	}

	public String getJNDILookupV2() {
		return JNDILookupV2;
	}

	public void setJNDILookupV2(String lookupV2) {
		JNDILookupV2 = lookupV2;
	}
}
