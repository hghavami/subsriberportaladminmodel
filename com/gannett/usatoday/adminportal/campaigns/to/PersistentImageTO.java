package com.gannett.usatoday.adminportal.campaigns.to;

import java.io.Serializable;

import com.gannett.usatoday.adminportal.campaigns.intf.PersistentImageIntf;

public class PersistentImageTO implements Serializable, PersistentImageIntf {

	/**
	 * 
	 */
	private static final long serialVersionUID = 353958317478017539L;
	private String imageFileType;
	private String imageName;
	private byte[] contents;
	private boolean protectedImage = false;
	
	public PersistentImageTO() {
	}

	public byte[] getImageContents() {
		
		return this.contents;
	}

	public String getImageFileType() {
		return this.imageFileType;
	}

	public String getImageName() {
		return this.imageName;
	}

	public void setImageContents(byte[] contents) {
		this.contents = contents;
	}

	public void setImageFileType(String type) {
		this.imageFileType = type;
	}

	public void setImageName(String name) {
		this.imageName = name;
	}

	public boolean isProtectedImage() {
		return protectedImage;
	}

	public void setProtectedImage(boolean protectedImage) {
		this.protectedImage = protectedImage;
	}

}
