package com.gannett.usatoday.adminportal.campaigns.to;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignTOIntf;

public class UsatCampaignTO implements UsatCampaignTOIntf, Serializable {

	private int primaryKey = -1;
	private int campaignState = 0;
	private String createdBy = null;
	private String updatedBy = null;
	private int type = 0;
	private DateTime createdTime = null;
	private DateTime updatedTime = null;
	private String pubCode = null;
	private String keyCode = null;
	private String campaignName = null;
	private String vanityURL = null;
	private String redirectURL = null;
	private String groupName = null;
	private String redirectToPage = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsatCampaignTO() {
		super();
	}

	/**
	 * 
	 * @param newCampaign Generates a transfer object for a campaign.
	 */
	public UsatCampaignTO (UsatCampaignIntf newCampaign) {
		this.primaryKey = newCampaign.getPrimaryKey();
		this.campaignState = newCampaign.getCampaignState();
		this.createdBy = newCampaign.getCreatedBy();
		this.updatedBy = newCampaign.getUpdatedBy();
		this.type = newCampaign.getType();
		this.createdTime = newCampaign.getCreatedTime();
		this.updatedTime = newCampaign.getUpdatedTime();
		this.pubCode = newCampaign.getPubCode();
		this.keyCode = newCampaign.getKeyCode();
		this.campaignName = newCampaign.getCampaignName();
		this.vanityURL = newCampaign.getVanityURL();
		this.redirectURL = newCampaign.getRedirectURL();
		this.groupName = newCampaign.getGroupName();
		this.redirectToPage = newCampaign.getRedirectToPage();
		
	}
	
	public String getGroupName() {
		return this.groupName;
	}

	public String getCampaignName() {
		return this.campaignName;
	}

	public int getCampaignState() {
		return this.campaignState;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public DateTime getCreatedTime() {
		return this.createdTime;
	}

	public String getKeyCode() {
		return this.keyCode;
	}

	public int getPrimaryKey() {
		return this.primaryKey;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public String getRedirectToPage() {
		return this.redirectToPage;
	}

	public String getRedirectURL() {
		return this.redirectURL;
	}

	public String getVanityURL() {
		return this.vanityURL;
	}

	public int getType() {
		return this.type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public DateTime getUpdatedTime() {
		return this.updatedTime;
	}

	public void setGroupName(String group) {
		this.groupName = group;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public void setCampaignState(int campaignState) {
		this.campaignState = campaignState;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedTime(DateTime createdTime) {
		this.createdTime = createdTime;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public void setPrimaryKey(int primaryKey) {
		this.primaryKey = primaryKey;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

	public void setRedirectToPage(String redirectToPage) {
		this.redirectToPage = redirectToPage;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	public void setVanityURL(String vanity) {
		this.vanityURL = vanity;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedTime(DateTime updatedTime) {
		this.updatedTime = updatedTime;
	}

}
