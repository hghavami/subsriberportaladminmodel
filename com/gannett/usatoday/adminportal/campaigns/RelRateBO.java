package com.gannett.usatoday.adminportal.campaigns;

import com.gannett.usatoday.adminportal.campaigns.intf.RelRtCodeIntf;
import com.gannett.usatoday.adminportal.campaigns.to.RelRtCodeTO;
import com.gannett.usatoday.adminportal.integration.XtrntRelRtDAO;
import com.usatoday.UsatException;

public class RelRateBO implements RelRtCodeIntf {

	private String pubCode = "";
	private String deliveryMethod = "";
	private String frequencyOfDelivery = "";
	private String piaRateCode = "";
	private String relOfferDescription = "";
	private String relPeriodLength = "";

	public static RelRateBO fetchRateCode(String pubCode, String rateCode) throws UsatException {
		if (pubCode == null || rateCode == null) {
			throw new UsatException("Fetch Rate: Invalid input parms.");
		}
		
		XtrntRelRtDAO dao = new XtrntRelRtDAO();
		
		RelRtCodeTO result = dao.getRatesForPubAndRateCode(pubCode, rateCode);
	
		return new RelRateBO(result);
	}
	
	public RelRateBO (RelRtCodeIntf source) {
		if (source != null) {
			this.pubCode = source.getPubCode();
			this.deliveryMethod = source.getDeliveryMethod();
			this.frequencyOfDelivery = source.getFrequencyOfDelivery();
			this.piaRateCode = source.getPiaRateCode();
			this.relOfferDescription = source.getRelOfferDescription();
			this.relPeriodLength = source.getRelPeriodLength();
		}
	}
	
	public String getDeliveryMethod() {
		return this.deliveryMethod;
	}

	public String getFrequencyOfDelivery() {
		return this.frequencyOfDelivery;
	}

	public String getPiaRateCode() {
		return this.piaRateCode;
	}

	public String getPubCode() {
		return this.pubCode;
	}

	public String getRelOfferDescription() {
		return this.relOfferDescription;
	}

	public String getRelPeriodLength() {
		return this.relPeriodLength;
	}

}
