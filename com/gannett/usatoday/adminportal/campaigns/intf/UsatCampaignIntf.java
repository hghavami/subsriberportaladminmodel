package com.gannett.usatoday.adminportal.campaigns.intf;

import java.util.Collection;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
import com.gannett.usatoday.adminportal.campaigns.utils.CampaignPublisherFailure;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.usatoday.UsatException;

public interface UsatCampaignIntf extends UsatCampaignTOIntf {
    public static final int BASIC_VANITY_URL = 0;
    public static final int BASIC_SPLASH = 1;
    public static final int SIMPLE_ORDER = 2;
    public static final int TARGETED_ORDER = 3;
    
    public static final int NEW_STATE = 0;
    public static final int STAGED_STATE = 1;
	public static final int PUBLISHED_STATE = 2;
	public static final int REPUBLISH_STATE = 3; 

	// get product associated with this campaign
	public USATProductBO getProduct() throws UsatException;
	
	// setters
	public void setCampaignName(String name) throws UsatException;
	public void setCampaignState(int state) throws UsatException;
	public String getCampaignStateDescription();
	public void setCreatedBy(String userID) throws UsatException;
	public void setUpdatedBy(String userID) throws UsatException;
	public void setType(int campaignType) throws UsatException;
	public void setCreatedTime(DateTime created) throws UsatException;
	public void setPubCode(String publication) throws UsatException;
	public void setKeyCode(String keyCode) throws UsatException;
	public void setUpdatedTime(DateTime lastUpdated) throws UsatException;
	public void setVanityURL(String vanity) throws UsatException;
	public void setRedirectURL(String redirectURL) throws UsatException;
	public void setCampaignGroup(String groupName) throws UsatException;
	public void setRedirectToPage(String page) throws UsatException;
	
	// validation methods
	public boolean getIsChanged();
	public boolean getIsCampaignValid();
	public String getErrorMessage();
	
	// errors from publishing
	public Collection<CampaignPublisherFailure> getPublishFailures();
	public void setPublishFailures(Collection<CampaignPublisherFailure> publishFailures);
	
	// persistent methods
	public void save(boolean saveCampaignOnly) throws UsatException;
	public boolean delete() throws UsatException;
	
	// Promotion methods
	public void clearAllPromotionConfigs();
	public EditablePromotionSetIntf getPromotionSet();
	public void setPromotionSet(EditablePromotionSetIntf newSet);
}
