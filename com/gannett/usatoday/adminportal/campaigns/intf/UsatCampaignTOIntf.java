package com.gannett.usatoday.adminportal.campaigns.intf;

import org.joda.time.DateTime;

public interface UsatCampaignTOIntf {

	// all getter methods for database data with no setters or other business methods.
	public int getPrimaryKey();
	public int getCampaignState();
	public String getCreatedBy();
	public String getUpdatedBy();
	public int getType();
	public DateTime getCreatedTime();
	public DateTime getUpdatedTime();
	public String getPubCode();
	public String getKeyCode();
	public String getCampaignName();
	public String getVanityURL();
	public String getRedirectURL();
	public String getGroupName();
	public String getRedirectToPage();
}
