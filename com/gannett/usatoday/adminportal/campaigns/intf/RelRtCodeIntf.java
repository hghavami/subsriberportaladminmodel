package com.gannett.usatoday.adminportal.campaigns.intf;

public interface RelRtCodeIntf{

	public String getPubCode();
	public String getFrequencyOfDelivery();
	public String getDeliveryMethod();
	public String getPiaRateCode();
	public String getRelPeriodLength();
	public String getRelOfferDescription();
}
