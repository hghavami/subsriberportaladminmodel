package com.gannett.usatoday.adminportal.campaigns.intf;

public interface KeyCodeIntf extends Comparable<KeyCodeIntf>{

	public String getPubCode();
	public String getContestCode();
	public String getSourceCode();
	public String getPromoCode();
	public String getKeyCode();
	public String getOfferDescription();
}
