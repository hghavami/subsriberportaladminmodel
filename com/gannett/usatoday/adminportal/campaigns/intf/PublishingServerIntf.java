package com.gannett.usatoday.adminportal.campaigns.intf;

public interface PublishingServerIntf {
    public static final String TEST_SERVER_TYPE = "TEST";
    public static final String PROD_SERVER_TYPE = "PROD";
    
    public static final String FTP_PUBLISH = "FTP";
    public static final String FILE_COPY_PUBLISH = "FILECOPY";

    
	public String getHost();
	public String getJNDILookup();
	public String getJNDILookupV2();
	public String getPublicHostURL();
	public String getPublishingMethod();
	public String getPublishRootDir();
	public String getFTPPwd();
	public String getFTPUid();
	public String getRedirectURL();
    public String getRedirectURL1();	
	public int getServerId();
	public String getServerType();	
	public boolean isActive();
	
}
