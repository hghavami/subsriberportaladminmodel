package com.gannett.usatoday.adminportal.campaigns.intf;

public interface PersistentImageIntf {

	public String getImageName();
	public void setImageName(String name);
	public String getImageFileType();
	public void setImageFileType(String type);
	public byte[] getImageContents();
	public void setImageContents(byte[] contents);
	public boolean isProtectedImage();
}
