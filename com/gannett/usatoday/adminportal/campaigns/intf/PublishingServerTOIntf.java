package com.gannett.usatoday.adminportal.campaigns.intf;

import java.io.Serializable;

public interface PublishingServerTOIntf extends PublishingServerIntf, Serializable {

	public void setHost(String host);
	public void setJNDILookup(String jndiName);
	public void setPublicHostURL(String hostURL);
	public void setPublishingMethod(String method);
	public void setPublishRootDir(String rootDir);
	public void setFTPPwd(String ftpPwd);
	public void setFTPUid(String ftpUid);
	public void setRedirectURL(String redirectURL);
    public void setRedirectURL1(String redirectURL1);	
	public void setServerId(int primaryKey);
	public void setServerType(String type);	
	public void setActive(boolean active);

}
