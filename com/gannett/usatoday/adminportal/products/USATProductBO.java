package com.gannett.usatoday.adminportal.products;

import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.integration.ProductUpdaterDAO;
import com.gannett.usatoday.adminportal.products.intf.UpdateableProductIntf;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.PersistentProductIntf;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.suppliers.SupplierIntf;
import com.usatoday.business.interfaces.products.suppliers.SupplierTOIntf;
import com.usatoday.businessObjects.products.suppliers.SupplierBO;
import com.usatoday.integration.ProductDAO;
import com.usatoday.integration.ProductTO;
import com.usatoday.integration.SupplierDAO;

public class USATProductBO implements UpdateableProductIntf {

    private String taxRateCode = "";
    private String productCode = "";
    private String brandingPubCode = "UT";
    private String name = "";
    private String description = "";
    private String detailedDescription = "";
    private int minQuantityPerOrder = 1;
    private int maxQuantityPerOrder = 0;  // 0 indicates no maximum
    private int id = 0;
    protected boolean taxable = true;
    private int fulfillmentMethod = -1;
    protected int inventory = 0;
    protected double unitPrice = 0.0;
    private DateTime startDate = null;
    private DateTime endDate = null;
    private int supplierID = -1;
    private String customerServicePhone = null;
    private String defaultKeycode = null;
    private String expiredOfferKeycode = null;
    private int maxDaysInFutureBeforeFulfillment = 90;
    protected int holdDaysDelay = -1;
    private int productType = 0;
    
    protected SupplierIntf supplier = null;

	public static Collection<USATProductBO> fetchProducts() throws UsatException {
		ArrayList<USATProductBO> products = new ArrayList<USATProductBO>();
		
	    ProductDAO pDAO = new ProductDAO();
	    Collection<PersistentProductIntf> productIntfs = pDAO.searchForProducts("");
        try {
        	for (PersistentProductIntf pProd : productIntfs) {
        		USATProductBO tempProd = new USATProductBO(pProd);
        		
        		products.add(tempProd);
        		
        	}
        }
        catch(Exception e) {
            throw new UsatException(e);
        }
        return products;
	}

	public static USATProductBO fetchProduct(String productCode) throws UsatException {
		USATProductBO product = null;
		
	    ProductDAO pDAO = new ProductDAO();
	    Collection<PersistentProductIntf> productIntfs = pDAO.getProductsByProductCode(productCode);
        try {
        	if (productIntfs.size() > 1) {
        		throw new UsatException("Too many products returned: " + productIntfs.size() + " for product code " + productCode);
        	}
        	for (PersistentProductIntf pProd : productIntfs) {
        		USATProductBO tempProd = new USATProductBO(pProd);
        		
        		product = tempProd;
        		
        	}
        }
        catch(Exception e) {
            throw new UsatException(e);
        }
        return product;
	}
	
	
	public USATProductBO(PersistentProductIntf prod) {
       
        if (prod == null) {
            return;
        }
        this.taxRateCode = prod.getTaxRateCode();
        this.productCode = prod.getProductCode();
        this.brandingPubCode = prod.getBrandingPubCode();
        this.name = prod.getName();
        this.description = prod.getDescription();
        this.detailedDescription = prod.getDetailedDescription();
        this.id = prod.getID();
        this.minQuantityPerOrder = prod.getMinQuantityPerOrder();
        this.maxQuantityPerOrder = prod.getMaxQuantityPerOrder();
        this.taxable = prod.isTaxable();
        this.inventory = prod.getInventory();
        this.fulfillmentMethod = prod.getFulfillmentMethod();
        this.supplierID = prod.getSupplierID();
        this.customerServicePhone = prod.getCustomerServicePhone();
        this.defaultKeycode = prod.getDefaultKeycode();
        this.expiredOfferKeycode = prod.getExpiredOfferKeycode();
        this.maxDaysInFutureBeforeFulfillment = prod.getMaxDaysInFutureBeforeFulfillment();
        this.holdDaysDelay = prod.getHoldDaysDelay();
        this.productType = prod.getProductType();
        
        try {
            this.unitPrice = prod.getUnitPrice();
        }
        catch (UsatException e) {
            // should never get here;
        }
        this.startDate = prod.getStartDate();
        this.endDate = prod.getEndDate();
    
	}


	public void setEndDate(DateTime end) {
		this.endDate = end;
		
	}

	public void setFulfillmentMethod(int method) {
		this.fulfillmentMethod = method;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}

	public void setStartDate(DateTime start) {
		this.startDate = start;
	}

	public int getInventory() {
		return this.inventory;
	}

	public void saveToTestServers() throws UsatException {
	
		ProductUpdaterDAO dao = new ProductUpdaterDAO();
		
		ProductTO prod = this.convertToTO();
		
		dao.updateProductOnTestServers(prod);
	}

	public void saveToProductionServers() throws UsatException {
		
		ProductUpdaterDAO dao = new ProductUpdaterDAO();
		
		ProductTO prod = this.convertToTO();
		
		dao.updateProductOnProductionServers(prod);
	}
	
	private ProductTO convertToTO() throws UsatException {
		ProductTO prod = new ProductTO();
		prod.setBrandingPubCode(this.getBrandingPubCode());
		prod.setId(this.getID());
		prod.setCustomerServicePhone(this.getCustomerServicePhone());
		prod.setDefaultKeycode(this.getDefaultKeycode());
		prod.setDescription(this.getDescription());
		prod.setDetailedDescription(this.getDetailedDescription());
		prod.setEndDate(this.getEndDate());
		prod.setExpiredOfferKeycode(this.getExpiredOfferKeycode());
		prod.setFulfillmentMethod(this.getFulfillmentMethod());
		prod.setHoldDaysDelay(this.getHoldDaysDelay());
		prod.setInventory(this.getInventory());
		prod.setMaxDaysInFutureBeforeFulfillment(this.getMaxDaysInFutureBeforeFulfillment());
		prod.setName(this.getName());
		prod.setMaxQuantityPerOrder(this.getMaxQuantityPerOrder());
		prod.setMinQuantityPerOrder(this.getMinQuantityPerOrder());
		prod.setProductCode(this.getProductCode());
		prod.setTaxable(this.isTaxable());
		prod.setSupplierID(this.getSupplierID());
		prod.setUnitPrice(this.getUnitPrice());
		prod.setProductType(this.getProductType());
		return prod;
	}
	public void setSupplierID(int supplierPrimaryKey) {
		this.supplierID = supplierPrimaryKey;
	}

	@Override
	public int getHoldDaysDelay() {
		return this.holdDaysDelay;
	}

	@Override
	public void setDefaultKeyCode(String newKeyCode) {
		if (newKeyCode != null && newKeyCode.trim().length()<6) {
			this.defaultKeycode = newKeyCode.trim();
		}
	}

	@Override
	public void setExpiredOfferKeyCode(String newExpiredOfferKeycode) {
		if (newExpiredOfferKeycode != null && newExpiredOfferKeycode.trim().length()<6) {
			this.expiredOfferKeycode = newExpiredOfferKeycode.trim();
		}
	}

	@Override
	public void setHoldDelayDays(int newHoldDays) {
		if (newHoldDays > -1) {
			this.holdDaysDelay = newHoldDays;
		}
		
	}

	@Override
	public void setMaxDaysInFutureToFulfill(int numDays) {
		if (numDays > -1) {
			this.maxDaysInFutureBeforeFulfillment = numDays;
		}
		
	}

    public String getTaxRateCode() {
        return this.taxRateCode;
    }
	public String getProductCode() {
		return this.productCode;
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}

	public String getDetailedDescription() {
		return this.detailedDescription;
	}

	public boolean isTaxable() {
		return this.taxable;
	}

	public int getID() {
		return this.id;
	}

	public double getUnitPrice() throws UsatException {
		return this.unitPrice;
	}
	
	/**
	 * Sub classes should add additional checks if necessary or 
	 * override this method completely.
	 */
	public boolean isAvailable() {
	    boolean available = false;
	    // if no inventory then product is not available
	    if (this.inventory > 0) {
	        // if no start and end date restrictions then it is available
	        if (this.startDate == null && this.endDate == null) {
	            available = true;
	        }
	        else if (this.startDate != null && this.endDate != null) {
	            // if start and end date restrictions make sure we fall between them
	            if (this.startDate.isBeforeNow() && this.endDate.isAfterNow()) {
	                available = true;
	            }
	        }
	        else if (startDate != null) {
	            // if just a start date restriction make sure we are after it.
	            if (this.startDate.isBeforeNow()) {
	                available = true;
	            }
	        }
	        else {
	            // if just an end date restriction make sure we are before it
	            if (this.endDate.isAfterNow()) {
	                available = true;
	            }
	        }
	        
	    }
	    return available;
	}

	protected DateTime getStartDate() {
	    return this.startDate;
	}
	
	protected DateTime getEndDate() {
	    return this.endDate;
	}
    public int getMaxQuantityPerOrder() {
        return this.maxQuantityPerOrder;
    }
    public int getMinQuantityPerOrder() {
        return this.minQuantityPerOrder;
    }

	
	public boolean isElectronicDelivery() {
		if (this.fulfillmentMethod == ProductIntf.ELECTRONIC_DELIVERY) {
			return true;
		}
		return false;
	}

	
	public int getFulfillmentMethod() {
		return this.fulfillmentMethod;
	}

	
	public String getBrandingPubCode() {
		return this.brandingPubCode;
	}

	
	public SupplierIntf getSupplier() {
		// 
		if (this.supplier == null && this.supplierID > 0) {
			// try to retrive from db
			try {
				SupplierDAO sDao = new SupplierDAO();
				
				SupplierTOIntf sTO = sDao.fetchSupplierByID(this.supplierID);
				
				SupplierBO sBO = new SupplierBO(sTO);
				
				this.supplier = sBO;
				
			}
			catch (Exception e) {
				; // ignore
			}
		}
		return this.supplier;
	}

	public int getSupplierID() {
		return supplierID;
	}

	
	public String getCustomerServicePhone() {
		return this.customerServicePhone;
	}

	public String getDefaultKeycode() {
		return defaultKeycode;
	}

	public String getExpiredOfferKeycode() {
		return expiredOfferKeycode;
	}

	public int getMaxDaysInFutureBeforeFulfillment() {

		return this.maxDaysInFutureBeforeFulfillment;
	}

	@Override
	public int updateInventory(int delta) {
		return 0;
	}

	@Override
	public void setInitialInventory(int initialInventory) {
		this.inventory = initialInventory;
	}

	@Override
	public void setInsertTimestamp(DateTime time) {
		//
	}

	@Override
	public int getProductType() {
		return this.productType;
	}

}
