package com.gannett.usatoday.adminportal.products.intf;

import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.ProductIntf;

public interface UpdateableProductIntf  extends ProductIntf {

    public void setStartDate(DateTime start);
    public void setEndDate(DateTime end);
    public void setInventory(int inventory);
    public void setInitialInventory(int initialInventory);
    public void setInsertTimestamp(DateTime time);
    public void setFulfillmentMethod(int method);
    public void setSupplierID(int supplierPrimaryKey);
    public void setDefaultKeyCode(String newKeyCode);
    public void setExpiredOfferKeyCode(String newExpiredOfferKeycode);
    public void setHoldDelayDays(int newHoldDays);
    public void setMaxDaysInFutureToFulfill(int numDays);
	
    public void saveToTestServers() throws UsatException;
    public void saveToProductionServers() throws UsatException;
}
