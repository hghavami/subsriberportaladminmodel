package com.gannett.usatoday.adminportal.prodstats;

import com.usatoday.UsatException;

public class ProdStatsBO {

	public static int getCountOfNewUTStarts() throws UsatException {
		int count = 0;
		
		//ProductionStatsDAO dao = new ProductionStatsDAO();
		
		//count = dao.fetchNumberOfNewStarts(UsaTodayConstants.UT_PUBCODE);
		
		return count;
	}

	public static int getCountOfNewEEStarts() throws UsatException {
		int count = 0;
		
		//ProductionStatsDAO dao = new ProductionStatsDAO();
		
		//count = dao.fetchNumberOfNewStarts(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
		
		return count;
	}
	
	public static int getCountOfNewSportsWeeklyStarts() throws UsatException {
		int count = 0;
		//ProductionStatsDAO dao = new ProductionStatsDAO();
		
		///count = dao.fetchNumberOfNewStarts(UsaTodayConstants.SW_PUBCODE);
		
		
		return count;
	}

	public static int getCountOfUTRenewals() throws UsatException {
		int count = 0;
		
		//ProductionStatsDAO dao = new ProductionStatsDAO();
		
		//count = dao.fetchNumberOfRenewals(UsaTodayConstants.UT_PUBCODE);
		
		return count;
	}

	public static int getCountOfEERenewals() throws UsatException {
		int count = 0;
		
		//ProductionStatsDAO dao = new ProductionStatsDAO();
		
		///count = dao.fetchNumberOfRenewals(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
		
		return count;
	}

	
	public static int getCountOfSportsWeeklyRenewals() throws UsatException {
		int count = 0;
		
		//ProductionStatsDAO dao = new ProductionStatsDAO();
		
		//count = dao.fetchNumberOfRenewals(UsaTodayConstants.SW_PUBCODE);
		
		return count;
	}
	
	public static int getCountOfNewStartsForProduct(String productCode) throws UsatException {
		int count = 0;
		//ProductionStatsDAO dao = new ProductionStatsDAO();
		
		//count = dao.fetchNumberOfNewStarts(productCode);
		
		
		return count;
	}

	public static int getCountOfRenewalsForProduct(String productCode) throws UsatException {
		int count = 0;
		
		//ProductionStatsDAO dao = new ProductionStatsDAO();
		
		//count = dao.fetchNumberOfRenewals(productCode);
		
		return count;
	}

}
