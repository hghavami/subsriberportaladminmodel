package com.gannett.usatoday.adminportal.junit;

import com.gannett.usatoday.adminportal.campaigns.utils.CampaignFTPUtilities;

public class FTPUtilityTest {

	private String imageName = null;
	private String vanity = null;
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		if (args.length < 1) {
			System.out.println("useage: FTPUtilityTest -i [imageName] -v vanityName");
			System.out.println("must inclue either one image or one vanity or both.");
			System.exit(0);
		}

		FTPUtilityTest test = new FTPUtilityTest();
		
		for (int i = 0; i < args.length; i++) {
			try {
				if (args[i].equalsIgnoreCase("-i")) {
					test.setImageName(args[i+1]);
					continue;
				}
				if (args[i].equalsIgnoreCase("-v")) {
					test.setVanity(args[i+1]);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		test.checkForImage();
		test.checkForVanity();
	}
	
	public boolean checkForImage() {
		boolean imageOnServer = false;
		CampaignFTPUtilities ftpUtil = new CampaignFTPUtilities();
		if (this.getImageName() != null) {
			System.out.println("Checking for image: " + imageName);
			imageOnServer = ftpUtil.isImageOnProductionServer(imageName);
			System.out.println("Image on Server..........." + imageOnServer);
		}
		else {
			System.out.println("Skipping Image Check.");
		}
		return imageOnServer;
	}
	
	public boolean checkForVanity() {
		boolean vanityInUse = false;
		CampaignFTPUtilities ftpUtil = new CampaignFTPUtilities();
		if (this.getVanity() != null) {
			System.out.println("Checking for vanity: " + vanity);
			vanityInUse = ftpUtil.isVanityInUse(vanity);
			System.out.println("Vanity In Use..........." + vanityInUse);
		}
		else {
			System.out.println("Skipping Vanity Check.");
		}
		return vanityInUse;
	}
	
	public String getImageName() {
		return imageName;
	}
	public String getVanity() {
		return vanity;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public void setVanity(String vanity) {
		this.vanity = vanity;
	}

}
