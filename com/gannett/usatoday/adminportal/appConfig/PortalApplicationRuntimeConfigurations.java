package com.gannett.usatoday.adminportal.appConfig;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.gannett.usatoday.adminportal.appConfig.intf.PortalApplicationSettingIntf;
import com.gannett.usatoday.adminportal.appConfig.to.PortalApplicationSettingTO;
import com.gannett.usatoday.adminportal.integration.PortalApplicationConfigDAO;

public class PortalApplicationRuntimeConfigurations {	
	
	private HashMap<String, PortalApplicationSettingIntf> settings = new HashMap<String, PortalApplicationSettingIntf>();

	public PortalApplicationRuntimeConfigurations() {
		super();
		this.loadSettings();
	}
	
	public void reloadSettings () {
		this.loadSettings();
	}
	
	// general getter
	public PortalApplicationSettingIntf getSetting(String key) {
		return this.settings.get(key);
	}
	
	// helper methods

	
	public PortalApplicationSettingIntf getSubscriberPortalAdminSystemDescription() {
		PortalApplicationSettingIntf setting = null;
		setting = this.settings.get(PortalApplicationSettingIntf.PORTAL_ADMIN_DESCRIPTION);
		return setting;
	}
	
	public PortalApplicationSettingIntf getUTDefaultRenewalRateCode() {
		PortalApplicationSettingIntf setting = null;
		setting = this.settings.get(PortalApplicationSettingIntf.UT_DEFAULT_RENEWAL_RATECODE);
		return setting;
	}
	
	public PortalApplicationSettingIntf getSportsWeeklyDefaultRenewalRateCode() {
		PortalApplicationSettingIntf setting = null;
		setting = this.settings.get(PortalApplicationSettingIntf.SW_DEFAULT_RENEWAL_RATECODE);
		return setting;
	}
	
	public PortalApplicationSettingIntf getUTDefaultForceEZPayOfferRateCode() {
		PortalApplicationSettingIntf setting = null;
		setting = this.settings.get(PortalApplicationSettingIntf.UT_DEFAULT_FORCE_EZPAY_OFFER_RATECODE);
		return setting;
	}
	
	public PortalApplicationSettingIntf getSportsWeeklyDefaultForceEZPayOfferRateCode() {
		PortalApplicationSettingIntf setting = null;
		setting = this.settings.get(PortalApplicationSettingIntf.SW_DEFAULT_FORCE_EZPAY_OFFER_RATECODE);
		return setting;
	}
	
	public PortalApplicationSettingIntf getUTForceEZPayOfferProgramEnabled() {
		PortalApplicationSettingIntf setting = null;
		setting = this.settings.get(PortalApplicationSettingIntf.UT_FORCE_EZPAY_OFFER_ENABLED);
		return setting;
	}
	
	public PortalApplicationSettingIntf getProductionDomain() {
		PortalApplicationSettingIntf setting = null;
		setting = this.settings.get(PortalApplicationSettingIntf.PRODUCTION_DOMAIN);
		return setting;
	}
	
	public PortalApplicationSettingIntf getSportsWeeklyForceEZPayOfferProgramEnabled() {
		PortalApplicationSettingIntf setting = null;
		setting = this.settings.get(PortalApplicationSettingIntf.SW_FORCE_EZPAY_OFFER_ENABLED);
		return setting;
	}
	
	private void loadSettings() {
		
		if (this.settings.size() > 0) {
			this.settings.clear();
		}
		
		try {
			PortalApplicationConfigDAO dao = new PortalApplicationConfigDAO();
			
			Collection<PortalApplicationSettingTO> settingTOs = dao.getApplicationConfigurations();
			
			for(PortalApplicationSettingTO to : settingTOs) {
				PortalApplicationSettingBO bo = new PortalApplicationSettingBO(to);
				
				this.settings.put(bo.getKey(), bo);
				
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Collection<PortalApplicationSettingIntf> getAllSettings() {
		ArrayList<PortalApplicationSettingIntf> s = new ArrayList<PortalApplicationSettingIntf>(this.settings.values());
		
		return s;
	}

	public Collection<PortalApplicationSettingIntf> getAllForceEZPAYSettings() {
		ArrayList<PortalApplicationSettingIntf> s = new ArrayList<PortalApplicationSettingIntf>();
		
		for (String settingKey : this.settings.keySet()) {
			PortalApplicationSettingIntf setting = this.settings.get(settingKey);
			if (setting.getGroup().equalsIgnoreCase("forceEZPayOffer")) {
				s.add(setting);
			}
		}
		return s;
	}
}
