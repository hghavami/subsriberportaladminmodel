package com.gannett.usatoday.adminportal.appConfig.intf;

import org.joda.time.DateTime;

import com.usatoday.UsatException;

public interface PortalApplicationSettingIntf {

	public static final String UT_DEFAULT_KEYCODE = "usat.ut.default.keycode";
	public static final String SW_DEFAULT_KEYCODE = "usat.sw.default.keycode";
	
	public static final String UT_EXPIRED_KEYCODE = "usat.ut.default.expired.offer";
	public static final String SW_EXPIRED_KEYCODE = "usat.sw.default.expired.offer";

	public static final String FORCE_EZPAY_GROUP = "forceEZPayOffer";
	public static final String UT_DEFAULT_RENEWAL_RATECODE = "usat.ut.default.renewal.ratecode";
	public static final String SW_DEFAULT_RENEWAL_RATECODE = "usat.sw.default.renewal.ratecode";
	
	public static final String UT_DEFAULT_FORCE_EZPAY_OFFER_RATECODE = "usat.ut.default.forceEZPayOffer.rateCode";
	public static final String SW_DEFAULT_FORCE_EZPAY_OFFER_RATECODE = "usat.sw.default.forceEZPayOffer.rateCode";
	
	public static final String UT_FORCE_EZPAY_OFFER_ENABLED = "usat.ut.default.forceEZPayOffer.enabled";
	public static final String SW_FORCE_EZPAY_OFFER_ENABLED = "usat.sw.default.forceEZPayOffer.enabled";

	public static final String PORTAL_ADMIN_PUBLISH_ALERT_RECIPIENTS = "usat.admin.campaign.publishingAlerts";
	
	public static final String PRODUCTION_DOMAIN = "usat.production.domain";
	public static final String PORTAL_ADMIN_DESCRIPTION = "usat.subportaladmin.sys.description";
	
	public int getID();
	public int getUpdateCount();
	public DateTime getLastUpdateTime();
	public String getGroup();
	public void setGroup(String newGroup);
	public String getKey();
	public void setKey(String newKey);
	public String getValue();
	public void setValue(String newValue);
	public String getLabel();
	public void setLabel(String label);
	public String getDescription();
	public void setDescription(String newDescription);
	
	public void save() throws UsatException;
}
