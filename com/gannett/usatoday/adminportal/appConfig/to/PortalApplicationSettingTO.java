package com.gannett.usatoday.adminportal.appConfig.to;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.appConfig.intf.PortalApplicationSettingIntf;
import com.usatoday.UsatException;

public class PortalApplicationSettingTO implements
		PortalApplicationSettingIntf, Serializable {

	private int id = -1;
	private int updateCount = 0;
	private DateTime lastUpdateTime = null;
	private String group = null;
	private String key = null;
	private String value = null;
	private String description = null;
	private String label = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getDescription() {
		return this.description;
	}

	public String getGroup() {
		return this.group;
	}

	public int getID() {
		return this.id;
	}

	public String getKey() {
		return this.key;
	}

	public String getValue() {
		return this.value;
	}

	public void setDescription(String newDescription) {
		this.description = newDescription;
	}

	public void setGroup(String newGroup) {
		this.group = newGroup;

	}

	public void setKey(String newKey) {
		this.key = newKey;

	}

	public void setValue(String newValue) {
		this.value = newValue;

	}
	
	public void setID(int id) {
		this.id = id;
	}

	public int getUpdateCount() {
		return updateCount;
	}

	public void setUpdateCount(int updateCount) {
		this.updateCount = updateCount;
	}

	public DateTime getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(DateTime updateTime) {
		this.lastUpdateTime = updateTime;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public void save() throws UsatException {
		throw new UsatException("Save should be called from BO");
		
	}

}
