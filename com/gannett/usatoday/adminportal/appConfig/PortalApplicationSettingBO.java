package com.gannett.usatoday.adminportal.appConfig;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.appConfig.intf.PortalApplicationSettingIntf;
import com.gannett.usatoday.adminportal.appConfig.to.PortalApplicationSettingTO;
import com.gannett.usatoday.adminportal.integration.PortalApplicationConfigDAO;
import com.usatoday.UsatException;

public class PortalApplicationSettingBO implements
		PortalApplicationSettingIntf {
	private int id = -1;
	private String group = null;
	private String key = null;
	private String value = null;
	private String description = null;
	private DateTime lastUpdateTime = null;
	private int updateCount = 0;
	private String label = null;
	
	//private PortalApplicationSettingIntf original = null;
	
	public PortalApplicationSettingBO() {
		super();
	}

	public PortalApplicationSettingBO(PortalApplicationSettingIntf source) {
		super();
		this.id = source.getID();
		this.group = source.getGroup();
		this.key = source.getKey();
		this.value = source.getValue();
		this.description = source.getDescription();
		this.lastUpdateTime = source.getLastUpdateTime();
		this.updateCount = source.getUpdateCount();
		this.label = source.getLabel();
		
		//this.original = source;
	}
	
	public String getDescription() {
		return this.description;
	}

	public String getGroup() {
		return this.group;
	}

	public int getID() {
		return this.id;
	}

	public String getKey() {
		return this.key;
	}

	public String getValue() {
		return this.value;
	}

	public void setDescription(String newDescription) {
		if (newDescription != null) {
			newDescription = newDescription.trim();
			if (newDescription.length() > 256) {
				System.out.println("Truncating new description to 256 characters");
				newDescription = newDescription.substring(0,255);
			}
		}
		this.description = newDescription;
	}

	public void setGroup(String newGroup) {
		if (newGroup != null) {
			newGroup = newGroup.trim();
			if (newGroup.length() > 20) {
				System.out.println("Truncating new group name to 20 characters");
				newGroup = newGroup.substring(0,19);
			}
		}
		this.group = newGroup;
	}

	public void setKey(String newKey) {
		if (newKey != null) {
			newKey = newKey.trim();
			if (newKey.length() > 50) {
				System.out.println("Truncating new key name to 50 characters");
				newKey = newKey.substring(0,49);
			}
		}
		this.key = newKey;
	}

	public void setValue(String newValue) {
		if (newValue != null) {
			newValue = newValue.trim();
			if (newValue.length() > 100) {
				System.out.println("Truncating new value to 100 characters");
				newValue = newValue.substring(0,99);
			}
		}
		this.value = newValue;
	}
	
	public void save() throws UsatException {
		
		PortalApplicationSettingTO saveTO = new PortalApplicationSettingTO();
		if (this.id > -1) {
			// update
			saveTO.setDescription(this.description);
			saveTO.setGroup(this.group);
			saveTO.setID(this.id);
			saveTO.setKey(this.key);
			saveTO.setLastUpdateTime(this.lastUpdateTime);
			saveTO.setUpdateCount(this.updateCount);
			saveTO.setValue(this.value);
			
			PortalApplicationConfigDAO dao = new PortalApplicationConfigDAO();
			saveTO = dao.update(saveTO);
			
			this.lastUpdateTime = saveTO.getLastUpdateTime();
			this.updateCount = saveTO.getUpdateCount();
			
			// set original if we ever implement that.
		}
		else {
			// insert new
			//future
			throw new UsatException("Insert of new configurations is not supported.");
		}
		
	}

	public DateTime getLastUpdateTime() {
		return lastUpdateTime;
	}

	public int getUpdateCount() {
		return updateCount;
	}
	
	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
