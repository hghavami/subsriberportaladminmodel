/**
 * 
 */
package com.gannett.usatoday.adminportal.users;

import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.integration.PortalUserDAO;
import com.gannett.usatoday.adminportal.users.intf.PortalUserIntf;
import com.gannett.usatoday.adminportal.users.intf.PortalUserTOIntf;
import com.usatoday.UsatException;

/**
 * @author aeast
 *
 */
public class PortalUserBO implements PortalUserIntf {

	private PortalUserTOIntf original = null;
    private String userID = null;
    private DateTime insertTimestamp = null;
    private boolean enabled = true;
    private String lastName = null;
    private String firstName = null;
    private String phone = null;
    private String email = null;
    private String password = null;
    
    // state members
    private boolean isPersistent = false;
    private boolean changed = true;
   // private boolean rolesChanged = true;
    
    // related members
    // roles
    private Collection<PortalUserRoleEnum> assignedRoles = new ArrayList<PortalUserRoleEnum>();
	/**
	 * 
	 */
	public PortalUserBO() {
	}

	public PortalUserBO(PortalUserTOIntf source) {
		this.changed = false;
		this.email = source.getEmailAddress();
		this.enabled = source.getEnabled();
		this.firstName = source.getFirstName();
		this.lastName = source.getLastName();
		this.insertTimestamp = source.getCreatedTime();
		this.isPersistent = true;
		this.password = source.getPassword();
		this.phone = source.getPhone();
		this.userID = source.getUserID();
		this.assignedRoles.addAll(source.getAssignedRoles());
		this.original = source;
		
	}
	
	public static PortalUserIntf authenticateUser(String userID, String password) throws UsatException {
		PortalUserIntf user = null;
		
		PortalUserDAO dao = new PortalUserDAO();
		
		PortalUserTOIntf dbUser = dao.fetchUserByIDandPwd(userID, password);
		
		if (dbUser != null) {
			user = new PortalUserBO(dbUser);
		}
		
		return user;
	}
	
	public static Collection<PortalUserIntf> retrieveAllUsers(PortalUserIntf requestingUser) throws UsatException {

		if (requestingUser == null || !(requestingUser.hasAccess(PortalUserRoleEnum.POWER) || 
				requestingUser.hasAccess(PortalUserRoleEnum.ADMINISTRATOR))) {
			throw new UsatException("Not Authorized to fetch all users. Only Administrators and Power Users may work with system users.");
		}
		
		PortalUserDAO dao = new PortalUserDAO();
		Collection<PortalUserTOIntf> userTOs = dao.fetchAllPortalUsers();
		
		ArrayList<PortalUserIntf> userBOs = new ArrayList<PortalUserIntf>();
		
		if (userTOs != null && userTOs.size() > 0) {
			for (PortalUserTOIntf uTO : userTOs) {
				PortalUserBO u = new PortalUserBO(uTO);
				userBOs.add(u);
			}
		}
		
		return userBOs;
	}

	public static Collection<PortalUserIntf> retrieveUsersByEmail(String emailAddress) throws UsatException {
		
		PortalUserDAO dao = new PortalUserDAO();
		Collection<PortalUserTOIntf> userTOs = dao.fetchPortalUsersByEmail(emailAddress);
		
		ArrayList<PortalUserIntf> userBOs = new ArrayList<PortalUserIntf>();
		
		if (userTOs != null && userTOs.size() > 0) {
			for (PortalUserTOIntf uTO : userTOs) {
				PortalUserBO u = new PortalUserBO(uTO);
				userBOs.add(u);
			}
		}
		
		return userBOs;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#getPhone()
	 */
	public String getPhone() {
		return this.phone;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#getCreatedTime()
	 */
	public DateTime getCreatedTime() {
		return this.insertTimestamp;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#getEmailAddress()
	 */
	public String getEmailAddress() {
		return this.email;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#getEnabled()
	 */
	public boolean getEnabled() {
		return this.enabled;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#getFirstName()
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#getIsChanged()
	 */
	public boolean getIsChanged() {
		boolean userChanged = false;
		if (!this.email.equalsIgnoreCase(original.getEmailAddress())) {
			userChanged = true;
		}
		if (!userChanged && this.getEnabled() != this.original.getEnabled()) {
			userChanged = true;
		}
		if (!userChanged && !this.getFirstName().equals(original.getFirstName())) {
			userChanged = true;
		}
		if (!userChanged && !this.getLastName().equals(original.getLastName())) {
			userChanged = true;
		}
		if (!userChanged && !this.getPassword().equals(original.getPassword())) {
			userChanged = true;
		}
		if (!userChanged && !this.getPhone().equalsIgnoreCase(original.getPhone())) {
			userChanged = true;
		}
		if (!userChanged) {
			Collection<PortalUserRoleEnum> currentRoles = this.getAssignedRoles();
			Collection<PortalUserRoleEnum> oldRoles = original.getAssignedRoles();
			if (oldRoles.size() != currentRoles.size() || !oldRoles.containsAll(currentRoles)) {
				userChanged = true;				
			}
		}
		
		this.changed = userChanged;
		return this.changed;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#getLastName()
	 */
	public String getLastName() {
		return this.lastName;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#getPassword()
	 */
	public String getPassword() {
		return this.password;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#getUserId()
	 */
	public String getUserID() {
		return this.userID;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#save()
	 */
	public void save() throws UsatException {
		if (this.isPersistent && this.getIsChanged()) {
			// Update the User
			PortalUserDAO dao = new PortalUserDAO();
			
			dao.update(this);
			// replace the original with this object in case of another save request.
			this.original = new PortalUserBO(this);
			this.changed = false;
		}
		else if (!this.isPersistent){
			// Insert the user
			PortalUserDAO dao = new PortalUserDAO();
			
			dao.insert(this);
			// replace the original with this object in case of another save request.
			this.original = new PortalUserBO(this);
			this.changed = false;
			this.isPersistent = true;
		}
		else {
			; // no chages to persistent object.
		}
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#setEmailAddress(java.lang.String)
	 */
	public void setEmailAddress(String newEmailAddress) throws UsatException {
		if (newEmailAddress != null && newEmailAddress.length() < 129) {
			this.email = newEmailAddress;
		}
		else {
			throw new UsatException("Invalid Email Address");
		}
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#setEnabled(boolean)
	 */
	public void setEnabled(boolean isEnabled) {
		this.enabled = isEnabled;
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#setFirstName(java.lang.String)
	 */
	public void setFirstName(String newFirstName) throws UsatException {
		if (newFirstName != null && newFirstName.length() < 41) {
			this.firstName = newFirstName;
		}
		else {
			throw new UsatException("Invalid First Name");
		}
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#setLastName(java.lang.String)
	 */
	public void setLastName(String newLastName) throws UsatException {
		if (newLastName != null && newLastName.length() < 41) {
			this.lastName = newLastName;
		}
		else {
			throw new UsatException("Invalid Last Name");
		}
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#setNewPassword(byte[])
	 */
	public void setNewPassword(String newPassword) throws UsatException {
		if (newPassword != null && newPassword.length() > 0 && newPassword.length() < 21) {
			this.password = newPassword;
		}
		else {
			throw new UsatException("Invalid Password");
		}
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#setPhone(java.lang.String)
	 */
	public void setPhone(String newPhone) throws UsatException {
		if (newPhone != null && newPhone.length() < 11) {
			this.phone = newPhone;
		}
		else {
			throw new UsatException("Invalid Phone Number");
		}
	}

	/* (non-Javadoc)
	 * @see com.gannett.usatoday.adminportal.users.intf.PortalUserIntf#setUserId(java.lang.String)
	 */
	public void setUserID(String userID) throws UsatException {
		if (userID != null && userID.length() < 33) {
			this.userID = userID;
		}
		else {
			throw new UsatException("Invalid User ID");
		}
	}


	public void addRole(PortalUserRoleEnum role) {
		boolean alreadyAssigned = false;
		for (PortalUserRoleEnum aRole : this.assignedRoles) {
			if (aRole == role){
				alreadyAssigned = true;
			}
		}
		if (!alreadyAssigned) {
			this.assignedRoles.add(role);
		}
	}

	public boolean hasAccess(PortalUserRoleEnum toRole) {
		boolean userHasAccess = false;
		for (PortalUserRoleEnum aRole : this.assignedRoles) {
			if (aRole == toRole){
				userHasAccess = true;
			}
		}
		return userHasAccess;
	}

	public void setAssignedRoles(Collection<PortalUserRoleEnum> rolesAssigned) {
		if (this.assignedRoles == rolesAssigned) {
			// attempting to set roles to self simply ignore
		}
		else {
			this.assignedRoles.clear();
			this.assignedRoles.addAll(rolesAssigned);
		}
	}

	public Collection<PortalUserRoleEnum> getAssignedRoles() {
		return this.assignedRoles;
	}

	public void delete() throws UsatException {
		if (this.isPersistent && this.userID.length() > 0) {
			// Update the User
			PortalUserDAO dao = new PortalUserDAO();
			
			dao.delete(this);
			// replace the original with this object in case of another save request.
			this.original = new PortalUserBO(this);
			this.changed = false;
			this.isPersistent = false;
		}		
	}

}
