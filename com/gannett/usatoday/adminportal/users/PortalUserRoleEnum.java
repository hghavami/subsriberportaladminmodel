package com.gannett.usatoday.adminportal.users;

public enum PortalUserRoleEnum {
	LIMITED (4),
	NORMAL(2),
	POWER(3), 
	ADMINISTRATOR(1);
	
	private int dbID;
	PortalUserRoleEnum(int type) {
		dbID = type;
	}
	
	public int getDbRepresentation() {
		return dbID;
	}
	
	public String toString() {
		String valueStr = "Normal User";
		switch (dbID) {
		case 4:
			valueStr = "Limited User";
			break;
		case 1:
			valueStr = "Administrator";
			break;
		case 3:
			valueStr = "Power User";
			break;

		default:
			break;
		}
		return valueStr;
	}
}
