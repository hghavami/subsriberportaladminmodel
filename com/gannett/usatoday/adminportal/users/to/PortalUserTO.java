package com.gannett.usatoday.adminportal.users.to;

import java.io.Serializable;
import java.util.Collection;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.users.PortalUserRoleEnum;
import com.gannett.usatoday.adminportal.users.intf.PortalUserTOIntf;

public class PortalUserTO implements PortalUserTOIntf, Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userID = null;
    private DateTime insertTimestamp = null;
    private boolean enabled = false;
    private String lastName = null;
    private String firstName = null;
    private String phone = null;
    private String email = null;
    private String password = null;
    Collection<PortalUserRoleEnum> assignedRoles = null;
	
	public String getEmailAddress() {
		return this.email;
	}

	public boolean getEnabled() {
		return this.enabled;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public String getPhone() {
		return this.phone;
	}

	public String getUserID() {
		return this.userID;
	}

	public DateTime getCreatedTime() {
		return insertTimestamp;
	}

	public String getEmail() {
		return email;
	}

	public DateTime getInsertTimestamp() {
		return insertTimestamp;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setInsertTimestamp(DateTime insertTimestamp) {
		this.insertTimestamp = insertTimestamp;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Collection<PortalUserRoleEnum> getAssignedRoles() {
		return assignedRoles;
	}

	public void setAssignedRoles(Collection<PortalUserRoleEnum> assignedRoles) {
		this.assignedRoles = assignedRoles;
	}

}
