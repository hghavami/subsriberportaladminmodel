package com.gannett.usatoday.adminportal.users.intf;

import java.util.Collection;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.users.PortalUserRoleEnum;

public interface PortalUserTOIntf {

	public String getUserID();
	public boolean getEnabled();
	public String getLastName();
	public String getFirstName();
	public String getPhone();
	public String getEmailAddress();
	public String getPassword();
	public DateTime getCreatedTime();
	public Collection<PortalUserRoleEnum> getAssignedRoles();
	
}
