package com.gannett.usatoday.adminportal.users.intf;

import java.util.Collection;

import com.gannett.usatoday.adminportal.users.PortalUserRoleEnum;
import com.usatoday.UsatException;

public interface PortalUserIntf extends PortalUserTOIntf {

	public void setUserID(String userID) throws UsatException;
	
	public void setEnabled(boolean isEnabled);
	
	public void setLastName(String newLastName) throws UsatException;
	
	public void setFirstName(String newFirstName) throws UsatException;
	
	public void setPhone(String newPhone) throws UsatException;
	
	public void setEmailAddress(String newEmailAddress) throws UsatException;
	
	public void setNewPassword(String newPassword) throws UsatException;
	
	public void save() throws UsatException;
	
	public void delete() throws UsatException;
	
	public boolean getIsChanged();
	
	public void setAssignedRoles(Collection<PortalUserRoleEnum> rolesAssigned);
	
	public void addRole(PortalUserRoleEnum role);
	
	public boolean hasAccess(PortalUserRoleEnum toRole);
	
}
